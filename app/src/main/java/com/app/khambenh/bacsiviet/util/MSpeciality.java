package com.app.khambenh.bacsiviet.util;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MSpeciality {

    @SerializedName("speciality_id")
    @Expose
    private Integer specialityId;
    @SerializedName("speciality_name")
    @Expose
    private String specialityName;
    @SerializedName("en_speciality_name")
    @Expose
    private String enSpecialityName;
    @SerializedName("fr_speciality_name")
    @Expose
    private String frSpecialityName;
    @SerializedName("specialty_url")
    @Expose
    private String specialtyUrl;
    @SerializedName("speciality_image")
    @Expose
    private String specialityImage;
    @SerializedName("speciality_desc")
    @Expose
    private String specialityDesc;

    public Integer getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(Integer specialityId) {
        this.specialityId = specialityId;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public String getEnSpecialityName() {
        return enSpecialityName;
    }

    public void setEnSpecialityName(String enSpecialityName) {
        this.enSpecialityName = enSpecialityName;
    }

    public String getFrSpecialityName() {
        return frSpecialityName;
    }

    public void setFrSpecialityName(String frSpecialityName) {
        this.frSpecialityName = frSpecialityName;
    }

    public String getSpecialtyUrl() {
        return specialtyUrl;
    }

    public void setSpecialtyUrl(String specialtyUrl) {
        this.specialtyUrl = specialtyUrl;
    }

    public String getSpecialityImage() {
        return specialityImage;
    }

    public void setSpecialityImage(String specialityImage) {
        this.specialityImage = specialityImage;
    }

    public String getSpecialityDesc() {
        return specialityDesc;
    }

    public void setSpecialityDesc(String specialityDesc) {
        this.specialityDesc = specialityDesc;
    }
}