package com.app.khambenh.bacsiviet.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.MessageFirebaseActivity;
import com.app.khambenh.bacsiviet.stringee.activity.OutgoingCallActivity;
import com.app.khambenh.bacsiviet.stringee.service.StringeeService;
import com.stringee.StringeeClient;

import org.json.JSONException;
import org.json.JSONObject;

import static com.app.khambenh.bacsiviet.utils.ResourceUtils.getString;


//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.Intent;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.app.khambenh.bacsiviet.CoreApp;
//import com.app.khambenh.bacsiviet.R;
//import com.app.khambenh.bacsiviet.activities.CallActivity;
//import com.app.khambenh.bacsiviet.activities.MessageFirebaseActivity;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//
public class CallUltils {
    public static boolean isFromChatActivity = false;
//
//    public static void videoCall(QBUser qbUser, final Context mContext) {
//        String type = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_TYPE, String.class);
//        if (!type.equalsIgnoreCase("user")) {
//            startCall(true, qbUser, mContext);
//        } else {
//            int paid = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_PAID, Integer.class);
//            long unit = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_UNIT, Long.class);
//            long balance = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE, Long.class);
//            if (paid == 1 && unit <= balance) {
//                startCall(true, qbUser, mContext);
//            } else {
//                dialogNaptien(mContext);
//            }
//        }
//    }
//
//    public static void audioCall(QBUser qbUser, Context mContext) {
//        String type = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_TYPE, String.class);
//        if (!type.equalsIgnoreCase("user")) {
//            startCall(false, qbUser, mContext);
//        }
//        {
//            int paid = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_PAID, Integer.class);
//            long unit = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_UNIT, Long.class);
//            long balance = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE, Long.class);
//            if (paid == 1 && unit <= balance) {
//                startCall(false, qbUser, mContext);
//            } else {
//                dialogNaptien(mContext);
//            }
//        }
//    }
//
    public static void checkToCall(QBUser qbUser, Activity activity, final String type) {
        JSONObject postparams = new JSONObject();
        String usename = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_NAME, String.class);
        try {
            postparams.put("email", "" + usename);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_CHECK_TO_CALL, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            boolean isSuccess = false;
                            int can_chat = 0, can_call_audio = 0, can_call_video = 0;

                            if (response.has("isSuccess")) {
                                isSuccess = response.getBoolean("isSuccess");
                            }
                            if (response.has("can_chat")) {
                                can_chat = response.getInt("can_chat");
                            }
                            if (response.has("can_call_audio")) {
                                can_call_audio = response.getInt("can_call_audio");
                            }
                            if (response.has("can_call_video")) {
                                can_call_video = response.getInt("can_call_video");
                            }
                            if (isSuccess) {
                                if (type.equals(Consts.TYPE_CHAT)) {
                                    if (can_chat == 1) {
                                        chat(qbUser, activity);
                                    } else {
                                        Log.e("can_chat", can_chat+"");
                                        ShowDialogPermission(activity);
                                    }
                                }
                                else if (type.equals(Consts.TYPE_CALL_AUDIO)) {
                                    if (can_call_audio == 1) {
                                        StringeeClient client = StringeeService.getInstance().stringeeClient;
                                        if (client.isConnected()) {
                                            Intent intent = new Intent(activity, OutgoingCallActivity.class);
                                            intent.putExtra("name", qbUser.getFullName());
                                            intent.putExtra("from", client.getUserId());
                                            intent.putExtra("to", qbUser.getId()+"");
                                            intent.putExtra("is_video_call", false);
                                            activity.startActivity(intent);
                                        }
                                    } else {
                                        Log.e("can_call_audio", can_call_audio+"");
                                        ShowDialogPermission(activity);
                                    }
                                } else if (type.equals(Consts.TYPE_CALL_VIDEO)) {
                                    if (can_call_video == 1) {
                                        StringeeClient client = StringeeService.getInstance().stringeeClient;
                                        if (client.isConnected()) {
                                            Intent intent = new Intent(activity, OutgoingCallActivity.class);
                                            intent.putExtra("name", qbUser.getFullName());
                                            intent.putExtra("from", client.getUserId());
                                            intent.putExtra("to", qbUser.getId()+"");
                                            intent.putExtra("is_video_call", true);
                                            activity.startActivity(intent);
                                        }
                                    } else {
                                        Log.e("can_call_video", can_call_video+"");
                                        ShowDialogPermission(activity);
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(context, "" + error.getMessage(), Toast.LENGTH_LONG).show();
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void chat(QBUser qbUser, Context mContext) {
//        ArrayList<QBUser> selectedUsers = new ArrayList<>();
//        selectedUsers.add(qbUser);
//        if (selectedUsers.get(0).getCustomData() != null && selectedUsers.get(0).getCustomData().contains("-")) {
//            selectedUsers.get(0).setFullName(selectedUsers.get(0).getCustomData().split("-")[0]);
//        }
//        int user_id = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);
        MessageFirebaseActivity.startMessageFirebase(mContext, true, qbUser);
    }
//
//    private static void dialogNaptien(Context mContext) {
//
//        final Dialog dialog = new Dialog(mContext);
////        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.setTitle(getString(R.string.title_note));
//        dialog.setContentView(R.layout.dialog_notifi_need_to_recharge);
//        dialog.setCancelable(false);
//        TextView txtMessDialog = (TextView) dialog.findViewById(R.id.txtMessDialog);
//        txtMessDialog.setText(getString(R.string.notifi_need_to_recharge));
//        Button btnSeen = (Button) dialog.findViewById(R.id.btnSeen);
//        btnSeen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//
////        if (isFromChatActivity) {
////            Intent intent = new Intent();
////            intent.setAction(Consts.ACTION_SHOW_DIALOG_TO_RECHARGE_CHAT_ACTIVITY);
////            mContext.sendBroadcast(intent);
////        } else {
////            final Dialog dialog = new Dialog(mContext);
//////        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
////            dialog.setTitle(getString(R.string.title_note));
////            dialog.setContentView(R.layout.dialog_notifi_need_to_recharge);
////            dialog.setCancelable(false);
////            TextView txtMessDialog = (TextView) dialog.findViewById(R.id.txtMessDialog);
////            txtMessDialog.setText(getString(R.string.notifi_need_to_recharge));
////            Button btnSeen = (Button) dialog.findViewById(R.id.btnSeen);
////            btnSeen.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    dialog.dismiss();
////                }
////            });
////            dialog.show();
////        }
//
////        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
////        builder.setTitle(mContext.getString(R.string.title_note));
////        builder.setMessage(mContext.getString(R.string.notifi_need_to_recharge));
////        builder.setCancelable(false);
////        builder.setNegativeButton(mContext.getString(R.string.btn_seen), new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialogInterface, int i) {
////                dialogInterface.dismiss();
////            }
////        });
////        AlertDialog alertDialog = builder.create();
////        alertDialog.show();
//    }
//
//    private static void startCall(boolean isVideoCall, QBUser qbUser, Context mContext) {
//        String tag2 = "IOS";
//        if (qbUser.getTags().size() > 1) {
//            tag2 = qbUser.getTags().get(1);
//        }
//        ArrayList<Integer> opponentsList = new ArrayList<>();
//        opponentsList.add(qbUser.getId());
//        QBRTCTypes.QBConferenceType conferenceType = isVideoCall
//                ? QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
//                : QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO;
//
//        QBRTCClient qbrtcClient = QBRTCClient.getInstance(mContext);
//
//        QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType);
//
//        WebRtcSessionManager.getInstance(mContext).setCurrentSession(newQbRtcSession);
//        if (ChatHelper.getCurrentUser() != null) {
//            PushNotificationSender.sendPushMessage(opponentsList, "" + ChatHelper.getCurrentUser().getFullName(), tag2);
//        } else {
//            PushNotificationSender.sendPushMessage(opponentsList, "User", tag2);
//        }
//
////        CallActivity.start(mContext, qbUser, false);
//        StaticCommon.IsStartCallActivity = true;
//        Intent intent = new Intent(mContext, CallActivity.class);
//        intent.putExtra(Consts.EXTRA_IS_INCOMING_CALL, false);
//        intent.putExtra(Consts.EXTRA_QB_USER, qbUser);
//        mContext.startActivity(intent);
//
//    }
//
    private static void ShowDialogPermission(Context mContext) {

        final Dialog dialog = new Dialog(mContext);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        dialog.setTitle(getString(R.string.title_note));
        dialog.setContentView(R.layout.dialog_notifi_need_to_recharge);
        dialog.setCancelable(false);
        TextView txtMessDialog = (TextView) dialog.findViewById(R.id.txtMessDialog);
        txtMessDialog.setText(getString(R.string.notifi_need_to_recharge));
        Button btnSeen = (Button) dialog.findViewById(R.id.btnSeen);
        btnSeen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

//
//        if (isFromChatActivity) {
//            Intent intent = new Intent();
//            intent.setAction(Consts.ACTION_SHOW_DIALOG_PEMISSION_CHAT_ACTIVITY);
//            mContext.sendBroadcast(intent);
//        } else {
//            final Dialog dialog = new Dialog(mContext);
////        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//            dialog.setTitle(getString(R.string.title_note));
//            dialog.setContentView(R.layout.dialog_notifi_need_to_recharge);
//            dialog.setCancelable(false);
//            TextView txtMessDialog = (TextView) dialog.findViewById(R.id.txtMessDialog);
//            txtMessDialog.setText(getString(R.string.notifi_need_permission));
//            Button btnSeen = (Button) dialog.findViewById(R.id.btnSeen);
//            btnSeen.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                }
//            });
//            dialog.show();
//        }
    }

    public static void unLockScreen(Activity activity) {
        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }
}
