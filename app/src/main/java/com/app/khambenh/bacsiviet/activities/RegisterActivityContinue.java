package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.app.khambenh.bacsiviet.R;

public class RegisterActivityContinue extends BaseActivity  {
    private Button btnContinue;
    private Activity self;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        self = this;
        setContentView(R.layout.k_activity_input_phone);
        initView();
        initEvent();
    }

    private void initView() {
        btnContinue = findViewById(R.id.btnRegister);
    }

    private void initEvent() {
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(self, RegisterActivityContinue.class));
            }
        });
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
}
