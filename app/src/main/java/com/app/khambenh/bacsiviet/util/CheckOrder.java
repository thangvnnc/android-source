package com.app.khambenh.bacsiviet.util;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckOrder implements Serializable {
    @SerializedName("func")
    private String func;
    @SerializedName("version")
    private String version;
    @SerializedName("merchant_id")
    private String merchantId;
    @SerializedName("token_code")
    private String tokenCode;
    @SerializedName("checksum")
    private String checksum;

    public CheckOrder() {

    }

    public void setFunc(String func) {
        this.func = func;
    }

    public String getFunc() {
        return func;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setTokenCode(String tokenCode) {
        this.tokenCode = tokenCode;
    }

    public String getTokenCode() {
        return tokenCode;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksum() {
        return checksum;
    }


    public static class CheckOrderResult implements Serializable {

        @SerializedName("response_code")
        private String responseCode;
        @SerializedName("buyer_email")
        private String buyerEmail;
        @SerializedName("buyer_fullname")
        private String buyerFullName;
        @SerializedName("buyer_mobile")
        private String buyerMobile;
        @SerializedName("order_code")
        private String orderCode;
        @SerializedName("total_amount")
        private int totalAmount;
        @SerializedName("currency")
        private String currency;
        @SerializedName("language")
        private String language;
        @SerializedName("buyer_address")
        private String buyerAddress;
        @SerializedName("transaction_id")
        private int transactionId;
        @SerializedName("transaction_status")
        private int transactionStatus;

        public CheckOrderResult() {
        }

        public String getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(String responseCode) {
            this.responseCode = responseCode;
        }

        public String getBuyerEmail() {
            return buyerEmail;
        }

        public void setBuyerEmail(String buyerEmail) {
            this.buyerEmail = buyerEmail;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(int totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getBuyerAddress() {
            return buyerAddress;
        }

        public void setBuyerAddress(String buyerAddress) {
            this.buyerAddress = buyerAddress;
        }

        public int getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(int transactionId) {
            this.transactionId = transactionId;
        }

        public int getTransactionStatus() {
            return transactionStatus;
        }

        public void setTransactionStatus(int transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        public String getBuyerFullName() {
            return buyerFullName;
        }

        public void setBuyerFullName(String buyerFullName) {
            this.buyerFullName = buyerFullName;
        }

        public String getBuyerMobile() {
            return buyerMobile;
        }

        public void setBuyerMobile(String buyerMobile) {
            this.buyerMobile = buyerMobile;
        }
    }
}
