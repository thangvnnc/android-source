package com.app.khambenh.bacsiviet.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.Doctor;
import com.app.khambenh.bacsiviet.util.ResultUser;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * QuickBlox team
 */
public class DoctorAdapter extends RecyclerView.Adapter<DoctorAdapter.UserViewHolder> {
    private Activity mContext;
    private List<Doctor> arr_users;
    ProgressDialog progressDialog;

    public DoctorAdapter(Activity context, List<Doctor> users) {
        this.mContext = context;
        this.arr_users = users;
        this.progressDialog = new ProgressDialog(mContext);
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.k_item_doctor, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        final Doctor doctor = arr_users.get(position);

        holder.opponentName.setText(doctor.getDoctor_name());
        holder.txt_chuyennganh.setText(showDoctorClinic(doctor));
        Glide.with(mContext).load(doctor.getProfile_image()).error(R.drawable.ic_doctor).into(holder.opponentIcon);
//        if (doctor.getFeatured() == 1) {
//            holder.img_status.setVisibility(View.VISIBLE);
//        } else {
//            holder.img_status.setVisibility(View.INVISIBLE);
//        }
        // holder.ratingBar.setRating(doctor.getVote());
        holder.ratingBar.setText("" + doctor.getVote());
        // holder.ratingBar.setIsIndicator(true);
        holder.layout_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showAction(user, finalLink_profile);
                progressDialog.setMessage(mContext.getString(R.string.notifi_connection_with_chat) + " " + doctor.getDoctor_name());
                progressDialog.show();
//                Performer<QBUser> qbUser = QBUsers.getUserByExternalId(doctor.getUser_id());
//                qbUser.performAsync(new QBEntityCallback<QBUser>() {
//                    @Override
//                    public void onSuccess(QBUser qbUser, Bundle bundle) {
                CommonUtils.GetUserServer(doctor.getUser_id(), new ResultUser() {
                    @Override
                    public void onResult(QBUser qbUser) {
                        progressDialog.dismiss();
                        CallUltils.checkToCall(qbUser, mContext, Consts.TYPE_CHAT);
                        qbUser.setWebsite("https://medixlink.com/danh-sach-bac-si-chi-tiet/"+doctor.getDoctor_id());
                        SharedPrefsHelper.getInstance().saveOpponentQbUser(qbUser);
                    }
                });

//                    }
//
//                    @Override
//                    public void onError(QBResponseException e) {
//                        progressDialog.dismiss();
//                        CommonUtils.showToast(mContext, doctor.getDoctor_name() + " " + mContext.getString(R.string.error_found_user_on_qb));
//                        Log.e("ERROR_QB", e.getMessage().toUpperCase());
//                    }
//
//                });
            }
        });

    }

    private String showDoctorClinic(Doctor doctor) {
        String result = "";
        if (doctor.getDoctor_clinic() != null && !doctor.getDoctor_clinic().equalsIgnoreCase("")) {
            result = doctor.getSpecialitys() + " - " + doctor.getDoctor_clinic();
        } else {
            result =  doctor.getSpecialitys();
        }
        result = result.replaceAll("<ul>", "");
        result = result.replaceAll("</ul>", "");
        result = result.replaceAll("<li>", "");
        result = result.replaceAll("</li>", "");
        return result;
    }

    @Override
    public int getItemCount() {
        return arr_users.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        CircleImageView opponentIcon;
        // ImageView img_status;
        TextView opponentName, txt_chuyennganh;
        CardView layout_holder;
        TextView ratingBar;

        public UserViewHolder(View view) {
            super(view);
            opponentIcon = view.findViewById(R.id.k_item_doctor_img_avatar_id);
            opponentName = view.findViewById(R.id.k_item_doctor_txt_name_id);
            // img_status = view.findViewById(R.id.img_status);
            txt_chuyennganh = view.findViewById(R.id.k_item_doctor_txt_specialist_id);
            layout_holder = view.findViewById(R.id.k_item_doctor_layout_holder_id);
            ratingBar = view.findViewById(R.id.k_item_doctor_txt_start_id);
        }
    }

}
