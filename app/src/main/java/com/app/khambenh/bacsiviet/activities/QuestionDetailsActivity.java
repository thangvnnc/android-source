package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.CommentAdapter;
import com.app.khambenh.bacsiviet.util.HttpUtils;
import com.app.khambenh.bacsiviet.util.MComment;
import com.app.khambenh.bacsiviet.util.MResults;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionDetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    private TextView _txtTitle = null;
    private TextView _txtContent = null;
    private Button _btnAddQuestion = null;
    private EditText _edtInputComment = null;
    private ImageView _imgImageQA = null;
    private RecyclerView _rvComment = null;
    private List<MComment> _comments = null;
    private CommentAdapter _commentAdapter = null;
    private long _questionId = 0;
    private String _questionTitle = "";
    private String _questionContent = "";
    private String _specialityId = "";
    private Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_question_details);

        _questionId = getIntent().getLongExtra("question_id", 0);
        _questionTitle = getIntent().getStringExtra("question_title");
        _questionContent = getIntent().getStringExtra("question_content");
        _specialityId = getIntent().getStringExtra("speciality_id");

        init();
        getData();
    }

    /**
     * Ánh xạ và khởi tạo
     */
    private void init()
    {
        _txtTitle = findViewById(R.id.activity_question_details_txt_title_id);
        _txtContent = findViewById(R.id.activity_question_details_txt_content_id);
        _btnAddQuestion = findViewById(R.id.activity_question_details_btn_new_question_id);
        _edtInputComment = findViewById(R.id.activity_question_details_edt_input_comment_id);
        _rvComment = findViewById(R.id.activity_question_details_rv_list_comment_id);
        _imgImageQA = findViewById(R.id.activity_question_details_img_id);

        Picasso.with(this)
                .load("https://medixlink.com/public/images/"+_questionId)
                .into(_imgImageQA);

        _txtTitle.setText(_questionTitle);
        _txtContent.setText(_questionContent);
        _comments = new ArrayList<>();
        _commentAdapter = new CommentAdapter(this, _comments);
        _rvComment.setLayoutManager(new LinearLayoutManager(this));
        _rvComment.setAdapter(_commentAdapter);

        _btnAddQuestion.setOnClickListener(this);
        _edtInputComment.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    if (event.getRawX() >= (_edtInputComment.getRight() - _edtInputComment.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                    {
                        insertComment();
                        return true;
                    }
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View view)
    {
        int id = view.getId();
        switch (id)
        {
            case R.id.activity_question_details_btn_new_question_id:
                startActivity(new Intent(QuestionDetailsActivity.this, AddQuestionActivity.class));
                break;
        }
    }

    private void getData()
    {
        _comments.clear();
        _commentAdapter.notifyDataSetChanged();

        Map<String, String> params = new HashMap<>();
        params.put("question_id", String.valueOf(_questionId));

        HttpUtils httpUtils = new HttpUtils(this, new HttpUtils.VolleyCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                if (result == null || result.equals(""))
                {
                    return;
                }
                Gson gson = new Gson();
                List<MComment> comments = gson.fromJson(result, new TypeToken<List<MComment>>()
                {
                }.getType());

                if (comments == null || comments.size() == 0)
                {
                    return;
                }
                _comments.addAll(comments);
                _commentAdapter.notifyDataSetChanged();
            }
        });
        httpUtils.call(URLUtils.URL_LIST_COMMENT, params);
    }

    private void insertComment()
    {
        String content = _edtInputComment.getText().toString().trim();
        if (content.equals(""))
        {
            return;
        }

        Integer userId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);
        Map<String, String> params = new HashMap<>();
        params.put("answer_user_id", String.valueOf(userId));
        params.put("question_id", String.valueOf(_questionId));
        params.put("answer_content", content);
        params.put("speciality_id", _specialityId);

        HttpUtils httpUtils = new HttpUtils(this, new HttpUtils.VolleyCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                if (result == null || result.equals(""))
                {
                    return;
                }
                Gson gson = new Gson();
                MResults results = gson.fromJson(result, new TypeToken<MResults>()
                {
                }.getType());

                if (results == null || results.getCode() != 0)
                {
                    Toast.makeText(QuestionDetailsActivity.this, results.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                getData();
                _edtInputComment.setText("");
                hideKeyboard(QuestionDetailsActivity.this);
                _rvComment.scrollToPosition(_comments.size() - 1);
            }
        });
        httpUtils.call(URLUtils.URL_INSERT_COMMENT, params);
    }

    public static void hideKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null)
        {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
