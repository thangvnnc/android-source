package com.app.khambenh.bacsiviet.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.stringee.service.StringeeService;
import com.app.khambenh.bacsiviet.util.MessageEvent;
import com.app.khambenh.bacsiviet.util.UserDetail;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.ConnectivityUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.KeyboardUtils;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.Toaster;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import com.app.khambenh.bacsiviet.services.CallService;

public class LoginActivity extends BaseActivity {

    private String TAG = LoginActivity.class.getSimpleName();

    private EditText userNameEditText, passWord;
    private QBUser userForSave;
    private TextView txtFocusUserName;
    private TextView txtFocusPassword;
    Button btLogin;
    private Button btLoginFace = null;
    SharedPreferences pre;
    private LinearLayout call_hotline;
    CallbackManager callbackManager;
    private TextView txtRegister;
    private TextView txtVersion;
    String regex = "[a-zA-Z0-9.\\-_]{3,}";

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.k_activity_login);
        pre = getSharedPreferences("app_bacsi", MODE_PRIVATE);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initUI();
        loginFacebook();
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {/* Do something */
        if (event != null) {
            try {
                userNameEditText.setText(event.getUserName());
                passWord.setText(event.getPassWord());
                processLogin(event.getUserName(), event.getPassWord());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.k_activity_login_root_view_id);
    }

    private void initUI() {
//        setActionBarTitle(R.string.title_login_activity);

        userNameEditText = findViewById(R.id.k_activity_login_edt_user_name_id);
        passWord = findViewById(R.id.k_activity_login_edt_password_id);
        txtFocusUserName = findViewById(R.id.k_activity_login_txt_user_name_id);
        txtFocusUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameEditText.requestFocus();
            }
        });
        txtFocusPassword = findViewById(R.id.k_activity_login_txt_password_id);
        txtFocusPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passWord.requestFocus();
            }
        });
        userNameEditText.addTextChangedListener(new LoginEditTextWatcher(userNameEditText));
        btLogin = (Button) findViewById(R.id.k_activity_login_btn_login_id);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEnteredUserNameValid()) {
                    hideKeyboard();
                    try {
                        processLogin(userNameEditText.getText().toString(), passWord.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        txtRegister = findViewById(R.id.k_activity_login_btn_signup_id);
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, InputPhone.class));
            }
        });
        btLoginFace = findViewById(R.id.k_activity_login_btn_login_face_id);
        txtVersion = findViewById(R.id.k_activity_login_txt_version);
        txtVersion.setText("Version: " + getCurrenVersion());
    }

    private String getCurrenVersion() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        return currentVersion;
    }

    private void loginFacebook() {
        callbackManager = CallbackManager.Factory.create();
        // LoginButton loginButton = findViewById(R.id.login_button);
        final LoginButton loginButton = new LoginButton(this);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Login success
                // Get UserName - ID
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject jsonObject, GraphResponse response) {
                                Log.v("LoginFB", jsonObject.toString());
                                if (jsonObject != null) {
                                    Log.v("LoginFB2", jsonObject.toString());
                                    checkAccountFB(jsonObject);
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                if (ConnectivityUtils.isNetworkConnected()) {
                    CommonUtils.SaveLLog(LoginActivity.this, "", "Login fb cancel", Consts.TAG_LOGIN_ACTIVITY);
                } else {
                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                }
                // CoreApp code
            }

            @Override
            public void onError(FacebookException exception) {
                Log.v("LoginFB2", exception.toString());

                // CoreApp code
                if (ConnectivityUtils.isNetworkConnected()) {
                    CommonUtils.SaveLLog(LoginActivity.this, "", exception.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                } else {
                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                }
            }
        });

        btLoginFace.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                loginButton.performClick();
            }
        });
    }

    private void processLogin(final String userName, final String passWord) throws JSONException {
        showProgressDialog(R.string.dlg_login);
        JSONObject postparams = new JSONObject();
        postparams.put("email", "" + userName);
        postparams.put("pwd", "" + passWord);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URLUtils.URL_LOGIN_MOBILE, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                if (response.getString("isLogin").equals("true")) {
                                    QBUser qbUser = new QBUser(userName, Consts.DEFAULT_USER_PASSWORD);
                                    String fullname = response.getString("fullname");
                                    qbUser.setFullName(fullname);
                                    List<String> userTags = new ArrayList<>();
                                    String type = response.getString("user_type").toLowerCase();
                                    userTags.add(type);
                                    userTags.add("Android");
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_TYPE, type);
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_FULLNAME, fullname);
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_NAME, userName);
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_PAID, response.getInt("paid"));
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_LOGIN_FB, false);
                                    if (response.has("unit")) {
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_UNIT, response.getLong("unit"));
                                    }
                                    if (response.has("link")) {
                                        qbUser.setWebsite(response.getString("link"));
                                    }
                                    JSONObject jsUser = response.getJSONObject("user");

                                    long balance = response.getLong("balance");
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_BALANCE, balance);

                                    if (type.equals("user")) {
                                        if (response.has("user")) {
                                            UserDetail userDetail = new Gson().fromJson(jsUser.toString(), UserDetail.class);
                                            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_DETAIL, userDetail);
                                            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_ID, jsUser.getInt("user_id"));
                                            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_AVATAR, jsUser.getString("avatar"));
                                            qbUser.setId(jsUser.getInt("user_id"));
                                            qbUser.setFullName(jsUser.getString("fullname"));
                                            qbUser.setLogin(jsUser.getString("email"));
                                            qbUser.setCustomData(jsUser.getString("avatar"));
                                            qbUser.setPhone(jsUser.getString("phone"));
                                            qbUser.setFacebookId(jsUser.getString("id_facebook"));
                                            qbUser.setExternalId(String.valueOf(jsUser.getInt("user_id")));
                                            int gender = jsUser.getInt("gender");
                                            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_GENDER, gender);
                                            if (gender == Consts.TYPE_MAN) {
                                                userTags.add("MALE");
                                            } else if (gender == Consts.TYPE_WOMEN) {
                                                userTags.add("FEMALE");
                                            } else {
                                                userTags.add("OTHER");
                                            }

                                        }

                                    } else {
                                        String avatar_doctor = response.getString("image");
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_AVATAR, avatar_doctor);
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_ID, jsUser.getInt("user_id"));
                                        qbUser.setId(jsUser.getInt("user_id"));
                                        qbUser.setFullName(jsUser.getString("fullname"));
                                        qbUser.setLogin(jsUser.getString("email"));
                                        qbUser.setPhone(jsUser.getString("phone"));
                                        qbUser.setFacebookId(jsUser.getString("id_facebook"));
                                        qbUser.setCustomData(fullname + "-_-" + avatar_doctor);
                                        qbUser.setExternalId(String.valueOf(jsUser.getInt("user_id")));
                                    }
                                    qbUser.setTags(userTags);
                                    checkQBUserByLogin(userName, qbUser);
                                    String topics = "" + jsUser.getInt("user_id");
                                    FirebaseMessaging.getInstance().subscribeToTopic(topics);
                                } else {
                                    hideProgressDialog();
                                    if (ConnectivityUtils.isNetworkConnected()) {
                                        Toast.makeText(LoginActivity.this, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                        CommonUtils.SaveLLog(LoginActivity.this, "", response.getString("msg"), Consts.TAG_LOGIN_ACTIVITY);
                                    } else {
                                        CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                                    }

                                }
                            } catch (JSONException e) {
                                if (ConnectivityUtils.isNetworkConnected()) {
//                                    Toast.makeText(LoginActivity.this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
                                    CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                                } else {
                                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                                }
                                hideProgressDialog();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        if (ConnectivityUtils.isNetworkConnected()) {
                            CommonUtils.SaveLLog(LoginActivity.this, "", error.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                        } else {
                            CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                        }
                        //Failure Callback

                    }
                });

// Adding the request to the queue along with a unique string tag
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

    }

    private void checkAccountFB(final JSONObject object) {
        try {
            JSONObject postparams = new JSONObject();
            postparams.put("idfacebook", "" + object.getString("id"));
            Log.v("LoginFB3", "" + object.getString("id"));

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_CHECK_ACCOUNT, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            boolean isExist = response.getBoolean("exist");
                            Log.v("LoginFB4", "" + response.toString());
                            if (isExist) {
                                processLoginwithFB(object, null);
                            } else {
                                showDialogCheckCode(object);
                            }
                        } catch (JSONException e) {
                            Log.v("LoginFB5", "" + e.toString());
                            if (ConnectivityUtils.isNetworkConnected()) {
                                CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                            } else {
                                CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                            }
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    e.printStackTrace();
                    Log.v("LoginFB6", "" + e.toString());

                    if (ConnectivityUtils.isNetworkConnected()) {
                        CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                    } else {
                        CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                    }
                    //Failure Callback
                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            Log.v("LoginFB7", "" + e.toString());
            e.printStackTrace();
            if (ConnectivityUtils.isNetworkConnected()) {
                CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
            } else {
                CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
            }
        }

    }

    private void processLoginwithFB(final JSONObject object, String checkcode) {
        showProgressDialog(R.string.dlg_login);
        try {
            Log.v("LoginFB8", "" + object.getString("id"));
            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_FACEBOOK_ID, object.getString("id"));
            JSONObject postparams = new JSONObject();
            postparams.put("username", "" + object.getString("id"));
            postparams.put("fullname", "" + object.getString("name"));
            postparams.put("qwd", "" + object.getString("id"));
            if (checkcode != null) {
                if (!checkcode.trim().equals("")) {
                    postparams.put("presenter", "" + checkcode);
                }
            }

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_APP_FABOOK_API, postparams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response != null) {
                                try {
                                    if (response.getBoolean("isLogin")) {
                                        JSONObject jsUser = response.getJSONObject("user");
                                        UserDetail userDetail = new Gson().fromJson(jsUser.toString(), UserDetail.class);
                                        userDetail.setFullname(object.getString("name"));
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_DETAIL, userDetail);

                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_ID, jsUser.getInt("user_id"));
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_NAME, object.getString("id"));
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_FULLNAME, object.getString("name"));

                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_PAID, response.getInt("paid"));
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_LOGIN_FB, true);
                                        if (response.has("unit")) {
                                            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_UNIT, response.getLong("unit"));
                                        }

                                        int gender = jsUser.getInt("gender");
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_GENDER, gender);

                                        String type = response.getString("user_type").toLowerCase();
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_TYPE, type);

                                        final QBUser qbUser = new QBUser(object.getString("id"), Consts.DEFAULT_USER_PASSWORD);
                                        qbUser.setId(jsUser.getInt("user_id"));
                                        qbUser.setLogin(jsUser.getString("email"));
                                        qbUser.setFullName(object.getString("name"));
                                        List<String> userTags = new ArrayList<>();
                                        userTags.add(response.getString("user_type").toLowerCase());
                                        userTags.add("Android");
                                        qbUser.setPhone(jsUser.getString("phone"));
                                        qbUser.setFacebookId(object.getString("id"));
                                        qbUser.setTags(userTags);
                                        if (gender == Consts.TYPE_MAN) {
                                            userTags.add("MALE");
                                        } else if (gender == Consts.TYPE_WOMEN) {
                                            userTags.add("FEMALE");
                                        } else {
                                            userTags.add("OTHER");
                                        }
                                        URL url = extractFacebookIcon(object.getString("id"));

                                        if (type.equals("user")) {
                                            long balance = response.getLong("balance");
                                            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_BALANCE, balance);
                                            String avatar_user = jsUser.getString("avatar");
                                            if (avatar_user != null && !avatar_user.equals("")) {
                                                SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_AVATAR, avatar_user);
                                                qbUser.setCustomData(avatar_user);
                                            } else {
                                                SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_AVATAR, url.toString());
                                                qbUser.setCustomData(url.toString());
                                            }
                                        } else {
                                            String avatar_doctor = response.getString("image");
                                            if (avatar_doctor != null && !avatar_doctor.equals("")) {
                                                SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_AVATAR, avatar_doctor);
                                                qbUser.setCustomData(avatar_doctor);
                                            } else {
                                                SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_AVATAR, url.toString());
                                                qbUser.setCustomData(url.toString());
                                            }

                                        }
                                        qbUser.setExternalId(String.valueOf(jsUser.getInt("user_id")));
                                        checkQBUserByLogin(object.getString("id"), qbUser);
//                                        String topics = "" + jsUser.getInt("user_id");
//                                        FirebaseMessaging.getInstance().subscribeToTopic(topics);
                                    } else {
                                        if (ConnectivityUtils.isNetworkConnected()) {
                                            CommonUtils.SaveLLog(LoginActivity.this, "",response.getString("msg"), Consts.TAG_LOGIN_ACTIVITY);
                                        } else {
                                            CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                                        }
//                                        Toast.makeText(LoginActivity.this, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    Log.v("LoginFB9", "" + e.getMessage());

                                    e.printStackTrace();
                                    if (ConnectivityUtils.isNetworkConnected()) {
                                        CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                                    } else {
                                        CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                                    }
                                    Log.d("ERROR", "" + e.getMessage());
                                    hideProgressDialog();
                                }
                            }
                        }


                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            e.printStackTrace();
                            Log.v("LoginFB10", "" + e.getMessage());
                            if (ConnectivityUtils.isNetworkConnected()) {
                                CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                            } else {
                                CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                            }
                            hideProgressDialog();

                            //Failure Callback

                        }
                    });

// Adding the request to the queue along with a unique string tag
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

        } catch (JSONException e) {
            Log.v("LoginFB11", "" + e.getMessage());

            if (ConnectivityUtils.isNetworkConnected()) {
                CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
            } else {
                CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
            }
            e.printStackTrace();
        }

    }

    public URL extractFacebookIcon(String id) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL imageURL = new URL("http://graph.facebook.com/" + id
                    + "/picture?type=large");
            return imageURL;
        } catch (Throwable e) {
            if (ConnectivityUtils.isNetworkConnected()) {
                CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
            } else {
                CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
            }
            return null;
        }
    }

    private void checkQBUserByLogin(final String userName, final QBUser qbUserRegister) {
//        requestExecutor.getUserByLogin(userName, new QBEntityCallback<QBUser>() {
//            @Override
//            public void onSuccess(final QBUser user, Bundle bundle) {
//                qbUserRegister.setId(user.getId());
//                qbUserRegister.setOldPassword(Consts.DEFAULT_USER_PASSWORD);
//                QBUsers.signIn(qbUserRegister).performAsync(new QBEntityCallback<QBUser>() {
//                    @Override
//                    public void onSuccess(QBUser qbUse, Bundle args) {
//                        // success
//                        requestExecutor.updateUser(qbUserRegister, new QBEntityCallback<QBUser>() {
//                            @Override
//                            public void onSuccess(QBUser updateUser, Bundle bundle) {
                                loginToChat(qbUserRegister);
//                            }
//
//                            @Override
//                            public void onError(QBResponseException e) {
//                                loginToChat(qbUserRegister);
//                                Log.d("check_By_Login_Error", "" + e.getMessage());
//                                if (ConnectivityUtils.isNetworkConnected()) {
//                                    CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
//                                } else {
//                                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
//                                }
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onError(QBResponseException e) {
//                        // error
//                        Log.d("check_By_Login_Error", "" + e.getMessage());
//                        if (ConnectivityUtils.isNetworkConnected()) {
//                            CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
//                        } else {
//                            CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
//                        }
//                    }
//                });
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Log.d("check_By_Login_Error", "" + e.getMessage());
//                if (ConnectivityUtils.isNetworkConnected()) {
////                    CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
//                } else {
//                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
//                }
//                QBUsers.signOut();
//                signUpQBUser(qbUserRegister);
//            }
//        });
    }

//    private void checkQBUserByFullName(final String username, final QBUser qbUserRegister) {
//        requestExecutor.loadUsersByFullname(username, new QBEntityCallback<ArrayList<QBUser>>() {
//            @Override
//            public void onSuccess(ArrayList<QBUser> qbUserArrayList, Bundle bundle) {
//                if (qbUserArrayList.size() != 0) {
//                    ArrayList<QBUser> arrQbUsers = new ArrayList<>();
//                    for (int i = 0; i < qbUserArrayList.size(); i++) {
//                        final QBUser qbUser = qbUserArrayList.get(i);
//                        if (qbUser.getFullName().equals(username)) {
//                            arrQbUsers.add(qbUser);
//                        }
//                    }
//                    if (arrQbUsers.size() == 0) {
//                        signUpQBUser(qbUserRegister);
//                    } else {
//                        final QBUser qbUser = arrQbUsers.get(0);
////                        qbUser.getLogin();
//                        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
//                        loginToChat(qbUser);
//                    }
//                }
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Log.d("check_By_Fullname_Error", "" + e.getMessage());
//                signUpQBUser(qbUserRegister);
//            }
//        });
//    }

//    private void signUpQBUser(final QBUser qbUser) {
//        QBUsers.signUp(qbUser).performAsync(new QBEntityCallback<QBUser>() {
//            @Override
//            public void onSuccess(QBUser user, Bundle args) {
                // success
//                loginToChat(qbUser);
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                // error
//                hideProgressDialog();
//                Log.d("SignUp_Error", "" + e.getMessage());
//                if (ConnectivityUtils.isNetworkConnected()) {
//                    CommonUtils.SaveLLog(LoginActivity.this, "", e.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
//                } else {
//                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
//                }
//            }
//        });
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.menu_login_user_done:
//                if (isEnteredUserNameValid()) {
//                    hideKeyboard();
//                    try {
//                        processLogin(userNameEditText.getText().toString(), passWord.getText().toString());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                return true;

            default:

                return super.onOptionsItemSelected(item);
        }
    }


    private boolean isEnteredUserNameValid() {
        if (userNameEditText != null && !userNameEditText.getText().equals("")) {
            return true;
        }
        return false;
    }

    private void hideKeyboard() {
        KeyboardUtils.hideKeyboard(userNameEditText);

    }

    private void showDialogCheckCode(final JSONObject jsonObject) {
        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.setTitle(getString(R.string.title_note));
        dialog.setContentView(R.layout.dialog_check_code);
        dialog.setCancelable(false);

        final EditText editCheckCode = (EditText) dialog.findViewById(R.id.edit_check_code);

        Button btnSkip = (Button) dialog.findViewById(R.id.btn_skip);
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processLoginwithFB(jsonObject, null);
                dialog.dismiss();
            }
        });

        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processLoginwithFB(jsonObject, editCheckCode.getText().toString());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

//    private void startSignUpNewUser(final QBUser newUser) {
//        showProgressDialog(R.string.dlg_creating_new_user);
//        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
//                    @Override
//                    public void onSuccess(QBUser result, Bundle params) {
//                        loginToChat(result);
//                    }
//
//                    @Override
//                    public void onError(QBResponseException e) {
//                        Log.d("error_s", "" + e.getMessage());
//                        if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
//                            signInCreatedUser(newUser, true);
//                        } else {
//                            hideProgressDialog();
//                            Toaster.longToast(R.string.sign_up_error);
//                        }
//                    }
//                }
//        );
//    }


    private void loginToChat(final QBUser qbUser) {
        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
        userForSave = qbUser;
        startLoginService(qbUser);

    }

    private void startOpponentsActivity() {
        OpponentsActivity.start(LoginActivity.this, false);
        finish();
    }

    private void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, qbUser.getTags().get(0));
        sharedPrefsHelper.saveQbUser(qbUser);
    }

//    private QBUser createUserWithEnteredData(String type_user, String custom_data) {
//        if (type_user.equals("doctor") || type_user.equals("clinic")) {
//            return createQBUserWithCurrentData(String.valueOf(userNameEditText.getText()), "doctor", custom_data);
//        }
//        return createQBUserWithCurrentData(String.valueOf(userNameEditText.getText()), "user", custom_data);
//    }

//    private QBUser createQBUserWithCurrentData(String userName, String chatRoomName, String custom_data) {
//        QBUser qbUser = null;
//        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(chatRoomName)) {
//            List<String> userTags = new ArrayList<>();
//            userTags.add(chatRoomName);
//
//            qbUser = new QBUser();
//            qbUser.setFullName(userName);
//            qbUser.setCustomData(custom_data);
//            qbUser.setFacebookId(pre.getString("user_id", "" + new Random().nextInt(1000000) + new Random().nextInt(100000)));
//            qbUser.setLogin(getCurrentDeviceId());
//            qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
//            qbUser.setTags(userTags);
//        }
//
//        return qbUser;
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Consts.EXTRA_LOGIN_RESULT_CODE) {
//            hideProgressDialog();
            boolean isLoginSuccess = data.getBooleanExtra(Consts.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(Consts.EXTRA_LOGIN_ERROR_MESSAGE);

            if (isLoginSuccess) {
                saveUserData(userForSave);
                startOpponentsActivity();
            } else {
                if (ConnectivityUtils.isNetworkConnected()) {
                    CommonUtils.SaveLLog(LoginActivity.this, "", errorMessage, Consts.TAG_LOGIN_ACTIVITY);
                } else {
                    CommonUtils.showToast(LoginActivity.this, getString(R.string.error_connection_faild));
                }
                Toaster.longToast(getString(R.string.login_chat_login_error) + errorMessage);
                userNameEditText.setText(userForSave.getFullName());
                //chatRoomNameEditText.setText(userForSave.getTags().get(0));
            }
        }
    }

//    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
//        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
//            @Override
//            public void onSuccess(QBUser result, Bundle params) {
//                if (deleteCurrentUser) {
//                    removeAllUserData(result);
//                } else {
//                    startOpponentsActivity();
//                }
//            }
//
//            @Override
//            public void onError(QBResponseException responseException) {
//                hideProgressDialog();
//                Toaster.longToast(R.string.sign_up_error);
//            }
//        });
//    }

//    private void removeAllUserData(final QBUser user) {
//        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
//            @Override
//            public void onSuccess(Void aVoid, Bundle bundle) {
//                UsersUtils.removeUserData(getApplicationContext());
//                startSignUpNewUser(createUserWithEnteredData(pre.getString("user_type", "user"), pre.getString("custom_data", "User ")));
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Log.d("error_s", "" + e.getMessage());
//                hideProgressDialog();
//                Toaster.longToast(R.string.sign_up_error);
//            }
//        });
//    }

    private void startLoginService(QBUser qbUser) {
        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_IS_LOGIN, true);
//        SubscribeService.subscribeToPushes(this, true);
//        Intent tempIntent = new Intent(this, CallService.class);
//        PendingIntent pendingIntent = createPendingResult(Consts.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
//        startActivity(this, qbUser, pendingIntent);
        saveUserData(userForSave);
        startOpponentsActivity();
        if (StringeeService.isRunning(this)) {
            String idUser = SharedPrefsHelper.getInstance().getQbUser().getId()+"";
            StringeeService.getInstance().connect(idUser);
        }
    }

//    private String getCurrentDeviceId() {
//        return Utils.generateDeviceId(this);
//    }

    private class LoginEditTextWatcher implements TextWatcher {
        private EditText editText;

        private LoginEditTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            editText.setError(null);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
