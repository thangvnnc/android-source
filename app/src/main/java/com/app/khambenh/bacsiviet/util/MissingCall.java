package com.app.khambenh.bacsiviet.util;

public class MissingCall {
    private int currUserID;
    private int opponentID;
    private Long Time;
    private boolean isMissingCall;

    public int getCurrUserID() {
        return currUserID;
    }

    public void setCurrUserID(int currUserID) {
        this.currUserID = currUserID;
    }

    public Long getTime() {
        return Time;
    }

    public void setTime(Long time) {
        Time = time;
    }

    public boolean isMissingCall() {
        return isMissingCall;
    }

    public void setMissingCall(boolean missingCall) {
        isMissingCall = missingCall;
    }

    public int getOpponentID() {
        return opponentID;
    }

    public void setOpponentID(int opponentID) {
        this.opponentID = opponentID;
    }
}
