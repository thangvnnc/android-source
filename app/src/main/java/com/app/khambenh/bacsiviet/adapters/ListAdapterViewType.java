package com.app.khambenh.bacsiviet.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.ItemViewType;

import java.util.List;

public class ListAdapterViewType extends ArrayAdapter<ItemViewType> {
    private LayoutInflater mInflater;

    public ListAdapterViewType(Context context, List<ItemViewType> items) {
        super(context, 0, items);
        mInflater = LayoutInflater.from(context);
    }

    public enum RowType {
        LIST_ITEM, HEADER_ITEM
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        RecyclerView.ViewHolder holder = null;

        View View = null;

        try{
            View = getItem(position).getView(getContext(), mInflater, convertView, position);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return View;
    }

    @Override
    public ItemViewType getItem(int position) {
        return super.getItem(position);
    }

    public static class ViewHolder {
        public View View;
    }
}
