package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.LocaleUtils;
import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Speciality implements Serializable, Item {
    @SerializedName("speciality_id")
    private int speciality_id;

    @SerializedName("speciality_name")
    private String speciality_name;

    @SerializedName("en_speciality_name")
    private String en_speciality_name;

    @SerializedName("fr_speciality_name")
    private String fr_speciality_name;

    @SerializedName("speciality_url")
    private String speciality_url;

    @SerializedName("speciality_image")
    private String speciality_image;

    @SerializedName("speciality_desc")
    private String speciality_desc;


    public int getSpeciality_id() {
        return speciality_id;
    }

    public void setSpeciality_id(int speciality_id) {
        this.speciality_id = speciality_id;
    }

    public String getSpeciality_name() {
        return speciality_name;
    }

    public void setSpeciality_name(String speciality_name) {
        this.speciality_name = speciality_name;
    }

    public String getSpeciality_url() {
        return speciality_url;
    }

    public void setSpeciality_url(String speciality_url) {
        this.speciality_url = speciality_url;
    }

    public String getSpeciality_image() {
        return speciality_image;
    }

    public void setSpeciality_image(String speciality_image) {
        this.speciality_image = speciality_image;
    }

    public String getSpeciality_desc() {
        return speciality_desc;
    }

    public void setSpeciality_desc(String speciality_desc) {
        this.speciality_desc = speciality_desc;
    }

    @Override
    public View getView(Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.k_item_specialist, null);
        } else {
            rowView = convertView;
        }
        ImageView imgAvatar = (ImageView) rowView.findViewById(R.id.k_item_specialist_img_id);
        TextView txt_name = (TextView) rowView.findViewById(R.id.k_item_specialist_title_id);
        // ImageView img_status = (ImageView) rowView.findViewById(R.id.img_status);
        // img_status.setVisibility(View.GONE);

        Glide.with(context).load(getSpeciality_image()).error(R.color.blue).into(imgAvatar);
        String lang = LocaleUtils.getCurrentLocale(context).getLanguage();
        if (lang.equals("en")) {
            txt_name.setText(getEn_speciality_name());
        } else if (lang.equals("fr")) {
            txt_name.setText(getFr_speciality_name());
        } else {
            txt_name.setText(getSpeciality_name());
        }
        return rowView;
    }

    public String getFr_speciality_name() {
        return fr_speciality_name;
    }

    public void setFr_speciality_name(String fr_speciality_name) {
        this.fr_speciality_name = fr_speciality_name;
    }

    public String getEn_speciality_name() {
        return en_speciality_name;
    }

    public void setEn_speciality_name(String en_speciality_name) {
        this.en_speciality_name = en_speciality_name;
    }
}
