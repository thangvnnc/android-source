package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.stringee.service.StringeeService;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.ConnectivityUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.LanguageUtils;
import com.app.khambenh.bacsiviet.utils.QBChatService;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.app.khambenh.bacsiviet.utils.VersionChecker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//import com.app.khambenh.bacsiviet.services.CallService;

public class SplashActivity extends Activity {

    private Activity activity = this;
    private SharedPrefsHelper sharedPrefsHelper;
    String appVersion;
    String storeVersion;
    VersionChecker versionChecker;
    boolean gps_enable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageUtils.loadLocale(this);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        gps_enable = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gps_enable) {
            runnable.run();
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        } else {
            if (ConnectivityUtils.isNetworkConnected()) {
                Bundle bundle = getIntent().getExtras();
                Log.e("bundle", "" + bundle);
                if (bundle != null) {
                    String senderId = bundle.getString("senderId");
                    Log.e("senderId", "" + senderId);
                    if (senderId != null) {
                        Intent intent = new Intent(this, OpponentsActivity.class);
                        intent.putExtra(Consts.EXTRA_USER_ID, senderId);
                        startActivity(intent);
                    } else {
                        start();
                    }
                } else {
                    start();
                }
            } else {
                CommonUtils.dialogNotConnecction(SplashActivity.this);
            }
        }

        printKeyHash(this);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        return super.onCreateDialog(id);
    }




    protected void startLoginService(QBUser qbUser) {
        if (StringeeService.isRunning(this)) {
            StringeeService.getInstance().connect(qbUser.getId()+"");
        }
//        if (StringeeService.isRunning(this)) {
//            String idUser = QBChatService.getInstance().getUser().getId() + "";
//            StringeeService.getInstance().connect(idUser);
//        }
//        CallService.start(this, qbUser);
    }

    private void startOpponentsActivity() {
        OpponentsActivity.start(SplashActivity.this, false);
        finish();
    }

    private void startLoginsActivity() {
        LoginActivity.start(SplashActivity.this);
        finish();
    }


    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    private void showdialogUpdateVersion() {
        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.setTitle(getString(R.string.title_note));
        dialog.setContentView(R.layout.dialog_update_version);
        dialog.setCancelable(false);
        TextView txtMessDialog = (TextView) dialog.findViewById(R.id.txtMessDialog);
        String str = getString(R.string.notifi_your_version) + " " + getCurrenVersion() + ". " + getString(R.string.notifi_update_your_version);
        txtMessDialog.setText(str);

        Button btnLater = (Button) dialog.findViewById(R.id.btn_update_later);
        Button btnUpdate = (Button) dialog.findViewById(R.id.btn_update);
        btnLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                versionChecker.showForceUpdateDialog();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
        dialog.show();
    }

    private String getCurrenVersion() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        return currentVersion;
    }

    private void start() {
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        checkVersionUpdate(new VersionUpdate() {
            @Override
            public void update(boolean isUpdate) {
                if (isUpdate == true) {
                    showdialogUpdateVersion();
                    return;
                }
                if (SharedPrefsHelper.getInstance().getQbUser() != null) {
                    startLoginService(sharedPrefsHelper.getQbUser());
                    startOpponentsActivity();
                    return;
                } else {
                    startLoginsActivity();
                }

                if (checkConfigsWithSnackebarError()) {
                    proceedToTheNextActivityWithDelay();
                }
            }
        });
    }
    private static Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    private static final int SPLASH_DELAY = 1500;

    protected void proceedToTheNextActivityWithDelay() {
        mainThreadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                proceedToTheNextActivity();
            }
        }, SPLASH_DELAY);
    }

    protected boolean checkConfigsWithSnackebarError(){
        if (!sampleConfigIsCorrect()){
            showSnackbarErrorParsingConfigs();
            return false;
        }

        return true;
    }

    protected void showSnackbarErrorParsingConfigs(){
    }

    protected boolean sampleConfigIsCorrect(){
        return true;
    }

    private interface VersionUpdate {
        void update(boolean isUpdate);
    }

    private void checkVersionUpdate(final VersionUpdate versionUpdate) {
        appVersion = getCurrenVersion();
        JSONObject postparams = new JSONObject();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URLUtils.URL_CHECK_VERSION_UPDATE, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("versionUpdate");
                                for(int idx = 0; idx < jsonArray.length(); idx++) {
                                    String version = jsonArray.getInt(idx)+"";
                                    if (appVersion.equalsIgnoreCase(version)) {
                                        versionUpdate.update(true);
                                        return;
                                    }
                                }
                                versionUpdate.update(false);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Kết nối server thất bại vui lòng kiểm tra lại mạng.
                        error.printStackTrace();
                    }
                });
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!gps_enable) {
                LanguageUtils.loadLocale(activity);
                LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                gps_enable = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                handler.postDelayed(this, 2000);
            } else {
                handler.removeCallbacks(this);
                start();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }
}