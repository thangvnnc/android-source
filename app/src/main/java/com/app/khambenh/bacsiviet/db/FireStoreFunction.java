package com.app.khambenh.bacsiviet.db;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class FireStoreFunction {
    static FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static FirebaseFirestore getDb() {
        return db;
    }

    public static void setDb(FirebaseFirestore db) {
        FireStoreFunction.db = db;
    }

    public static CollectionReference getData(String collectionName){
        return db.collection(collectionName);
    }
}
