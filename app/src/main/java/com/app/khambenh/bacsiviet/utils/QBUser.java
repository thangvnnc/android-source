package com.app.khambenh.bacsiviet.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class QBUser implements Serializable {
    protected Integer id;
    protected String email;
    protected String login;
    protected String phone;
    protected String website;
    protected Date lastRequestAt;
    protected String externalId;
    protected String facebookId;
    protected String twitterId;
    protected String twitterDigitsId;
    protected Integer blobId;
    protected List<String> tags;
    protected String password;
    protected String oldPassword;

    private String customData;


    public QBUser() {
    }

    public QBUser(String username, String password) {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomData() {
        return customData;
    }

    public void setCustomData(String customData) {
        this.customData = customData;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    protected String fullName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Date getLastRequestAt() {
        return lastRequestAt;
    }

    public void setLastRequestAt(Date lastRequestAt) {
        this.lastRequestAt = lastRequestAt;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getTwitterDigitsId() {
        return twitterDigitsId;
    }

    public void setTwitterDigitsId(String twitterDigitsId) {
        this.twitterDigitsId = twitterDigitsId;
    }

    public Integer getBlobId() {
        return blobId;
    }

    public void setBlobId(Integer blobId) {
        this.blobId = blobId;
    }

    public List<String>  getTags() {
        return tags;
    }

    public String converTagsToString() {

        return "";
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

}
