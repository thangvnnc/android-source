package com.app.khambenh.bacsiviet.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.khambenh.bacsiviet.CoreApp;

public class SharedPrefs {
    //shareprefs key word
    public static final String SHAREPREFS_USER_DETAIL = "SHAREPREFS_USER_DETAIL";

    public static final String SHAREPREFS_USER_AVATAR = "SHAREPREFS_USER_AVATAR";
    public static final String SHAREPREFS_USER_GENDER = "SHAREPREFS_USER_GENDER";
    public static final String SHAREPREFS_USER_ADDRESS = "SHAREPREFS_USER_ADDRESS";
    public static final String SHAREPREFS_USER_TYPE = "SHAREPREFS_USER_TYPE";
    public static final String SHAREPREFS_USER_PAID = "SHAREPREFS_USER_PAID";
    public static final String SHAREPREFS_USER_NAME = "SHAREPREFS_USER_NAME";
    public static final String SHAREPREFS_USER_FULLNAME = "SHAREPREFS_USER_FULLNAME";
    public static final String SHAREPREFS_USER_PASS = "SHAREPREFS_USER_PASS";
    public static final String SHAREPREFS_USER_ID = "SHAREPREFS_USER_ID";
    public static final String SHAREPREFS_USER_CUSTOM_DATA = "SHAREPREFS_USER_CUSTOM_DATA";
    public static final String SHAREPREFS_USER_PHONE = "SHAREPREFS_USER_PHONE";
    public static final String SHAREPREFS_USER_FACEBOOK_ID = "SHAREPREFS_USER_FACEBOOK_ID";
    public static final String SHAREPREFS_USER_PRESENT = "SHAREPREFS_USER_PRESENT";
    public static final String SHAREPREFS_BALANCE = "SHAREPREFS_BALANCE";
    public static final String SHAREPREFS_UNIT= "SHAREPREFS_UNIT";

    public static final String SHAREPREFS_SPECIALITY= "SHAREPREFS_SPECIALITY";

    public static final String SHAREPREFS_QUANTITY_WHEN_RECHARGE = "SHAREPREFS_QUANTITY_WHEN_RECHARGE";
    public static final String SHAREPREFS_LOGIN_FB = "SHAREPREFS_LOGIN_FB";
    public static final String SHAREPREFS_OPPONENT_AVATAR = "SHAREPREFS_OPPONENT_AVATAR";

    public static final String SHAREPREFS_DEVICE_TOKEN = "SHAREPREFS_DEVICE_TOKEN";

    public static final String SHAREPREFS_IS_LOGIN = "SHAREPREFS_IS_LOGIN";
    public static final String SHAREPREFS_COUNT_MESSAGE_WITH_DOCTOR_MAIL = "COUNT_MESSAGE_WITH_DOCTOR_MAIL_USERNAME_IS:";




    private static final String PREFS_NAME = "app_bacsi";
    public static final String LANGUAGE = "langauge";

    private static SharedPrefs mInstance;
    private SharedPreferences mSharedPreferences;

    private SharedPrefs() {
        mSharedPreferences = CoreApp.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPrefs getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPrefs();
        }
        return mInstance;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass) {
        if (anonymousClass == String.class) {
            return (T) mSharedPreferences.getString(key, "");
        } else if (anonymousClass == Boolean.class) {
            return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, false));
        } else if (anonymousClass == Float.class) {
            return (T) Float.valueOf(mSharedPreferences.getFloat(key, 0));
        } else if (anonymousClass == Integer.class) {
            return (T) Integer.valueOf(mSharedPreferences.getInt(key, 0));
        } else if (anonymousClass == Long.class) {
            return (T) Long.valueOf(mSharedPreferences.getLong(key, 0));
        } else {
            return (T) CoreApp.getInstance()
                    .getGSon()
                    .fromJson(mSharedPreferences.getString(key, ""), anonymousClass);
        }
    }

    public <T> void put(String key, T data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        if (data instanceof String) {
            editor.putString(key, (String) data);
        } else if (data instanceof Boolean) {
            editor.putBoolean(key, (Boolean) data);
        } else if (data instanceof Float) {
            editor.putFloat(key, (Float) data);
        } else if (data instanceof Integer) {
            editor.putInt(key, (Integer) data);
        } else if (data instanceof Long) {
            editor.putLong(key, (Long) data);
        } else {
            editor.putString(key, CoreApp.getInstance().getGSon().toJson(data));
        }
        editor.apply();
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }
}
