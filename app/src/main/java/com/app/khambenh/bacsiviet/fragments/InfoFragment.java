package com.app.khambenh.bacsiviet.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.ChangePassActivity;
import com.app.khambenh.bacsiviet.activities.PaymentActivity;
import com.app.khambenh.bacsiviet.activities.SplashActivity;
import com.app.khambenh.bacsiviet.stringee.service.StringeeService;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.UsersUtils;
import com.facebook.login.LoginManager;

import java.text.DecimalFormat;

public class InfoFragment extends Fragment implements OnRefreshListener {

    private static InfoFragment infoFragment = null;
    public static InfoFragment getInstance() {
        if (infoFragment == null) {
            infoFragment = new InfoFragment();
        }
        return infoFragment;
    }

    private Context context = null;
    private RelativeLayout btnChangPW = null;
    private RelativeLayout btnLogout = null;
    private Button btnDeposit = null;
    private TextView tvBalance = null;
    private TextView tvFullname = null;
    private TextView tvEmail = null;

    private void registerBroadcastFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Consts.ACTION_UPDATE_BALANCE);
        context.registerReceiver(broadcastReceiver, filter);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Consts.ACTION_UPDATE_BALANCE)){
                long balance = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE, Long.class);
                tvBalance.setText(priceWithDecimal(balance));
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        registerBroadcastFilter();
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.k_activity_account_information, container, false);
        btnChangPW = rootView.findViewById(R.id.k_activity_account_information_change_pass_id);
        btnLogout = rootView.findViewById(R.id.k_activity_account_information_logout_id);
        btnDeposit =  rootView.findViewById(R.id.k_activity_account_information_btn_deposit_id);
        tvBalance = rootView.findViewById(R.id.k_activity_account_information_txt_money_id);
        tvFullname = rootView.findViewById(R.id.k_activity_account_information_txt_name_id);
        tvEmail = rootView.findViewById(R.id.k_activity_account_information_txt_email_id);
        initEvent();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        long balance = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE, Long.class);
        tvBalance.setText(priceWithDecimal(balance));
        QBUser currentUser = SharedPrefsHelper.getInstance().getQbUser();
        tvFullname.setText(currentUser.getFullName() == null? "": currentUser.getFullName());
        tvEmail.setText(currentUser.getEmail() == null? "": currentUser.getEmail());
    }

    public static String priceWithDecimal (long price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(price);
    }

    private void initEvent() {
        btnChangPW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changePass = new Intent(context, ChangePassActivity.class);
                startActivity(changePass);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringeeService.getInstance().disconnect();
                LoginManager.getInstance().logOut();
                removeAllUserData();
                ConversationFirebaseFragment.clearData();
                SharedPrefs.getInstance().clear();
                restartApp();
            }
        });

        btnDeposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, PaymentActivity.class));
            }
        });
    }

    private void restartApp() {
        Intent intent = new Intent(context, SplashActivity.class);
        int mPendingIntentId = 1234;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    private void removeAllUserData() {
        UsersUtils.removeUserData(context);
    }

    public void onRefresh() {
    }

    @Override
    public void onDestroy() {
        context.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}
