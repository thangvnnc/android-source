package com.app.khambenh.bacsiviet.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.db.FireStorageFuntion;
import com.app.khambenh.bacsiviet.db.FireStoreFunction;
import com.app.khambenh.bacsiviet.util.ImageOfMedicalRecord;
import com.app.khambenh.bacsiviet.util.MedicalRecord;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class AddMedicalRecordActivity extends AppCompatActivity {
    Button btnChooseImage, btnSave;
    EditText edit_title, edit_note;
    TwoWayView horizontal_layout_load_image;
    ListAdapter listAdapter;
    ArrayList<Uri> arrUris = new ArrayList<>();
    ArrayList<String> arrayListImage = new ArrayList<>();
    ArrayList<Item> arrayListMedicalRecord = new ArrayList<>();
    private File savedCaptureFile;
    UploadTask uploadTask;
    Uri uri;
    ProgressDialog progressDialog;

    android.content.BroadcastReceiver BroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Consts.ACTION_DELETE_IMAGE_OF_MEDICAL_RECORD)) {
                int position = intent.getIntExtra(Consts.EXTRA_POSITION, 0);
                arrayListMedicalRecord.remove(position);
                arrUris.remove(position);
                listAdapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medical_record);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_add_medical_record));

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);

        edit_title = findViewById(R.id.edit_title_medical_record);
        edit_note = findViewById(R.id.edit_note);
        btnChooseImage = findViewById(R.id.btnChooseImage);
        btnSave = findViewById(R.id.btnSave);
        horizontal_layout_load_image = findViewById(R.id.horizontal_layout_load_image);


        btnChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPicture();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage(getString(R.string.notifi_processing));
                progressDialog.show();
                uploadFile(0);
            }
        });

        registerBroadcastFilter();
    }

    private void uploadFile(final int i) {
        final StorageReference reference;
        if (i < arrUris.size()) {
            reference = FireStorageFuntion.getStorageReference(Consts.ChildFoder_MedicalRecord, arrUris.get(i).getLastPathSegment());
            uploadTask = reference.putFile(arrUris.get(i));
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    // Continue with the task to get the download URL
                    return reference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        arrayListImage.add(downloadUri.toString());
                        uploadFile(i + 1);
                    }
                }
            });

        } else {
            MedicalRecord medicalRecord = new MedicalRecord();
            medicalRecord.setUserID(SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class));
            medicalRecord.setTitle(edit_title.getText().toString());
            medicalRecord.setNote(edit_note.getText().toString());
            medicalRecord.setArrImages(arrayListImage);
            Long currentTime = Calendar.getInstance().getTimeInMillis();
            medicalRecord.setCreateDate(currentTime);

            FireStoreFunction.getDb().collection(Consts.Collection_MedicalReord)
                    .add(medicalRecord)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            if (documentReference != null) {
                                progressDialog.dismiss();
                                Intent intent = new Intent(AddMedicalRecordActivity.this, MedicalRecordActivity.class);
                                startActivityForResult(intent, Consts.RESULT_CODE_RELOAD_ACTIVITY_MEDICAL_RECORD);
                                finish();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("TAG", "Error adding document", e);
                        }
                    });
        }

    }

    private void registerBroadcastFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Consts.ACTION_DELETE_IMAGE_OF_MEDICAL_RECORD);
        registerReceiver(BroadcastReceiver, filter);
    }


    private void inputPicture() {
        final CharSequence[] items = {getString(R.string.capture), getString(R.string.choose_from_gallery), getString(R.string.cancel)};
        final AlertDialog.Builder builder = new AlertDialog.Builder(AddMedicalRecordActivity.this);
        builder.setTitle(getString(R.string.select));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                //take camera
                if (items[item].equals(getString(R.string.capture))) {
                    savedCaptureFile = new File(CommonUtils.getFolderApp(Consts.APP_NAME), Consts.FILE_TMP_NAME);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(savedCaptureFile));
                    startActivityForResult(takePictureIntent, Consts.REQUEST_CAMERA);
                } else if (items[item].equals(getString(R.string.choose_from_gallery))) {//choose from lib
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, Consts.SELECT_PICTURE);
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String url = "";
        if (requestCode == Consts.REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                uri = Uri.fromFile(savedCaptureFile);
//                Glide.with(this).load(savedCaptureFile).into(imageView);
            }
        } else if (requestCode == Consts.SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                uri = data.getData();
//                savedCaptureFile = new File(getPath(uri));
//                url = savedCaptureFile.getPath();
//                Glide.with(this).load(savedCaptureFile).into(imageView);
            }
        }
        if (uri != null) {
            ImageOfMedicalRecord imageOfMedicalRecord = new ImageOfMedicalRecord(uri);
            arrayListMedicalRecord.add(imageOfMedicalRecord);

            arrUris.add(uri);

            listAdapter = new ListAdapter(this, arrayListMedicalRecord);
            horizontal_layout_load_image.setAdapter(listAdapter);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(BroadcastReceiver);
    }
}
