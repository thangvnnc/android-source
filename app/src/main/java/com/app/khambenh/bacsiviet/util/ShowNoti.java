package com.app.khambenh.bacsiviet.util;

public class ShowNoti {
    boolean isShow;

    public ShowNoti(boolean show) {
        this.isShow = show;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }
}
