package com.app.khambenh.bacsiviet.stringee.utils;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import java.util.Timer;

public class TimerTask extends java.util.TimerTask {
    private Timer timer = new Timer();
    private TimerTaskImp timerTaskImp = null;

    private static final int FPS = 1;
    private static final int SECOND = 1000;

    private TextView txtTimer = null;
    private int timeTick = 0;
    private TimerTask self;
    private boolean isStarted = false;

    public TimerTask(TextView txtTimer) {
        this.txtTimer = txtTimer;
        self = this;
    }

    public void setTimerTaskImp(TimerTaskImp timerTaskImp) {
        this.timerTaskImp = timerTaskImp;
    }

    public void start() {
        if (isStarted == false) {
            isStarted = true;
            timer.scheduleAtFixedRate(this, 0, SECOND/FPS);
        }
    }

    public void stop() {
        timer.cancel();
        timer.purge();
        timerTaskImp.onStopTimerTask(timeTick);
        isStarted = false;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void run() {
        timerTaskImp.onStartTimerTask();
        timeTick++;
        int minutes = (timeTick % 3600) / 60;
        int seconds = timeTick % 60;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                self.txtTimer.setText(String.format("%02d:%02d", minutes, seconds));
                timerTaskImp.onTickTimerTask(timeTick);
            }
        });
    }
}
