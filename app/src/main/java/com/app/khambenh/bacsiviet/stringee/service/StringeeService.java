package com.app.khambenh.bacsiviet.stringee.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.MainApp;
import com.app.khambenh.bacsiviet.stringee.activity.IncomingCallActivity;
import com.app.khambenh.bacsiviet.stringee.common.StringeeToken;
import com.app.khambenh.bacsiviet.stringee.define.StringeeKeys;
import com.app.khambenh.bacsiviet.stringee.define.StringeeSound;
import com.app.khambenh.bacsiviet.stringee.define.TransferKeys;
import com.app.khambenh.bacsiviet.stringee.log.LogStringee;
import com.app.khambenh.bacsiviet.stringee.receive.TransferServiceReceiver;
import com.google.firebase.iid.FirebaseInstanceId;
import com.app.khambenh.bacsiviet.CoreApp;
import com.stringee.StringeeClient;
import com.stringee.call.StringeeCall;
import com.stringee.exception.StringeeError;
import com.stringee.listener.StatusListener;
import com.stringee.listener.StringeeConnectionListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class StringeeService extends Service implements StringeeConnectionListener {

    // Static
    private static StringeeService instance = null;
    public static StringeeService getInstance() {
        if (instance == null) {
            instance = new StringeeService();
        }
        return instance;
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, StringeeService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LogStringee.error(TAG, "startForegroundService");
            context.startForegroundService(intent);
        }
        else {
            context.startService(intent);
        }
    }

    public static void stop() {
        StringeeService stringeeService = getInstance();
        if (stringeeService != null) {
            stringeeService.stopSelf();
        }
    }

    public static boolean isNull() {
        return instance == null;
    }

    public static boolean isRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (StringeeService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private TransferServiceReceiver transferServiceReceiver = new TransferServiceReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogStringee.error(TAG, "received");
            String key = intent.getStringExtra(TransferKeys.KEY);
            if (StringeeSound.SOUND_KEY.equals(key)) {
                String soundKey = intent.getStringExtra(StringeeSound.SOUND_KEY);
                playSound(soundKey);
            }
        }
    };

    private final static String TAG = "StringeeService";

    // Non static
    private Map<String, MediaPlayer> soundMap = new HashMap<>();
    public StringeeClient stringeeClient ;
    public Map<String, StringeeCall> callsMap = new HashMap<>();
    private NotificationCompat.Builder notificationBuilder = null;
    private int notifyId = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initRingStone();
        IntentFilter intentFilterTransfer = new IntentFilter("service.Broadcast");
        registerReceiver(transferServiceReceiver, intentFilterTransfer);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initStringee();
        initNotifyKeepServiceRunning(intent);
        reconnect();
        LogStringee.error(TAG, "onStartCommand");
        return START_NOT_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        reconnect();
        LogStringee.error(TAG, "onTaskRemoved");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (instance != null) {
            instance = null;
        }

        if (stringeeClient != null) {
            stringeeClient.disconnect();
            stringeeClient = null;
        }
        unregisterReceiver(transferServiceReceiver);
        sendBroadcast(new Intent("RestartServiceReceiver"));
        LogStringee.error(TAG, "onDestroy");
    }

    @Override
    public void onConnectionConnected(StringeeClient stringeeClient, boolean b) {
        saveUserId(CoreApp.getInstance(), stringeeClient.getUserId());
        registerNotify();
        LogStringee.error(TAG, "onConnectionConnected");
//        updateNotification("Đã kết nối");
//        LogStringee.toastAnywhere("Connected");
    }

    @Override
    public void onConnectionDisconnected(StringeeClient stringeeClient, boolean b) {
        LogStringee.error(TAG, "onConnectionDisconnected");
//        updateNotification("Ngắt kết nối");
        unregisterNotify();
    }

    @Override
    public void onIncomingCall(StringeeCall stringeeCall) {
        LogStringee.error(TAG, "onIncomingCall");
        callsMap.put(stringeeCall.getCallId(), stringeeCall);
        Intent intent = new Intent(CoreApp.getAppContext(), IncomingCallActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("call_id", stringeeCall.getCallId());
        startActivity(intent);
    }

    @Override
    public void onConnectionError(StringeeClient stringeeClient, StringeeError stringeeError) {
        LogStringee.error(TAG, "stringeeError : " + stringeeError.getMessage());
    }

    @Override
    public void onRequestNewToken(StringeeClient stringeeClient) {
        LogStringee.error(TAG, "onRequestNewToken");
        reconnect();
    }

    @Override
    public void onCustomMessage(String s, JSONObject jsonObject) {
        LogStringee.error(TAG, "onCustomMessage");
    }

    @Override
    public void onTopicMessage(String s, JSONObject jsonObject) {
        LogStringee.error(TAG, "onTopicMessage");
    }

    private void initRingStone() {
        MediaPlayer outgoingRing = MediaPlayer.create(CoreApp.getAppContext(), R.raw.sound_outgoing);
        outgoingRing.setLooping(true);
        soundMap.put(StringeeSound.OUTGOING_RING, outgoingRing);

        MediaPlayer ring = MediaPlayer.create(CoreApp.getAppContext(), R.raw.sound_ring);
        ring.setLooping(true);
        soundMap.put(StringeeSound.RING, ring);

        MediaPlayer incommingRing = MediaPlayer.create(CoreApp.getAppContext(), R.raw.sound_incomming);
        incommingRing.setLooping(true);
        soundMap.put(StringeeSound.INCOMMING_RING, incommingRing);

        MediaPlayer busy = MediaPlayer.create(CoreApp.getAppContext(), R.raw.sound_busy);
        busy.setLooping(false);
        soundMap.put(StringeeSound.BUSY, busy);

        MediaPlayer end = MediaPlayer.create(CoreApp.getAppContext(), R.raw.sound_hangup);
        end.setLooping(false);
        soundMap.put(StringeeSound.END, end);
    }

    private void playSound(String key) {
        stopSound();
        if (!StringeeSound.OFF.equals(key)) {
            AudioManager mAudioManager = (AudioManager) CoreApp.getInstance().getSystemService(Context.AUDIO_SERVICE);
            if (StringeeSound.INCOMMING_RING.equals(key)) {
                mAudioManager.setMode(AudioManager.STREAM_NOTIFICATION);
                mAudioManager.setSpeakerphoneOn(true);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                        0);
            }
            else {
                mAudioManager.setMode(AudioManager.MODE_IN_CALL);
                mAudioManager.setSpeakerphoneOn(false);
            }
            soundMap.get(key).start();
        }
        else {
            LogStringee.error(TAG, "OFF sound");
        }
    }

    private void stopSound() {
        if(soundMap.get(StringeeSound.OUTGOING_RING).isPlaying()) {
            soundMap.get(StringeeSound.OUTGOING_RING).stop();
            soundMap.get(StringeeSound.OUTGOING_RING).prepareAsync();
        }
        if(soundMap.get(StringeeSound.RING).isPlaying()) {
            soundMap.get(StringeeSound.RING).stop();
            soundMap.get(StringeeSound.RING).prepareAsync();
        }
        if(soundMap.get(StringeeSound.INCOMMING_RING).isPlaying()) {
            soundMap.get(StringeeSound.INCOMMING_RING).stop();
            soundMap.get(StringeeSound.INCOMMING_RING).prepareAsync();
        }
        if(soundMap.get(StringeeSound.BUSY).isPlaying()) {
            soundMap.get(StringeeSound.BUSY).stop();
            soundMap.get(StringeeSound.BUSY).prepareAsync();
        }
        if(soundMap.get(StringeeSound.END).isPlaying()) {
            soundMap.get(StringeeSound.END).stop();
            soundMap.get(StringeeSound.END).prepareAsync();
        }
    }

    private void initNotifyKeepServiceRunning(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startServiceAndroid9();
        else
            startServiceAndroidLessThan9(intent);

    }

    private void startServiceAndroidLessThan9(Intent intent) {
        String input = intent.getStringExtra("inputExtra");
        String chanelId = "chanelIdNotify";
        Intent notificationIntent = new Intent(CoreApp.getAppContext(), MainApp.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(CoreApp.getAppContext(), 0, notificationIntent, 0);
        notificationBuilder = new NotificationCompat.Builder(CoreApp.getAppContext(), chanelId)
                .setContentTitle("Medixlink")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_android_black_24dp)
                .setContentIntent(pendingIntent);
        Notification notification = notificationBuilder.build();
        startForeground(notifyId, notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startServiceAndroid9(){
        String NOTIFICATION_CHANNEL_ID = "com.app.khambenh.bacsiviet";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(CoreApp.getAppContext(), NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_android_black_24dp)
                .setContentTitle("Medixlink")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

    private void updateNotification(String str) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder.setContentTitle(str);
        mNotificationManager.notify(notifyId, notificationBuilder.build());
    }

    private void initStringee() {
        stringeeClient = new StringeeClient(CoreApp.getAppContext());
        stringeeClient.setConnectionListener(this);
    }

    private void registerNotify() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        StringeeClient client = StringeeService.getInstance().stringeeClient;
        // Register the token to Stringee Server
        LogStringee.error(TAG, refreshedToken);
        if (client != null && client.isConnected()) {
            client.registerPushToken(refreshedToken, new StatusListener() {
                @Override
                public void onSuccess() {
                    LogStringee.error(TAG, "registerPushToken success");
                }

                @Override
                public void onError(StringeeError stringeeError) {
                    super.onError(stringeeError);
                    LogStringee.error(TAG, "registerPushToken error: " + stringeeError.getMessage());
                }
            });
        }
    }

    private void unregisterNotify() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        StringeeClient client = StringeeService.getInstance().stringeeClient;
        // Register the token to Stringee Server
        if (client != null && client.isConnected()) {
            client.unregisterPushToken(refreshedToken, new StatusListener() {
                @Override
                public void onSuccess() {
                    LogStringee.error(TAG, "unregisterPushToken success");
                }

                @Override
                public void onError(StringeeError stringeeError) {
                    super.onError(stringeeError);
                    LogStringee.error(TAG, "unregisterPushToken error: " + stringeeError.getMessage());
                }
            });
        }
    }

    public void reconnect() {
        LogStringee.error(TAG, "Reconnect");
        String userTokenId = loadUserId(CoreApp.getInstance());
        if (userTokenId != null && "".equals(userTokenId) == false) {
            String token = StringeeToken.create(userTokenId);
            if (stringeeClient == null) {
                stringeeClient = new StringeeClient(CoreApp.getAppContext());
            }
            if (!stringeeClient.isConnected()) {
                stringeeClient.connect(token);
                LogStringee.error(TAG, "Token: " + token);

            }
            else {
                LogStringee.error(TAG, "Already connected ");
            }
        }
    }

    public void connect(String userId) {
        saveUserId(CoreApp.getInstance(), userId);
        reconnect();
    }

    public void disconnect() {
        if (stringeeClient != null) {
            StringeeService.getInstance().removeUserId(CoreApp.getInstance());
            stringeeClient.disconnect();
            stringeeClient = null;
        }
        else {
            LogStringee.error(TAG, "Disconnect is null strineeClient ");
        }
    }


    public String loadUserId(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Stringee", Context.MODE_PRIVATE);
        return sharedPreferences.getString(StringeeKeys.STRINGEE_KEY_SAVE_USER_ID, "");
    }

    public void saveUserId(Context context, String userId){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Stringee", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(StringeeKeys.STRINGEE_KEY_SAVE_USER_ID, userId);
        editor.apply();
    }

    public void removeUserId(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Stringee", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(StringeeKeys.STRINGEE_KEY_SAVE_USER_ID);
        editor.apply();
    }
}