package com.app.khambenh.bacsiviet.util;

/**
 * Created by os-nguyenxuanduc on 3/23/2018.
 */

public class MessageEvent {
    String keyword;
    String userName, passWord;

    public String getUserName() {
        return userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public MessageEvent(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    public MessageEvent(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }
}
