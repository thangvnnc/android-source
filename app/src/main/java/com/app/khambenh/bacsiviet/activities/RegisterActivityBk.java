package com.app.khambenh.bacsiviet.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.MessageEvent;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by os-nguyenxuanduc on 3/23/2018.
 */

public class RegisterActivityBk extends BaseActivity {
    private EditText txtFullname, txtPhone, txtUsername, txtPass, txtPresenter;
    private Spinner spType;
    private RadioButton radioMan, radioWoman, radioOther;
    private RadioGroup radioGroup;
    Button btnRegister;
    SharedPreferences pre;
    //    String arr_title_type[] = {"Thành viên", "Bác Sĩ", "Quản lý cơ sở y tế"}; //comment by Tuấn
    String arr_value_type[] = {"user", "professional", "place"};
    String type = "user";
    String regex = "[a-z0-9]+";
    int type_gender = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);
        //
//        setActionBarTitle(getString(R.string.title_register));
//        actionBar.setDisplayHomeAsUpEnabled(true);
        initView();
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void initView() {
        pre = getSharedPreferences("app_bacsi", MODE_PRIVATE);
        txtFullname = findViewById(R.id.txtFullname);
        txtPresenter = findViewById(R.id.txtPresenter);
        txtPhone = findViewById(R.id.txtPhone);
        txtUsername = findViewById(R.id.txtUsername);
        txtPass = findViewById(R.id.txtPass);
        radioGroup = findViewById(R.id.radioGender);
        radioMan = findViewById(R.id.radioMan);
        radioWoman = findViewById(R.id.radioWoman);
        radioOther = findViewById(R.id.radioOther);
        spType = findViewById(R.id.spType);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateForm()){
                    verifyPhone();
                }

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checked) {
                if (checked == R.id.radioOther) {
                    type_gender = Consts.TYPE_OTHER;
                } else if (checked == R.id.radioMan) {
                    type_gender = Consts.TYPE_MAN;
                } else {
                    type_gender = Consts.TYPE_WOMEN;
                }
            }
        });


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.arr_title_type, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        spType.setAdapter(adapter);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                type = arr_value_type[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void processRegister() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.registering));
        progressDialog.show();
        JSONObject postparams = new JSONObject();

        postparams.put("username", "" + txtUsername.getText());
        postparams.put("password", "" + txtPass.getText());
        postparams.put("fullname", "" + txtFullname.getText());
        postparams.put("gender", type_gender);
        postparams.put("phone", "" + txtPhone.getText());
        postparams.put("type", type);
        postparams.put("presenter", "" + txtPresenter.getText());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URLUtils.URL_REGISTER, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        if (response != null) {
                            try {
                                if (response.getString("isLogin").equals("true")) {
                                    EventBus.getDefault().post(new MessageEvent(txtUsername.getText().toString(), txtPass.getText().toString()));
                                    finish();
                                } else {
//                                    Toast.makeText(RegisterActivityBk.this, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        //Failure Callback

                    }
                });

// Adding the request to the queue along with a unique string tag
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

    }

    private boolean validateForm() {
        if ("".equals(txtFullname.getText().toString().trim())) {
            Toast.makeText(RegisterActivityBk.this, getString(R.string.error_empty_fullname), Toast.LENGTH_LONG).show();
            txtFullname.requestFocus();
            return false;
        }
        if ("".equals(txtUsername.getText().toString().trim())) {
            Toast.makeText(RegisterActivityBk.this, getString(R.string.error_empty_username), Toast.LENGTH_LONG).show();
            txtUsername.requestFocus();
            return false;
        }
        if ("".equals(txtPass.getText().toString().trim())) {
            Toast.makeText(RegisterActivityBk.this, getString(R.string.error_empty_password), Toast.LENGTH_LONG).show();
            txtPass.requestFocus();
            return false;
        }
        if (txtPhone.getText().toString().trim().length() < 8) {
            Toast.makeText(RegisterActivityBk.this, getString(R.string.error_phone_invalid), Toast.LENGTH_LONG).show();
            txtPass.requestFocus();
            return false;
        }

        String username = txtUsername.getText().toString();
        if (!username.matches(regex)) {
            CommonUtils.showToast(this, getString(R.string.wrong_username));
            return false;
        }
        return true;
    }

    public void verifyPhone() {
        AccountKit.logOut();
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN);// or .ResponseType.TOKEN

        configurationBuilder.setReadPhoneStateEnabled(true);
        PhoneNumber phoneNumber = new PhoneNumber("+84",txtPhone.getText().toString().trim());
        configurationBuilder.setInitialPhoneNumber(phoneNumber);
        UIManager uiManager = new SkinManager(
                SkinManager.Skin.CLASSIC,
                getResources().getColor(R.color.blue));
        configurationBuilder.setUIManager(uiManager);
        // ... perform additional configuration ...
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
        startActivityForResult(intent, APP_ACCOUNT_KIT_REQUEST_CODE);
    }

    void getAccountKitInfo() {

        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phone = phoneNumber.getPhoneNumber();
//                    String countryCode = TextUtils.isEmpty(phoneNumber.getCountryCodeIso())
//                            ? PhoneKit.getInstance().getCountryCodeIOS(phoneNumber.getCountryCode()) : phoneNumber.getCountryCodeIso();
                    String phoneCode = "+" + phoneNumber.getCountryCode();
                    JSONObject postparams = new JSONObject();
                    try {
                        postparams.put("phone", phone + "");

                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                URLUtils.URL_CHECK_PHONE, postparams, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (response != null) {
                                    try {
                                        boolean isExist = response.getBoolean("isExist");
                                        if(isExist){
                                            String msg = response.getString("msg");
                                            CommonUtils.showToast(RegisterActivityBk.this,msg);
                                        }else {
                                            processRegister();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onError(final AccountKitError error) {
                // Handle Error
                Log.e("Nhan", error.toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_ACCOUNT_KIT_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
//                showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0, 10));
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
//                goToMyLoggedInActivity();
                getAccountKitInfo();
            }

            // Surface the result to your user in an appropriate way.
//            Toast.makeText(
//                    this,
//                    toastMessage,
//                    Toast.LENGTH_LONG)
//                    .show();
        }
    }


}
