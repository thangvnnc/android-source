package com.app.khambenh.bacsiviet.util;

import android.util.Log;

import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StoreMessage
{
    private static final String KEY_STORE = "KEY_STORE_MESSAGE";
    private String mess;
    private String senderId;
    private String receiverId;
    private boolean auto;
    private Date c;

    public StoreMessage(Date c, String mess, String senderId, String receiverId, boolean auto) {
        this.c = c;
        this.mess = mess;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.auto = auto;
    }

    public void save()
    {
        List<StoreMessage> storeMessages = getMesssages();
        if (storeMessages == null)
        {
            createAndPut();
            return;
        }
        put();
    }

    public static List<StoreMessage> getMesssages()
    {
        SharedPrefs sharedPrefs = SharedPrefs.getInstance();
        String jsonString = sharedPrefs.get(KEY_STORE, String.class);
        if (jsonString == null)
        {
            return null;
        }
        Type listType = new TypeToken<ArrayList<StoreMessage>>(){}.getType();
        List<StoreMessage> storeMessages = new Gson().fromJson(jsonString, listType);
        return storeMessages;
    }

    private void createAndPut()
    {
        SharedPrefs sharedPrefs = SharedPrefs.getInstance();
        List<StoreMessage> storeMessages = new ArrayList<>();
        storeMessages.add(this);
        sharedPrefs.put(KEY_STORE, new Gson().toJson(storeMessages));
    }

    private void put()
    {
        SharedPrefs sharedPrefs = SharedPrefs.getInstance();
        List<StoreMessage> storeMessages = getMesssages();
        storeMessages.add(this);
        sharedPrefs.put(KEY_STORE, new Gson().toJson(storeMessages));
    }

    public static void remove(Date c, String mess, String senderId, String receiverId, boolean auto)
    {
        SharedPrefs sharedPrefs = SharedPrefs.getInstance();
        List<StoreMessage> storeMessages = getMesssages();
        if(storeMessages == null)
        {
            return;
        }
        for (int idx = 0; idx < storeMessages.size(); idx++) {
            StoreMessage storeMessage = storeMessages.get(idx);

            if (c.equals(storeMessage.getC()) && mess.equals(storeMessage.getMess())
                    && senderId.equals(storeMessage.getSenderId()) && receiverId.equals(storeMessage.getReceiverId())
                    && auto == storeMessage.auto);
            {
                storeMessages.remove(idx);
                break;
            }
        }
        sharedPrefs.put(KEY_STORE, storeMessages);
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public Date getC() {
        return c;
    }

    public void setC(Date c) {
        this.c = c;
    }
}
