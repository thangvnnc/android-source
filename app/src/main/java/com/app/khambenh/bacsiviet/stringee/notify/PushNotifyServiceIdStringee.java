package com.app.khambenh.bacsiviet.stringee.notify;

import com.app.khambenh.bacsiviet.stringee.log.LogStringee;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by luannguyen on 2/7/2018.
 */

public class PushNotifyServiceIdStringee extends FirebaseInstanceIdService {
    private final String TAG = "PushNtfServiceId";

    @Override
    public void onTokenRefresh() {
        LogStringee.error(TAG, "onTokenRefresh");
    }
}
