package com.app.khambenh.bacsiviet.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.db.FireStoreFunction;
import com.app.khambenh.bacsiviet.util.MedicalRecord;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MedicalRecordActivity extends AppCompatActivity {

    private Button btnAdd;
    private ListView lv_medical_record;
    ListAdapter listAdapter;
    ArrayList<Item> itemArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_record);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_medical_record));

        btnAdd = findViewById(R.id.btnAdd);
        lv_medical_record = findViewById(R.id.lv_medical_record);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MedicalRecordActivity.this, AddMedicalRecordActivity.class);
                startActivity(intent);
            }
        });
        getData();

//        listAdapter = new ListAdapter(this,)
    }

    private void getData() {
        int userID = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);

        FireStoreFunction.getData(Consts.Collection_MedicalReord).whereEqualTo(Consts.Field_userID, userID).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                itemArrayList.clear();
                ArrayList<MedicalRecord> medicalRecordArrayList = new ArrayList<>();
                for (QueryDocumentSnapshot document : task.getResult()) {
                    MedicalRecord medicalRecord = document.toObject(MedicalRecord.class);
                    medicalRecordArrayList.add(medicalRecord);
                }

                Collections.sort(medicalRecordArrayList, new Comparator<MedicalRecord>() {
                    @Override
                    public int compare(MedicalRecord left, MedicalRecord right) {
                        if (left.getCreateDate() == null || right.getCreateDate() == null)
                            return 0;
                        return right.getCreateDate().compareTo(left.getCreateDate());
                    }
                });

                itemArrayList.addAll(medicalRecordArrayList);
                listAdapter = new ListAdapter(MedicalRecordActivity.this, itemArrayList);
                lv_medical_record.setAdapter(listAdapter);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Consts.RESULT_CODE_RELOAD_ACTIVITY_MEDICAL_RECORD) {
            getData();
            listAdapter.notifyDataSetChanged();
        }
    }
}
