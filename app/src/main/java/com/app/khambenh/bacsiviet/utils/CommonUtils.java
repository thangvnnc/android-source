package com.app.khambenh.bacsiviet.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.db.FireStoreFunction;
import com.app.khambenh.bacsiviet.services.bubble.FloatingBubblePermissions;
import com.app.khambenh.bacsiviet.util.LogApp;
import com.app.khambenh.bacsiviet.util.ResultUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

//add by Tuan
public class CommonUtils {
    //Character
    public static char[] SOURCE_CHARACTERS = {'À', 'Á', 'Â', 'Ã', 'È', 'É',
            'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â',
            'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý',
            'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ',
            'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ',
            'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ',
            'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ',
            'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ',
            'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ',
            'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ',
            'ữ', 'Ự', 'ự',};

    public static char[] DESTINATION_CHARACTERS = {'A', 'A', 'A', 'A', 'E',
            'E', 'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a',
            'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u',
            'y', 'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u',
            'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A',
            'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e',
            'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E',
            'e', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
            'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
            'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
            'U', 'u', 'U', 'u',};

    private static char removeAccent(char ch) {
        int index = Arrays.binarySearch(SOURCE_CHARACTERS, ch);
        if (index >= 0) {
            ch = DESTINATION_CHARACTERS[index];
        }
        return ch;
    }

    public static String removeAccent(String str) {
        StringBuilder sb = new StringBuilder(str);
        for (int i = 0; i < sb.length(); i++) {
            sb.setCharAt(i, removeAccent(sb.charAt(i)));
        }
        return sb.toString();
    }

    public static final String MAIN_URL = "https://nganluong.vn/mobile_checkout_api_post.php";
    //    public static final String RETURN_URL = "http://bacsiviet.vn";
//    public static final String CANCEL_URL = "http://bacsiviet.vn/cancel";
//    public static final String NOTIFY_URL = "http://bacsiviet.vn/notify";
    public static final String RETURN_URL = "https://medixlink.com/";
    public static final String CANCEL_URL = "https://medixlink.com/cancel";
    public static final String NOTIFY_URL = "https://medixlink.com/notify";
    public static final String MERCHANT_ACCOUNT = "bacsivietok@gmail.com";
    public static String MERCHANT_ID = "55676";
    public static String MERCHANT_PASSWORD = "d743bd6e9676a7f212657a2e7b02df67";
    public static final int READ_TIMEOUT = 15000;
    public static final int CONNECTION_TIMEOUT = 15000;
    public static final String BASE_URL = "https://medixlink.com/api/v1.0/";
    public static final String URL_USER_API = "https://medixlink.com/api/v0.1/user/";

    public static String md5(String stringSendOrder) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(stringSendOrder.getBytes());
            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception ex) {
            return ex.toString();
        }
    }

    public static String getCodeError(Context context, String errorCode) {
        String descriptionCode = "";
        if (errorCode.equalsIgnoreCase("01")) {
            descriptionCode = context.getString(R.string.error_01);
        } else if (errorCode.equalsIgnoreCase("02")) {
            descriptionCode = context.getString(R.string.error_02);
        } else if (errorCode.equalsIgnoreCase("04")) {
            descriptionCode = context.getString(R.string.error_04);
        } else if (errorCode.equalsIgnoreCase("05")) {
            descriptionCode = context.getString(R.string.error_05);
        } else if (errorCode.equalsIgnoreCase("06")) {
            descriptionCode = context.getString(R.string.error_06);
        } else if (errorCode.equalsIgnoreCase("07")) {
            descriptionCode = context.getString(R.string.error_07);
        } else if (errorCode.equalsIgnoreCase("09")) {
            descriptionCode = context.getString(R.string.error_09);
        } else if (errorCode.equalsIgnoreCase("11")) {
            descriptionCode = context.getString(R.string.error_11);
        } else if (errorCode.equalsIgnoreCase("20")) {
            descriptionCode = context.getString(R.string.error_20);
        } else if (errorCode.equalsIgnoreCase("21")) {
            descriptionCode = context.getString(R.string.error_21);
        } else if (errorCode.equalsIgnoreCase("22")) {
            descriptionCode = context.getString(R.string.error_22);
        } else if (errorCode.equalsIgnoreCase("23")) {
            descriptionCode = context.getString(R.string.error_23);
        } else if (errorCode.equalsIgnoreCase("24")) {
            descriptionCode = context.getString(R.string.error_24);
        } else if (errorCode.equalsIgnoreCase("25")) {
            descriptionCode = context.getString(R.string.error_25);
        } else if (errorCode.equalsIgnoreCase("26")) {
            descriptionCode = context.getString(R.string.error_26);
        } else if (errorCode.equalsIgnoreCase("27")) {
            descriptionCode = context.getString(R.string.error_27);
        } else if (errorCode.equalsIgnoreCase("28")) {
            descriptionCode = context.getString(R.string.error_28);
        } else if (errorCode.equalsIgnoreCase("29")) {
            descriptionCode = context.getString(R.string.error_29);
        } else if (errorCode.equalsIgnoreCase("30")) {
            descriptionCode = context.getString(R.string.error_30);
        } else if (errorCode.equalsIgnoreCase("31")) {
            descriptionCode = context.getString(R.string.error_31);
        } else if (errorCode.equalsIgnoreCase("32")) {
            descriptionCode = context.getString(R.string.error_32);
        } else if (errorCode.equalsIgnoreCase("33")) {
            descriptionCode = context.getString(R.string.error_33);
        }

        return descriptionCode;
    }


    public static String timeStampToDate(long timeStamp) {
        try {
            Date netDate = (new Date(timeStamp));
            return new SimpleDateFormat(DatePattern.DISPLAY_DATE).format(netDate);
        } catch (Exception ex) {
            return "";
        }
    }

    public static void showDatePickerDialog(final Context context,
                                            final EditText et) {
        String dateTime = et.getText().toString().trim();
        final int mDay;
        final int mMonth;
        final int mYear;
        Calendar cal = Calendar.getInstance();
        if (dateTime.length() < 1) {
            dateTime = timeStampToDate(cal.getTimeInMillis());
        }
        mDay = Integer.parseInt(dateTime.split(Pattern.quote("-"))[0]);
        mMonth = Integer.parseInt(dateTime.split(Pattern.quote("-"))[1]);
        mYear = Integer.parseInt(dateTime.split(Pattern.quote("-"))[2]);
        DatePickerDialog dpd = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        String day = dayOfMonth > 9 ? dayOfMonth + "" : "0" + dayOfMonth;
                        String month = monthOfYear + 1 > 9 ?
                                monthOfYear + 1 + "" : "0" + (monthOfYear + 1);
                        et.setText(String.format("%s-%s-%d", day, month, year));
                    }
                }, mYear, mMonth - 1, mDay);

        dpd.show();
    }

    public static class DatePattern {
        public static String TIME = "HH:mm:ss";
        public static String SQL_DATE = "yyyy-MM-dd";
        public static String DISPLAY_DATE = "dd-MM-yyyy";
        public static String DISPLAY_TIME = "HH:mm";
        public static String SQL_GMT_TIME = "yyyy-MM-dd'T'HH:mm:ss";
    }

    public static void showToast(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static String parseDatetime(String inputPattern, String outPutPattern, String dateTime) {
        try {
            SimpleDateFormat in = new SimpleDateFormat(inputPattern);
            SimpleDateFormat output = new SimpleDateFormat(outPutPattern);
            Date date = in.parse(dateTime);
            return output.format(date);
        } catch (Exception ex) {
            return "";
        }
    }

    public static void showDialogRating(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.setTitle(activity.getString(R.string.title_rating));
        dialog.setContentView(R.layout.dialog_rating_call);
        dialog.setCancelable(false);
        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        final TextView txt_rating_code = (TextView) dialog.findViewById(R.id.text_rating_code);
        Button txt_send = (Button) dialog.findViewById(R.id.text_send);
        Button txt_later = (Button) dialog.findViewById(R.id.text_later);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                txt_rating_code.setText(String.valueOf(rating));
            }
        });

        txt_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                activity.finish();
            }
        });
        txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String rating = String.valueOf(ratingBar.getRating());
                dialog.dismiss();
                activity.finish();
            }
        });

        dialog.show();
    }

    public static void dialogNotConnecction(final Context context){
        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.setTitle(activity.getString(R.string.title_rating));
        dialog.setContentView(R.layout.dialog_not_connecttion);
        dialog.setCancelable(false);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static File getFolderApp(String apppName) {

        File returnedFolder = new File(android.os.Environment.getExternalStorageDirectory(), apppName);
        if (!returnedFolder.exists()) {
            returnedFolder.mkdirs();
        }
        return returnedFolder;
    }

    public static Bitmap decodeFile(File f, int reqestSize) throws Throwable {
        Matrix matrix = new Matrix();
        int degree = getExifOrientation(f.getAbsolutePath());

        matrix.postRotate(degree);

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(f.getAbsolutePath(), o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < reqestSize || height_tmp / 2 < reqestSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        //decode with current scale values
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), o2);
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return croppedBitmap;

    }

    public static int getExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filepath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            if (orientation != -1) {
                // We only recognise a subset of orientation tag values.
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }

            }
        }

        return degree;
    }

    /**
     * check permission
     *
     * @param mContext
     * @param permissions list permission need to check
     * @return
     */
    public static boolean checkPermission(Context mContext, String[] permissions) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            for (String per : permissions) {
                if ((mContext.checkSelfPermission(per) != PackageManager.PERMISSION_GRANTED)) {
                    return false;
                }
            }
        }
        return true;
    }

    // show alert request permission
    public static void showRequestPermission(Activity activity, String[] permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, permission, requestCode);
    }

    public static void checkAllPermission(Activity activity) {
        if (!CommonUtils.checkPermission(activity, Consts.PERMISSION_FILES)) {
            CommonUtils.showRequestPermission(activity, Consts.PERMISSION_FILES, Consts.REQUEST_CODE_PERMISSION_STORAGE);
        }
        if (!CommonUtils.checkPermission(activity, Consts.PERMISSION_CALL)) {
            CommonUtils.showRequestPermission(activity, Consts.PERMISSION_CALL, Consts.REQUEST_CODE_PERMISSION_CALL);
        }
        if (!CommonUtils.checkPermission(activity, Consts.PERMISSION_INTERNET)) {
            CommonUtils.showRequestPermission(activity, Consts.PERMISSION_INTERNET, Consts.REQUEST_CODE_PERMISSION_INTERNET);
        }
        if (!CommonUtils.checkPermission(activity, Consts.PERMISSION_PHONE)) {
            CommonUtils.showRequestPermission(activity, Consts.PERMISSION_PHONE, Consts.REQUEST_CODE_PERMISSION_PHONE);
        }
        if (!CommonUtils.checkPermission(activity, Consts.PERMISSION_FINE_LOCATION)) {
            CommonUtils.showRequestPermission(activity, Consts.PERMISSION_FINE_LOCATION, Consts.REQUEST_CODE_PERMISSION_FINE_LOCATION);
        }
        if (!CommonUtils.checkPermission(activity, Consts.PERMISSION_COARSE_LOCATION)) {
            CommonUtils.showRequestPermission(activity, Consts.PERMISSION_COARSE_LOCATION, Consts.REQUEST_CODE_PERMISSION_COARSE_LOCATION);
        }

        FloatingBubblePermissions.startPermissionRequest(activity);
    }

    public static String getDocConversation(int currUserId, int OpponentId) {
        if (currUserId < OpponentId) {
            return "" + currUserId + OpponentId;
        } else {
            return "" + OpponentId + currUserId;
        }
    }

    public static void SaveLLogBK(final Context context, String userId, String message, String TagActivity) {
        Date c = Calendar.getInstance().getTime();
        LogApp logApp = new LogApp(userId, message, TagActivity, c);
        FireStoreFunction.getDb().collection(Consts.LOG_APP)
                .document(c.toString())
                .set(logApp)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showToast(context,context.getString(R.string.notifi_save_log_success));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showToast(context,context.getString(R.string.error_save_log_faild));
                    }
                });
    }


    public static void SaveLLog(final Context context, String userId, String message, String TagActivity) {
        Date c = Calendar.getInstance().getTime();
        LogApp logApp = new LogApp(userId, message, TagActivity, c);
        FireStoreFunction.getDb().collection(Consts.LOG_APP)
                .document(c.toString())
                .set(logApp)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    public static void GetUserServer(String id, ResultUser resultUser) {
        JSONObject postparams = new JSONObject();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL_USER_API + id, postparams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    try {
                        QBUser qbUser = new QBUser();
                        if (response.has("user")) {
                            JSONObject jsUser = response.getJSONObject("user");
                            qbUser.setId(jsUser.getInt("user_id"));
                            qbUser.setLogin(jsUser.getString("email"));
                            qbUser.setFullName(jsUser.getString("fullname"));
                            qbUser.setCustomData(jsUser.getString("avatar"));
                            qbUser.setPhone(jsUser.getString("phone"));
                            qbUser.setFacebookId(jsUser.getString("id_facebook"));
                            qbUser.setExternalId(String.valueOf(jsUser.getInt("user_id")));
                            int userType = jsUser.getInt("user_type_id");
                            List<String> tags = new ArrayList<>();

                            if(userType == 1) {
                                tags.add("user");
                            }
                            else if(userType == 2) {
                                tags.add("doctor");
                            }

                            qbUser.setTags(tags);
                        }
                        if (response.has("doctor")) {
                            String doctor = response.getString("doctor");
                            if("null".equals(doctor) == false) {
                                JSONObject jsDoctor = response.getJSONObject("doctor");
                                Integer idDoctor = jsDoctor.getInt("doctor_id");
                                qbUser.setWebsite("https://medixlink.com/danh-sach-bac-si-chi-tiet/"+idDoctor);
                            }
                        }

                        resultUser.onResult(qbUser);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
    }
}
