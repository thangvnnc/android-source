package com.app.khambenh.bacsiviet.utils;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

public class QbUsersHolder {
    private SparseArray<QBUser> qbUserSparseArray;
    private static QbUsersHolder instance = null;
    private static QBUser qUser = null;

    public void setInstance(QbUsersHolder qbChatService) {
        instance = qbChatService;
    }

    public static QbUsersHolder getInstance(){
        if (instance != null) {
            return instance;
        }
        return null;
    }

    public void setUser(QBUser qbUser) {
        qUser = qbUser;
    }

    public QBUser getUser() {
        return qUser;
    }

    public List<QBUser> getUsersByIds(List<Integer> ids) {
        List<QBUser> users = new ArrayList<>();
        for (Integer id : ids) {
            QBUser user = getUserById(id);
            if (user != null) {
                users.add(user);
            }
        }

        return users;
    }

    public QBUser getUserById(int id) {
        return qbUserSparseArray.get(id);
    }
}
