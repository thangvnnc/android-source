package com.app.khambenh.bacsiviet.util;


import com.app.khambenh.bacsiviet.utils.QBUser;

public interface ResultUser {
    void onResult(QBUser qbUser);
}
