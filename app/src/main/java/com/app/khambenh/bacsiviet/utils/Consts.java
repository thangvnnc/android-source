package com.app.khambenh.bacsiviet.utils;

import android.Manifest;

/**
 * QuickBlox team
 */
public interface Consts {
    String MissCall = "<Misscall>";

    String EXTRA_POSITION = "EXTRA_POSITION";
    String EXTRA_URL_IMAGE = "EXTRA_URL_IMAGE";
    String EXTRA__LIST_IMAGE = "EXTRA_LIST_IMAGE";

    int TYPE_MAN = 1;
    int TYPE_WOMEN = 2;
    int TYPE_OTHER = 3;

    String APP_NAME = "Medix Link";
    String FILE_TMP_NAME = "tmp.jpg";//temp file
    //used to choose image
    int REQUEST_CAMERA = 1;
    int SELECT_PICTURE = 2;
    int RESULT_CODE_FROM_CROP_AVATAR = 123;
    int RESULT_CODE_RELOAD_ACTIVITY_MEDICAL_RECORD = 159;

    String TYPE_CALL_AUDIO = "TYPE_CALL_AUDIO";
    String TYPE_CHAT = "TYPE_CHAT";
    String TYPE_CALL_VIDEO = "TYPE_CALL_VIDEO";

    String ACTION_VIDEO_CALL_DIALOG = "ACTION_VIDEO_CALL_DIALOG";
    String ACTION_VOICE_CALL_DIALOG = "ACTION_VOICE_CALL_DIALOG";
    String ACTION_UPDATE_BALANCE = "ACTION_UPDATE_BALANCE";
    String ACTION_DELETE_IMAGE_OF_MEDICAL_RECORD = "ACTION_DELETE_IMAGE_OF_MEDICAL_RECORD";
    String ACTION_CREATE_COLLECTION = "ACTION_CREATE_COLLECTION";
    String ACTION_SHOW_DIALOG_TO_RECHARGE_CHAT_ACTIVITY = "ACTION_SHOW_DIALOG_TO_RECHARGE_CHAT_ACTIVITY";
    String ACTION_SHOW_DIALOG_PEMISSION_CHAT_ACTIVITY = "ACTION_SHOW_DIALOG_PEMISSION_CHAT_ACTIVITY";
    String ACTION_HAVE_MISSED_CALL = "ACTION_HAVE_MISSED_CALL";

    String EXTRA_DOCTOR_ID = "EXTRA_DOCTOR_ID";
    String EXTRA_MESSAGE_FROM_USER_ID = "EXTRA_MESSAGE_FROM_USER_ID";

    String DEFAULT_USER_PASSWORD = "x6Bt0VDy5";

    String VERSION_NUMBER = "1.0";

    int CALL_ACTIVITY_CLOSE = 1000;

    int ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422;
    int ERR_MSG_DELETING_HTTP_STATUS = 401;

    //CALL ACTIVITY CLOSE REASONS
    int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;
    String WIFI_DISABLED = "wifi_disabled";

    String OPPONENTS = "opponents";
    String CONFERENCE_TYPE = "conference_type";
    String EXTRA_TAG = "currentRoomName";
    int MAX_OPPONENTS_COUNT = 6;

    String PREF_CURREN_ROOM_NAME = "current_room_name";
    String PREF_CURRENT_TOKEN = "current_token";
    String PREF_TOKEN_EXPIRATION_DATE = "token_expiration_date";

    String EXTRA_QB_USER = "qb_user";

    String EXTRA_USER_ID = "user_id";
    String EXTRA_USER_LOGIN = "user_login";
    String EXTRA_USER_PASSWORD = "user_password";
    String EXTRA_PENDING_INTENT = "pending_Intent";

    String EXTRA_IS_CREATE_CONVERSATION = "EXTRA_IS_CREATE_CONVERSATION";

    String EXTRA_CONTEXT = "context";
    String EXTRA_OPPONENTS_LIST = "opponents_list";
    String EXTRA_CONFERENCE_TYPE = "conference_type";
    String EXTRA_IS_INCOMING_CALL = "conversation_reason";

    String EXTRA_IS_LOGIN_USER = "EXTRA_IS_LOGIN_USER";

    String EXTRA_LOGIN_RESULT = "login_result";
    String EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message";
    int EXTRA_LOGIN_RESULT_CODE = 1002;

    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

    String EXTRA_COMMAND_TO_SERVICE = "command_for_service";
    int COMMAND_NOT_FOUND = 0;
    int COMMAND_LOGIN = 1;
    int COMMAND_LOGOUT = 2;
    String EXTRA_IS_STARTED_FOR_CALL = "isRunForCall";
    String ALREADY_LOGGED_IN = "You have already logged in chat";

    public static final int DEFAULT_LANGUAGE_ID = 0; //add by Tuan

    enum StartConversationReason {
        INCOME_CALL_FOR_ACCEPTION,
        OUTCOME_CALL_MADE
    }

    //arr permission file
    String[] PERMISSION_FILES = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] PERMISSION_CALL = new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
    String[] PERMISSION_INTERNET = new String[]{Manifest.permission.INTERNET};
    String[] PERMISSION_PHONE = new String[]{Manifest.permission.READ_PHONE_STATE};
    String[] PERMISSION_FINE_LOCATION = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    String[] PERMISSION_COARSE_LOCATION = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};
    String[] PERMISSION_ALERT_SYSTEM = new String[]{Manifest.permission.SYSTEM_ALERT_WINDOW};

    //request code permission
    int REQUEST_CODE_PERMISSION_STORAGE = 793;
    int REQUEST_CODE_PERMISSION_CALL = 794;
    int REQUEST_CODE_PERMISSION_PHONE = 795;
    int REQUEST_CODE_PERMISSION_INTERNET = 796;
    int REQUEST_CODE_PERMISSION_FINE_LOCATION = 797;
    int REQUEST_CODE_PERMISSION_COARSE_LOCATION = 798;
    int REQUEST_CODE_ALERT_SYSTEM = 799;

    //----------FireBase--------
    //Collection firebase
    String Collection_MedicalReord = "MedicalReord";
    String Collection_MissingCall = "MissingCall";
    String COLLECTION_MESSAGE = "Message";
    String COLLECTION_CONVERSATION = "Conversation";
    String COLLECTION_USER = "User";
    String LOG_APP = "LOG";

    //Field for medicalrecord
    String Field_userID = "userID";

    //Field for missedcall
    String Field_currUserID = "currUserID";

    String MyCustomBucket = "gs://medix-link.appspot.com";
    String ChildFoder_MedicalRecord = "MedicalRecord/";
    String ChildFoder_Attachment = "Attachment/";

    // ------MAP--------
    double Distance_Default = 20;

    int TYPE_DEFAULT = 0;
    int TYPE_USER = 1;
    int TYPE_DOCTOR = 2;
    int TYPE_CLINIC = 3;
    int TYPE_DRUG = 4;

    // -----TAG LOG------
    String TAG_LOGIN_ACTIVITY="TAG_LOGIN_ACTIVITY";
    String TAG_REGISTER_VERIFY="TAG_REGISTER_VERIFY";
}
