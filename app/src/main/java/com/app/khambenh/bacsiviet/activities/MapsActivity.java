package com.app.khambenh.bacsiviet.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private Location myLocation = null;
    SupportMapFragment mapFragment;
    private boolean isLoginUser = false;
    private boolean isLoadMyLocation = true;
    boolean hasLocation = false;

    LatLng myLatLng;
    public static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 20;
    public static final int LOCATION_UPDATE_MIN_TIME = 1000;

    Button btn_update;
//    EditText edit_update_location;
    RelativeLayout lnLayout_update_location;
    int user_id;

    ProgressDialog progressDialog;

    public static void start(Context context, boolean isUser) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(Consts.EXTRA_IS_LOGIN_USER, isUser);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_maps);

    }

    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_maps);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void selectMarker() {
        LatLng latLng = null;
        if (myLatLng != null) {
            latLng = myLatLng;
        }
        addMarker(latLng, "", 0, 0);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        setMyLocation(latLng);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        progressDialog = new ProgressDialog(this);
        user_id = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);

        btn_update = (Button) findViewById(R.id.btn_update);
//        edit_update_location = (EditText) findViewById(R.id.edit_update_location);
        lnLayout_update_location = (RelativeLayout) findViewById(R.id.lnLayout_update_location);
        if (getIntent() != null) {
            this.isLoginUser = getIntent().getBooleanExtra(Consts.EXTRA_IS_LOGIN_USER, false);
            if (!isLoginUser) {
                lnLayout_update_location.setVisibility(View.VISIBLE);
            } else {
                lnLayout_update_location.setVisibility(View.GONE);
            }
        }
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMarker();
            }
        });
        //
        try {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setMyLocationEnabled(true);

            progressDialog.setMessage(getString(R.string.notifi_geting_your_location));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            handler.postDelayed(runnable,1000);
        }catch (SecurityException e){
            CommonUtils.showToast(this,e.getMessage());
        }
//        configGooogleAPIClient();
    }

//    protected synchronized void configGooogleAPIClient() {
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//        googleApiClient.connect();
//    }

    private void setMyLocation(LatLng latLng) {
        try {
            JSONObject postparams = new JSONObject();
            postparams.put("user_id", user_id);
            postparams.put("lat", latLng.latitude);
            postparams.put("lng", latLng.longitude);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_SET_LOCATION, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
//                        edit_update_location.setText("");
                        CommonUtils.showToast(MapsActivity.this, getString(R.string.notifi_update_success));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(MapsActivity.this, error.getMessage());
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            CommonUtils.showToast(MapsActivity.this, e.getMessage());
        }
    }

    private void getMyLocation() {
        try {
            JSONObject postparams = new JSONObject();
            postparams.put("user_id", user_id);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_GET_LOCATION, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            JSONObject jsonObject = response.getJSONObject("location");
                            double lat = jsonObject.getDouble("lat");
                            double lng = jsonObject.getDouble("lng");
                            LatLng latLng = new LatLng(lat, lng);
                            addMarker(latLng, "", 0, 0);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(MapsActivity.this, error.getMessage());
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            CommonUtils.showToast(MapsActivity.this, e.getMessage());
        }
    }

    private void getNearLocation(double distance) {
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("distance", distance);
            postparams.put("lat", myLatLng.latitude);
            postparams.put("lng", myLatLng.longitude);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_SEARCH_LOCATION, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("user");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                int userID = jsonObject.getInt("user_id");
                                String id = String.valueOf(userID);
//                                String fullname = jsonObject.getString("fullname");
//                                String phone = jsonObject.getString("phone");
                                int type_user = jsonObject.getInt("user_type_id");
                                double distance = jsonObject.getDouble("distance");
                                double lat = jsonObject.getDouble("lat");
                                double lng = jsonObject.getDouble("lng");
                                LatLng latLng = new LatLng(lat, lng);
                                if (userID == user_id) {
                                    isLoadMyLocation = false;
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                    addMarker(latLng, "", 0, distance);
                                } else {
                                    addMarker(latLng, id, type_user, distance);
                                }
                            }
                            if (isLoadMyLocation && !isLoginUser) {
                                getMyLocation();
                            }
                            if(isLoginUser) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 15));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(MapsActivity.this, error.getMessage());
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            CommonUtils.showToast(MapsActivity.this, e.getMessage());
        }
    }

    // Khi người dùng trả lời yêu cầu cấp quyền (cho phép hoặc từ chối).
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        switch (requestCode) {
            case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION: {


                // Chú ý: Nếu yêu cầu bị bỏ qua, mảng kết quả là rỗng.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "Permission granted!", Toast.LENGTH_LONG).show();

                    // Hiển thị vị trí hiện thời trên bản đồ.
//                    showMyLocation();
                }
                // Hủy bỏ hoặc từ chối.
                else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }


    private void addMarker(final LatLng latLng, final String user_id, final int type, final double distance) {

        if (type == Consts.TYPE_DEFAULT) {
            MarkerOptions option = new MarkerOptions();
            option.position(latLng);
            option.title(getString(R.string.my_location));
            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            final Marker currentMarker = mMap.addMarker(option);
            currentMarker.showInfoWindow();
        } else {
//            Performer<QBUser> qbUser = QBUsers.getUserByExternalId(user_id);
//            qbUser.performAsync(new QBEntityCallback<QBUser>() {
//                @Override
//                public void onSuccess(final QBUser qbUser, Bundle bundle) {
                    QBUser qbUser = null;
                    if (qbUser != null) {
                        String split[] = qbUser.getCustomData().split("-_-");
                        // Thêm Marker cho Map:
                        MarkerOptions option = new MarkerOptions();
                        option.position(latLng);
                        if (type == Consts.TYPE_DOCTOR) {
                            option.title(String.valueOf((int) ((distance) * 1000)) + " m");
                            option.snippet(split[0]);
                            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                        } else if (type == Consts.TYPE_CLINIC) {
                            option.title(String.valueOf((int) (distance * 1000)) + " m");
                            option.snippet(split[0]);
                            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        }

                        final Marker currentMarker = mMap.addMarker(option);
                        currentMarker.showInfoWindow();
                        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                if (marker.equals(currentMarker)) {
                                    if (type == Consts.TYPE_CLINIC || type == Consts.TYPE_DOCTOR) {
                                        progressDialog.show();
                                        CallUltils.chat(qbUser, MapsActivity.this);
                                    }
                                }
                            }
                        });
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                if (marker.equals(currentMarker)) {
                                    if (type == Consts.TYPE_CLINIC || type == Consts.TYPE_DOCTOR) {
                                        progressDialog.show();
                                        CallUltils.chat(qbUser, MapsActivity.this);
                                    }
                                }
                                return false;
                            }
                        });
                    }
//                }
//
//                @Override
//                public void onError(QBResponseException e) {
//
//                }
//            });
        }
    }

    public LatLng getLocationFromAddress(String strAddress) throws IOException {

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;


        address = coder.getFromLocationName(strAddress, 5);
        if (address == null) {
            return null;
        }
        Address location = address.get(0);
        p1 = new LatLng((double) (location.getLatitude()),
                (double) (location.getLongitude()));

        return p1;

    }

    @Override
    public void onLocationChanged(Location location) {
        handler.removeCallbacks(runnable);
        progressDialog.dismiss();
        myLocation = location;
        myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        getNearLocation(Consts.Distance_Default);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    private void getCurrentLocation() {
        try {
            LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            Location location = null;
            if (!(isGPSEnabled || isNetworkEnabled))
                CommonUtils.showToast(this, getString(R.string.error_cant_get_location));
            else {
                if (isNetworkEnabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this);
                    location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }

                if (isGPSEnabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this);
                    location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
            }
            if (location != null) {
                handler.removeCallbacks(runnable);
                progressDialog.dismiss();
                myLocation = location;
                myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                getNearLocation(Consts.Distance_Default);
            }
        }catch (SecurityException e){
            CommonUtils.showToast(this,e.getMessage());
        }
    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(myLocation==null){
                getCurrentLocation();
                handler.postDelayed(this,1000);
            }else {
                handler.removeCallbacks(this);
            }
        }
    };
}
