package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.adapters.ListAdapterViewType;
import com.app.khambenh.bacsiviet.db.FireStorageFuntion;
import com.app.khambenh.bacsiviet.db.FireStoreFunction;
import com.app.khambenh.bacsiviet.util.Attachment;
import com.app.khambenh.bacsiviet.util.Conversation;
import com.app.khambenh.bacsiviet.util.HeaderMessage;
import com.app.khambenh.bacsiviet.util.ResultUser;
import com.app.khambenh.bacsiviet.util.StoreMessage;
import com.app.khambenh.bacsiviet.util.UserFirebase;
import com.app.khambenh.bacsiviet.util.appMessage;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.ConnectivityUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.ItemViewType;
import com.app.khambenh.bacsiviet.utils.QBChatService;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.StaticCommon;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.facebook.common.Common;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageFirebaseActivity extends AppCompatActivity implements View.OnClickListener {

    public static String callUsername = "";
    public static boolean isCall = false;
    public Context context = null;

    ImageView btn_attachment, btn_send;
    ImageView btn_back, btn_call_video, btn_call_audio, btn_view_info;
    TextView txt_tittle;
    EditText edit_message;
    ListView lvMessage;
    TwoWayView lvAttachment;
    RelativeLayout layout_send_message;

    ListAdapterViewType chatAdapter;
    ListAdapter attacgmentAdapter;
    String docConversation;
    ArrayList<ItemViewType> itemArrayList = new ArrayList<>();
    ArrayList<appMessage> messageArrayList = new ArrayList<>();
    ArrayList<Item> attachmentArrayList = new ArrayList<>();
    ArrayList<Uri> arrUris = new ArrayList<>();
    ArrayList<String> arrayListImage = new ArrayList<>();

    int currUserId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);
    boolean isLoginFB = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_LOGIN_FB, Boolean.class);
    String currUserFullName = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_FULLNAME, String.class);
    String currTypeUser = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_TYPE, String.class);
    private QBUser qbUser;
    File savedCaptureFile;
    Uri uri;
    UploadTask uploadTask;
    ProgressDialog progressDialog;
    boolean isCreateConversation = false;
    boolean isClickBtnSend = false;
    int unread = 0;
    int status = 0;
    QBUser currentUser;
    Activity _activity = null;

    public static void startMessageFirebase(Context context, boolean isCreateConversation, QBUser qbUser) {
        Intent intent = new Intent(context, MessageFirebaseActivity.class);
        intent.putExtra(Intent.EXTRA_USER, qbUser);
        intent.putExtra(Consts.EXTRA_IS_CREATE_CONVERSATION, isCreateConversation);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setContentView(R.layout.activity_message_firebase);
        setContentView(R.layout.k_activity_message);
        context = this;
        _activity = this;
        currentUser = QBChatService.getInstance().getUser();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);


        StaticCommon.outMessageScreen = false;

        isCreateConversation = getIntent().getBooleanExtra(Consts.EXTRA_IS_CREATE_CONVERSATION, false);
        if (getIntent().hasExtra(Intent.EXTRA_USER)) {
            qbUser = (QBUser) getIntent().getSerializableExtra(Intent.EXTRA_USER);
            updateQBUser(qbUser);
            return;
        }

//        String fromId = getIntent().getStringExtra(Consts.EXTRA_MESSAGE_FROM_USER_ID);
//        if (fromId == null) {
            finish();
            Intent intent = new Intent(CoreApp.getAppContext(), SplashActivity.class);
            startActivity(intent);
//            return;
//        }
//
//        CommonUtils.GetUserServer(fromId, new ResultUser() {
//            @Override
//            public void onResult(QBUser qbUser) {
//                updateQBUser(qbUser);
//            }
//        });
    }

    private void updateQBUser(QBUser user) {
        qbUser = user;
        initView();
        initAdapter();
        loadChatHistory();
    }

    private void initView() {

        txt_tittle = (TextView) findViewById(R.id.k_activity_message_txt_tittle_id);
        txt_tittle.setText(qbUser.getFullName());

        btn_back = (ImageView) findViewById(R.id.k_activity_message_btn_back_id);
        btn_call_audio = (ImageView) findViewById(R.id.k_activity_message_btn_call_audio_id);
        btn_call_video = (ImageView) findViewById(R.id.k_activity_message_btn_call_video_id);
        btn_view_info = (ImageView) findViewById(R.id.k_activity_message_btn_view_info_id);


        layout_send_message = (RelativeLayout) findViewById(R.id.k_activity_message_layout_send_message_id);
        edit_message = (EditText) findViewById(R.id.k_activity_message_edit_message_id);

        lvMessage = (ListView) findViewById(R.id.k_activity_message_lvMessage_id);
        lvAttachment = (TwoWayView) findViewById(R.id.k_activity_message_lvAttachment_id);
        btn_attachment = (ImageView) findViewById(R.id.k_activity_message_btn_attachment_id);
        btn_send = (ImageView) findViewById(R.id.k_activity_message_btn_send_id);

        if (qbUser.getTags().get(0).equalsIgnoreCase("user")) {
            btn_view_info.setVisibility(View.INVISIBLE);
            btn_call_audio.setVisibility(View.INVISIBLE);
            btn_call_video.setVisibility(View.INVISIBLE);
        } else {
             btn_view_info.setVisibility(View.VISIBLE);
            btn_call_audio.setVisibility(View.VISIBLE);
            btn_call_video.setVisibility(View.VISIBLE);
        }

        btn_back.setOnClickListener(this);
        btn_view_info.setOnClickListener(this);
        btn_call_audio.setOnClickListener(this);
        btn_call_video.setOnClickListener(this);

        btn_send.setOnClickListener(this);
        btn_attachment.setOnClickListener(this);
    }

    private void initAdapter() {

        chatAdapter = new ListAdapterViewType(this, itemArrayList);
        lvMessage.setAdapter(chatAdapter);
        lvMessage.setDivider(null);
    }

    private void loadChatHistory() {
        String docConversation = getDocConversation();
        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .document(docConversation)
                .collection(Consts.COLLECTION_MESSAGE)
                .orderBy("createTime")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e("ERROR", "Listen failed.", e);
                            return;
                        }
                        itemArrayList.clear();
                        messageArrayList.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            appMessage message = doc.toObject(appMessage.class);
                            messageArrayList.add(message);
                        }

                        if (messageArrayList.size() == 1 && currUserId == Integer.parseInt(messageArrayList.get(0).getSenderId()) && currTypeUser.equalsIgnoreCase("user")) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }
                            Date c = Calendar.getInstance().getTime();
                            StoreMessage storeMessage = new StoreMessage(c, getString(R.string.auto_rep_message),  qbUser.getExternalId(), "", true);
                            storeMessage.save();
                            setMessage(c, getString(R.string.auto_rep_message), qbUser.getExternalId(), "" + currUserId, true);
                        }

                        initItemArrayList();
                        getLastMessageForEachUser();
                        initAdapter();
//                        lvMessage.smoothScrollToPosition();
                        lvMessage.setSelection(itemArrayList.size() - 1);
                    }
                });

        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .document(docConversation)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {

                    @Override
                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        try {
                            if (e != null) {
                                Log.e("ERROR", "Listen failed.", e);
                                return;
                            }

                            if (snapshot != null && snapshot.exists()) {
                                Log.e("Result", "Current data: " + snapshot.getData());
                                Conversation conversation = snapshot.toObject(Conversation.class);
                                if (currUserId == Integer.parseInt(conversation.getLastUser())) {
                                    unread = conversation.getUnread();
                                } else {
                                    if (!StaticCommon.outMessageScreen) {
                                        unread = 0;
                                        conversation.setUnread(unread);
                                        conversation.setStatusMessage(2);
                                        resetConversation(conversation);
                                    }
                                }
                                StaticCommon.statusMessage = conversation.getStatusMessage();
                                chatAdapter.notifyDataSetChanged();
                                lvMessage.setSelection(itemArrayList.size() - 1);
                            } else {
                                Log.e("Result", "Current data: null");
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                });
    }

    private void initItemArrayList() {
        String currentHeader = "";
        if (messageArrayList.size() != 0) {
            for (appMessage message : messageArrayList) {
                if (!currentHeader.equalsIgnoreCase(message.getDate())) {
                    currentHeader = message.getDate();
                    HeaderMessage headerMessage = new HeaderMessage();
                    headerMessage.setHeader(message.getDate());
                    itemArrayList.add(headerMessage);
                }
                itemArrayList.add(message);
            }
        }
    }

    private void getLastMessageForEachUser() {
        boolean hadFound = false;
        if (itemArrayList.size() > 0) {
            if (itemArrayList.get(itemArrayList.size() - 1).getViewType() == ListAdapterViewType.RowType.LIST_ITEM.ordinal()) {
                appMessage lastMessage = (appMessage) itemArrayList.get(itemArrayList.size() - 1);
                if (Integer.parseInt(lastMessage.getSenderId()) == currUserId) {
                    StaticCommon.LastMessage = itemArrayList.size() - 1;
                    for (int i = itemArrayList.size() - 2; i < itemArrayList.size() && i >= 0; i--) {
                        if (itemArrayList.get(i).getViewType() == ListAdapterViewType.RowType.LIST_ITEM.ordinal()) {
                            appMessage message = (appMessage) itemArrayList.get(i);
                            if (!hadFound) {
                                try {
                                    if (Integer.parseInt(message.getReceiverId()) == currUserId) {
                                        StaticCommon.opponentLastMessage = i;
                                        hadFound = true;
                                    }
                                }
                                catch (NumberFormatException nex)
                                {
                                    nex.printStackTrace();
                                }
                            }
                        }
                    }
                } else {
                    StaticCommon.opponentLastMessage = itemArrayList.size() - 1;

                    for (int i = itemArrayList.size() - 2; i < itemArrayList.size() && i >= 0; i--) {
                        if (itemArrayList.get(i).getViewType() == ListAdapterViewType.RowType.LIST_ITEM.ordinal()) {
                            appMessage message = (appMessage) itemArrayList.get(i);
                            if (!hadFound) {
                                if (Integer.parseInt(message.getSenderId()) == currUserId && !hadFound) {
                                    StaticCommon.LastMessage = i;
                                    hadFound = true;
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    private void resetConversation(Conversation conversation) {
        String docConversation = getDocConversation();
        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .document(docConversation)
                .set(conversation)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", "Error writing document", e);
                    }
                });
    }

    private void setConversation(String message, Date createTime) {
        String currUserAvatar = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_AVATAR, String.class);
        Conversation conversation = new Conversation();
        if (("".equals(message)) && (arrUris.size() > 0)) {
            message = "[Hình ảnh]";
        }
        conversation.setLastMessage(message);
        conversation.setLastUser("" + currUserId);
        conversation.setUnread(unread + 1);
        conversation.setStatusMessage(0);
        if (currUserId < Integer.parseInt(qbUser.getExternalId())) {
            conversation.setUserId_1("" + currUserId);
            conversation.setName_1("" + currUserFullName);
            if (isLoginFB) {
                conversation.setUserAvatar_1(currUserAvatar);
            } else {
                conversation.setUserAvatar_1("https://" + currUserAvatar);
            }

            conversation.setUserId_2("" + qbUser.getExternalId());
            conversation.setName_2("" + qbUser.getFullName());
            conversation.setUserAvatar_2(getAvatarFromQB(qbUser));
        } else {
            conversation.setUserId_1("" + qbUser.getExternalId());
            conversation.setName_1("" + qbUser.getFullName());
            conversation.setUserAvatar_1(getAvatarFromQB(qbUser));

            conversation.setUserId_2("" + currUserId);
            conversation.setName_2("" + currUserFullName);
            if (isLoginFB) {
                conversation.setUserAvatar_2(currUserAvatar);
            } else {
                conversation.setUserAvatar_2("https://" + currUserAvatar);
            }
        }

        conversation.setCreateTime(createTime);
        String docConversation = getDocConversation();
        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .document(docConversation)
                .set(conversation)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e("ERROR", "OKKKKKK");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", "Error writing document", e);
                    }
                });
    }

    private String getAvatarFromQB(QBUser qbUser) {
        String customData = qbUser.getCustomData();
        if(customData == null || "null".equals(customData)) {
            return "https://medixlink.com/public/images/ic_other_user.png";
        }
        if (customData.contains("-_-")) {
            String str[] = customData.split("-_-");
            return "https://" + str[1];
        } else {
            return qbUser.getCustomData();
        }
    }

    private void setMessage(final Date c, final String mess, final String senderId, final String receiverId, final boolean auto) {

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = df.format(c);
        String time = c.getHours() + ":" + c.getMinutes() + ":" + c.getSeconds();
        int index = itemArrayList.size();

        appMessage message = new appMessage();
        if (auto) {
            message.setMyAvatar("");
        } else {
            if (isLoginFB) {
                message.setMyAvatar(SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_AVATAR, String.class));
            } else {
                message.setMyAvatar("https://" + SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_AVATAR, String.class));
            }
        }

        message.setIndex(index);
        message.setMessage(mess);
        message.setSenderId("" + senderId);
        message.setReceiverId("" + receiverId);
        message.setDate("" + date);
        message.setTime("" + time);
        message.setCreateTime(c);
        message.setTotalAttachment(arrayListImage.size());
        message.setListAttachment(arrayListImage);

        String docConversation = getDocConversation();
        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .document(docConversation)
                .collection(Consts.COLLECTION_MESSAGE)
                .document("" + c)
                .set(message)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        checkNumberMessageCharge();
                        StoreMessage.remove(c, mess, senderId, receiverId, auto);
                        edit_message.setText("");
                        attachmentArrayList.clear();
                        arrayListImage.clear();
                        arrUris.clear();
                        lvAttachment.setVisibility(View.INVISIBLE);
                        progressDialog.dismiss();
                        isClickBtnSend = false;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", "Error writing document", e);
                    }
                });
    }

    private void checkNumberMessageCharge()
    {
        if (currentUser == null)
        {
            SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
            currentUser = sharedPrefsHelper.getQbUser();
        }
        String user_email = currentUser.getLogin();
        String doctor_email = MessageFirebaseActivity.callUsername;
        Integer countNumber = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_COUNT_MESSAGE_WITH_DOCTOR_MAIL + user_email + doctor_email, Integer.class);

        if (countNumber < 2) {
            SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_COUNT_MESSAGE_WITH_DOCTOR_MAIL + user_email + doctor_email, ++countNumber);
            return;
        }
        chargeMessage(user_email, doctor_email, countNumber);
    }

    public void chargeMessage(final String user_email, final String doctor_email, Integer countNumber) {
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("user_email", "" + user_email);
            postparams.put("doctor_email", "" + doctor_email);
            postparams.put("count", countNumber);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_CHARGE_MESSAGE, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            boolean isSuccess = false;

                            if (response.has("isSave")) {
                                isSuccess = response.getBoolean("isSave");
                            }

                            if (isSuccess) {
                                SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_COUNT_MESSAGE_WITH_DOCTOR_MAIL + user_email + doctor_email, 0);
                                if (response.has("balance")) {
                                    long balance = response.getLong("balance");
                                    SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_BALANCE, balance);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getDocConversation() {
        if (qbUser == null) {
//            Toast.makeText(context, "AAAAAAAA", Toast.LENGTH_LONG).show();
            return null;
        }
        String externalId = qbUser.getExternalId();
        if (currUserId < Integer.parseInt(externalId)) {
            docConversation = "" + currUserId + qbUser.getExternalId();
        } else {
            docConversation = "" + qbUser.getExternalId() + currUserId;
        }
        return docConversation;
    }

    private void sendMessage() {
        if (edit_message.getText().toString().trim().equalsIgnoreCase("") && arrayListImage.size() == 0) {
            return;
        } else {
            Date c = Calendar.getInstance().getTime();
            setConversation(edit_message.getText().toString(), c);
            setMessage(c, edit_message.getText().toString(), "" + currUserId, qbUser.getExternalId(), false);
            pushNotification();
        }
    }

    private void pushNotification() {

        FireStoreFunction.getData(Consts.COLLECTION_USER)
                .document(qbUser.getExternalId())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        UserFirebase userFirebase = document.toObject(UserFirebase.class);

                        String url = "https://fcm.googleapis.com/fcm/send";
                        try {
                            JSONObject notification = new JSONObject();
//                            notification.put("click_action", "android.intent.action.MAIN");
                            notification.put("title", currUserFullName);
                            notification.put("body", edit_message.getText().toString());
                            JSONObject data = new JSONObject();
                            data.put("senderId", currUserId);
                            JSONObject aps = new JSONObject();
                            aps.put("content-available", 1);
                            JSONObject pushNotification = new JSONObject();
                            pushNotification.put("notification", notification);
                            pushNotification.put("to", "" + userFirebase.getDevice_token());
                            pushNotification.put("priority", "high");
                            pushNotification.put("data", data);
                            pushNotification.put("aps", aps);
                            Log.e("Push", "" + pushNotification);
                            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                    url, pushNotification, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    if (response != null) {

                                    }

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //Failure Call

                                }
                            }) {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String, String> headers = new HashMap<>();
                                    headers.put("Content-Type", "application/json");
                                    headers.put("Authorization", getString(R.string.api_key_header_value_fcm));
                                    return headers;
                                }
                            };
                            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void sendMessage(List<StoreMessage> storeMessages) {
        for (int idx = 0; idx < storeMessages.size(); idx++)
        {
            StoreMessage storeMessage = storeMessages.get(idx);
            Date c = storeMessage.getC();
            String mess = storeMessage.getMess();
            String senderId = storeMessage.getSenderId();
            String receiverId = storeMessage.getReceiverId();
            boolean auto = storeMessage.isAuto();
            setMessage(c, mess, senderId, receiverId, auto);
            pushNotification(mess);
        }
    }

    private void pushNotification(final String mess) {
        FireStoreFunction.getData(Consts.COLLECTION_USER)
                .document(qbUser.getExternalId())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        UserFirebase userFirebase = document.toObject(UserFirebase.class);

                        String url = "https://fcm.googleapis.com/fcm/send";
                        try {
                            JSONObject notification = new JSONObject();
//                            notification.put("click_action", "android.intent.action.MAIN");
                            notification.put("title", currUserFullName);
                            notification.put("body", mess);
                            JSONObject data = new JSONObject();
                            data.put("senderId", currUserId);
                            JSONObject aps = new JSONObject();
                            aps.put("content-available", 1);
                            JSONObject pushNotification = new JSONObject();
                            pushNotification.put("notification", notification);
                            pushNotification.put("to", "" + userFirebase.getDevice_token());
                            pushNotification.put("priority", "high");
                            pushNotification.put("data", data);
                            pushNotification.put("sound", "default");
                            pushNotification.put("aps", aps);
                            Log.e("Push", "" + pushNotification);
                            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                    url, pushNotification, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    if (response != null) {

                                    }

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //Failure Call

                                }
                            }) {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String, String> headers = new HashMap<>();
                                    headers.put("Content-Type", "application/json");
                                    headers.put("Authorization", getString(R.string.api_key_header_value_fcm));
                                    return headers;
                                }
                            };
                            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void inputPicture(Context context) {
        final CharSequence[] items = {getString(R.string.capture), getString(R.string.choose_from_gallery), getString(R.string.cancel)};
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.select));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                //take camera
                if (items[item].equals(getString(R.string.capture))) {
                    savedCaptureFile = new File(CommonUtils.getFolderApp(Consts.APP_NAME), Consts.FILE_TMP_NAME);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(savedCaptureFile));
                    startActivityForResult(takePictureIntent, Consts.REQUEST_CAMERA);
                } else if (items[item].equals(getString(R.string.choose_from_gallery))) {//choose from lib
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, Consts.SELECT_PICTURE);
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Consts.REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                uri = Uri.fromFile(savedCaptureFile);
//                Glide.with(this).load(savedCaptureFile).into(imageView);
            }
        } else if (requestCode == Consts.SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                uri = data.getData();
//                savedCaptureFile = new File(getPath(uri));
//                url = savedCaptureFile.getPath();
//                Glide.with(this).load(savedCaptureFile).into(imageView);
            }
        }


        if (uri != null) {
            Attachment attachment = new Attachment(uri);
            attachmentArrayList.add(attachment);

            arrUris.add(uri);

            attacgmentAdapter = new ListAdapter(this, attachmentArrayList);
            lvAttachment.setAdapter(attacgmentAdapter);
            lvAttachment.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    showDialogDeleteAttachment(position);
                    return false;
                }
            });
            lvAttachment.setVisibility(View.VISIBLE);
        }
    }

    private void showDialogDeleteAttachment(final int position) {
        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
//        dialog.setTitle(getString(R.string.notifi_delete_attachment_message));
        dialog.setContentView(R.layout.dialog_delete_attachment_message);
        dialog.setCancelable(false);

        Button btn_delete_attachment = (Button) dialog.findViewById(R.id.btn_delete_attachment);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_delete_attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attachmentArrayList.remove(position);
                attacgmentAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void uploadtAttachment(final int i) {
        long balance = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE, Long.class);
        if (balance <= 0)
        {
            Log.e("Balance 1", balance+"");
            showDialogToReCharge();
            return;
        }

        if (i < arrUris.size()) {
            int count = i + 1;
            final StorageReference reference;
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(false);
            progressDialog.show();
            progressDialog.setMessage(count + "/" + arrUris.size());

            reference = FireStorageFuntion.getStorageReference(Consts.ChildFoder_Attachment, arrUris.get(i).getLastPathSegment());
            uploadTask = reference.putFile(arrUris.get(i));
            uploadTask
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            Double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setProgress(Integer.valueOf(progress.intValue()));
                        }
                    })
                    .continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            // Continue with the task to get the download URL
                            return reference.getDownloadUrl();
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                arrayListImage.add(downloadUri.toString());
                                uploadtAttachment(i + 1);
                            }
                        }
                    });
        } else {
            sendMessage();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        StaticCommon.outMessageScreen = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent();
        intent.setAction(Consts.ACTION_UPDATE_BALANCE);
        context.sendBroadcast(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.k_activity_message_btn_back_id:
                onBackPressed();
                break;
            case R.id.k_activity_message_btn_view_info_id:
                loadProfileByLink(qbUser.getWebsite());
                break;
            case R.id.k_activity_message_btn_call_audio_id:
                isCall = true;
                callUsername = qbUser.getLogin();
                CallUltils.checkToCall(qbUser, MessageFirebaseActivity.this, Consts.TYPE_CALL_AUDIO);
                break;
            case R.id.k_activity_message_btn_call_video_id:
                isCall = true;
                callUsername = qbUser.getLogin();
                CallUltils.checkToCall(qbUser, _activity, Consts.TYPE_CALL_VIDEO);
                break;
            case R.id.k_activity_message_btn_send_id:
                long balance = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE, Long.class);
                if (balance <= 0)
                {
                    Log.e("Balance 2", balance+"");
                    showDialogToReCharge();
                    return;
                }

                callUsername = qbUser.getLogin();
                if (!isClickBtnSend) {
                    isClickBtnSend = true;
//                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                    params.addRule(RelativeLayout.ABOVE, R.id.layout_send_message);
//                    lvMessage.setLayoutParams(params);
                    Date c = Calendar.getInstance().getTime();
                    StoreMessage storeMessage = new StoreMessage(c, edit_message.getText().toString(),  "" + currUserId, qbUser.getExternalId(), false);
                    storeMessage.save();
                    if(ConnectivityUtils.isNetworkConnected()){
                        uploadtAttachment(0);
                    }else {
                        edit_message.setText("");
                        attachmentArrayList.clear();
                        arrayListImage.clear();
                        arrUris.clear();
                        lvAttachment.setVisibility(View.INVISIBLE);
                        progressDialog.dismiss();
                        isClickBtnSend = false;
//                        CommonUtils.showToast(MessageFirebaseActivity.this,getString(R.string.error_connection_faild));
                    }

                }
                break;
            case R.id.k_activity_message_btn_attachment_id:
//                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                params.addRule(RelativeLayout.ABOVE, R.id.lvAttachment);
//                lvMessage.setLayoutParams(params);
                inputPicture(MessageFirebaseActivity.this);
                break;

        }
    }

    private void loadProfileByLink(String website) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(website));
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialogToReCharge() {
        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        dialog.setTitle(getString(R.string.title_note));
        dialog.setContentView(R.layout.dialog_notifi_need_to_recharge);
        dialog.setCancelable(false);
        TextView txtMessDialog = (TextView) dialog.findViewById(R.id.txtMessDialog);
        txtMessDialog.setText(getString(R.string.notifi_need_to_recharge));
        Button btnSeen = (Button) dialog.findViewById(R.id.btnSeen);
        btnSeen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<StoreMessage> storeMessages = StoreMessage.getMesssages();
        if (storeMessages != null) {
            sendMessage(storeMessages);
        }
    }
}
