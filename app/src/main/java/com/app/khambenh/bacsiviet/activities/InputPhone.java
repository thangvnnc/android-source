package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.khambenh.bacsiviet.R;

public class InputPhone extends Activity {
    private Button btnRegister = null;
    private EditText edtPhoneInput = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.k_activity_input_phone);

        edtPhoneInput = findViewById(R.id.k_activity_input_phone_edt_phone);
        btnRegister = findViewById(R.id.k_activity_phone_btn_confirm);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = edtPhoneInput.getText().toString().trim();
                if(phoneNumber.isEmpty() || phoneNumber.length() < 10){
                    Toast.makeText(getBaseContext(), "Enter a valid mobile", Toast.LENGTH_SHORT).show();
                    edtPhoneInput.requestFocus();
                    return;
                }

                Intent intent = new Intent(InputPhone.this, VerifyPhoneActivity.class);
                intent.putExtra("phoneNumber", phoneNumber);
                startActivity(intent);
                finish();
            }
        });
    }
}
