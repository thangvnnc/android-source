package com.app.khambenh.bacsiviet.utils.constant;

public interface MimeType {

    String IMAGE_MIME = "image/*";
    String STREAM_MIME = "application/octet-stream";
}
