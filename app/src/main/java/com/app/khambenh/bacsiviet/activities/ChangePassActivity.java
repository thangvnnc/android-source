package com.app.khambenh.bacsiviet.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassActivity extends AppCompatActivity {
    EditText edit_old_pass, edit_new_pass, edit_confirm_pass;
    Button btn_save;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.k_activity_change_pass);
        init();

    }

    private void init() {
        username = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_NAME, String.class);
        edit_old_pass = (EditText) findViewById(R.id.k_activity_change_pass_edt_user_name_id);
        edit_new_pass = (EditText) findViewById(R.id.k_activity_change_pass_edt_password_new_id);
        edit_confirm_pass = (EditText) findViewById(R.id.k_activity_change_pass_edt_password_id);
        btn_save = (Button) findViewById(R.id.k_activity_change_pass_btn_login_id);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    changPass();
                }
            }
        });
    }

    private boolean check() {
        if (!edit_new_pass.getText().toString().trim().equalsIgnoreCase(edit_confirm_pass.getText().toString().trim())) {
            return false;
        }
        return true;
    }

    private void changPass() {
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("username", username);
            postparams.put("old_password", edit_old_pass.getText().toString());
            postparams.put("new_password", edit_new_pass.getText().toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_CHANGE_PASS, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(response!=null){
                        try {
                            boolean success = response.getBoolean("success");
                            String mess = response.getString("message");
                            if(success){
                                CommonUtils.showToast(ChangePassActivity.this,mess);
                                finish();
                            }else {
                                CommonUtils.showToast(ChangePassActivity.this,mess);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
