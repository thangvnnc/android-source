package com.app.khambenh.bacsiviet.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.QuestionDetailsActivity;
import com.app.khambenh.bacsiviet.util.MComment;
import com.app.khambenh.bacsiviet.util.MQuestionItem;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Adapter hiển thị danh sách comment
 */
public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Activity _activity = null;
    private List<MComment> _comments = null;

    /**
     * Constructor
     * @param activity : activity
     * @param comments : danh sách comment
     */
    public CommentAdapter(Activity activity, List<MComment> comments)
    {
        _activity = activity;
        _comments = comments;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {
        populateItemRows((ItemViewHolder) viewHolder, position);
    }

    @Override
    public int getItemCount()
    {
        return _comments == null ? 0 : _comments.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder
    {
        TextView txtName;
        TextView txtTime;
        TextView txtContent;

        public ItemViewHolder(View itemView)
        {
            super(itemView);
            txtName = itemView.findViewById(R.id.item_comment_txt_name_id);
            txtTime = itemView.findViewById(R.id.item_comment_txt_time_id);
            txtContent = itemView.findViewById(R.id.item_comment_txt_content_id);
        }
    }

    /**
     * Biding dữ liệu lên view
     * @param viewHolder : viewholder
     * @param position : vị trí item
     */
    private void populateItemRows(ItemViewHolder viewHolder, int position)
    {
        MComment item = _comments.get(position);

        viewHolder.txtName.setText(item.getFullname().trim());
        viewHolder.txtTime.setText("Thời gian " + item.getCreateAt().trim());
        viewHolder.txtContent.setText(item.getAnswerContent().trim());
    }
}
