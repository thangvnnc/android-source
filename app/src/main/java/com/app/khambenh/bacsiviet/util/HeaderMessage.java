package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.ListAdapterViewType;
import com.app.khambenh.bacsiviet.utils.ItemViewType;

public class HeaderMessage implements ItemViewType {
    private String header;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Override
    public int getViewType() {
        return ListAdapterViewType.RowType.HEADER_ITEM.ordinal();
    }

    @Override
    public View getView(Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;

        rowView = (View) layoutInflater.inflate(R.layout.layout_header_message_firebase, null);

        TextView txt_header = rowView.findViewById(R.id.txt_header);
        txt_header.setText(getHeader());
        return rowView;
    }
}
