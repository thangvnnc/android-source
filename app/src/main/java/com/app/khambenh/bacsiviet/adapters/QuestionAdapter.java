package com.app.khambenh.bacsiviet.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.QuestionDetailsActivity;
import com.app.khambenh.bacsiviet.util.MQuestionItem;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Adapter hiển thị danh sách câu hỏi
 */
public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Activity _activity = null;
    private List<MQuestionItem.QItem> _questionItems = null;

    /**
     * Constructor
     * @param activity : activity
     * @param questionItems : danh sách câu hỏi
     */
    public QuestionAdapter(Activity activity, List<MQuestionItem.QItem> questionItems)
    {
        _activity = activity;
        _questionItems = questionItems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        // View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_question, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.k_item_question, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {
        populateItemRows((ItemViewHolder) viewHolder, position);
    }

    @Override
    public int getItemCount()
    {
        return _questionItems == null ? 0 : _questionItems.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder
    {
        SelectableRoundedImageView imgThumb;
        TextView txtName;
        TextView txtTitle;
        TextView txtContent;
        TextView txtDate;

        public ItemViewHolder(View itemView)
        {
            super(itemView);
//            imgThumb = itemView.findViewById(R.id.item_list_question_img_id);
//            txtTitle = itemView.findViewById(R.id.item_list_question_txt_title_id);
//            txtContent = itemView.findViewById(R.id.item_list_question_txt_content_id);

            imgThumb = itemView.findViewById(R.id.k_item_question_img_content_id);
            // txtTitle = itemView.findViewById(R.id.item_list_question_txt_title_id);
            txtContent = itemView.findViewById(R.id.k_item_question_txt_content_id);
            txtName = itemView.findViewById(R.id.item_list_question_txt_name_id);
            txtTitle = itemView.findViewById(R.id.k_item_question_txt_title_id);

            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent i = new Intent(_activity, QuestionDetailsActivity.class);
                    i.putExtra("question_id", _questionItems.get(getAdapterPosition()).getQuestionId());
                    i.putExtra("question_title", _questionItems.get(getAdapterPosition()).getQuestionTitle().trim());
                    i.putExtra("question_content", _questionItems.get(getAdapterPosition()).getQuestionContent().trim());
                    i.putExtra("speciality_id", _questionItems.get(getAdapterPosition()).getSpecialityId());
                    _activity.startActivity(i);
                }
            });
        }
    }

    /**
     * Biding dữ liệu lên view
     * @param viewHolder : viewholder
     * @param position : vị trí item
     */
    private void populateItemRows(ItemViewHolder viewHolder, int position)
    {
        String urlImage = URLUtils.URL_IMAGES;
        MQuestionItem.QItem item = _questionItems.get(position);
        Glide.with(_activity).load(urlImage + item.getQuestionId()).error(R.drawable.ic_image_holder).centerCrop().into(viewHolder.imgThumb);
        viewHolder.txtName.setText(item.getFullname());
        viewHolder.txtTitle.setText(item.getQuestionTitle().trim());
        viewHolder.txtContent.setText(item.getQuestionContent().trim());
    }
}
