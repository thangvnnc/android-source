package com.app.khambenh.bacsiviet.utils;

public class URLUtils {
    public static final String SEVER = "https://medixlink.com/";

    public static final String URL_LOGIN_MOBILE = SEVER + "dang-nhap-mobile"; // "http://bacsiviet.vn/dang-nhap-mobile"
    public static final String URL_APP_FABOOK_API = SEVER + "apiappfacebook";  // "http://bacsiviet.vn/apiappfacebook"
    public static final String URL_CHECK_ACCOUNT = SEVER + "check-account-exist";  // "http://medixlink.com/check-account-exist"
    public static final String URL_REGISTER = SEVER + "dangkyapp";  // "http://bacsiviet.vn/dangkyapp"
//    public static final String URL_TIME_CALL = SEVER + "times-call";  // "http://bacsiviet.vn/times-call"
    public static final String URL_TIME_CALL = SEVER + "times-call-v2";  // "http://bacsiviet.vn/times-call-v2"
    public static final String URL_LIST_PROMOTION = SEVER + "apilistkhuyenmai";  // "http://bacsiviet.vn/apilistkhuyenmai"
    public static final String URL_INFO_DOCTOR = SEVER + "infodoctor";  // "http://bacsiviet.vn/infodoctor"
    public static final String URL_LIST_DOCTOR = SEVER + "danh-sach/bac-si";  // "http://bacsiviet.vn/danh-sach/bac-si"

    public static final String URL_CHECK_TO_CALL = SEVER + "kiem-tra-vi-tien";  //https://medixlink.com/kiem-tra-vi-tien
    public static final String URL_CHARGE_MESSAGE = SEVER + "api/thanh-toan-tin-nhan";  //https://medixlink.com/thanh-toan-tin-nhan
    public static final String URL_RECHARGE = SEVER + "nap-tien-vao-vi";  //https://medixlink.com/nap-tien-vao-vi
    public static final String URL_UPDATE_USER = SEVER + "cap-nhat-user";  //https://medixlink.com/cap-nhat-user
    public static final String URL_SEARCH_LOCATION = SEVER + "vi-tri/timkiem";  //https://medixlink.com/cap-nhat-user
    public static final String URL_SET_LOCATION = SEVER + "vi-tri/set";  //https://medixlink.com/vi-tri/set
    public static final String URL_GET_LOCATION = SEVER + "vi-tri/get";  //https://medixlink.com/vi-tri/get
    public static final String URL_CHECK_PHONE = SEVER + "check/existphone";  //https://medixlink.com/check/existphone

    public static final String URL_GET_SPECIALITIES = SEVER + "api/chuyen-khoa";
    public static final String URL_GET_DOCTORS_BY_SPECIALITY = SEVER + "api/danh-sach/bac-si";
    public static final String URL_GET_CLINICS_BY_SPECIALITY = SEVER + "api/danh-sach/phong-kham";
    public static final String URL_GET_LIST_PROVINCE = SEVER + "api/danh-sach/tinh-thanh";
    public static final String URL_CHANGE_PASS = SEVER + "api/doi-mat-khau";

    public static final String URL_PUSH_NITIFICATION = "https://fcm.googleapis.com/fcm/send";
    public static final String URL_CHECK_VERSION_UPDATE = SEVER + "/api/update/version"; // "http://bacsiviet.vn/api/update/version"

    public static final String URL_LIST_COMMENT = SEVER + "apihoibacsi/danhsachbinhluan";
    public static final String URL_INSERT_COMMENT = SEVER + "apihoibacsi/traloicauhoi";
    public static final String URL_UPLOAD_IMAGE = SEVER + "api/upload-image";
    public static final String URL_INSERT_QUESTION = SEVER + "apihoibacsi/datcauhoi";
    public static final String URL_LIST_QUESTION = SEVER + "apihoibacsi/danhsach";
    public static final String URL_IMAGES = SEVER + "public/images/";
}
