package com.app.khambenh.bacsiviet.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.khambenh.bacsiviet.R;

public class SampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
    }
}
