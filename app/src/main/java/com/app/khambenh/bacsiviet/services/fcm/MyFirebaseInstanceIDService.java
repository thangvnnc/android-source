package com.app.khambenh.bacsiviet.services.fcm;

import android.util.Log;

import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_DEVICE_TOKEN, token);
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
