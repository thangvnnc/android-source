package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.ViewImageFullScreenActivity;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.Date;

public class MedicalRecord implements Item {

    private String title;
    private String note;
    private ArrayList<String> arrImages;
    private int userID;

    private Long createDate;
    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }


    private ArrayList<Uri> arrUris = new ArrayList<>();
    ListAdapter listAdapter;
    final ArrayList<Item> arrayListImage = new ArrayList<>();
    final ArrayList<String> stringArrayList = new ArrayList<>();
    String URL_FullImage;


    public ArrayList<String> getArrImages() {
        return arrImages;
    }

    public void setArrImages(ArrayList<String> arrImages) {
        this.arrImages = arrImages;
    }

    public int getUserID() {

        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    @Override
    public View getView(final Context context, LayoutInflater layoutInflater, View convertView, final int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_medical_record, null);
        } else {
            rowView = convertView;
        }
        final ProgressBar progress_bar = (ProgressBar) rowView.findViewById(R.id.progress_bar);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtTitle);
        TextView txtNote = (TextView) rowView.findViewById(R.id.txtNote);
        final ImageView imgShow = (ImageView) rowView.findViewById(R.id.imgShow);
        final TwoWayView horizontal_layout_load_image = (TwoWayView) rowView.findViewById(R.id.horizontal_layout_load_image);


        txtTitle.setText(getTitle());
        String note = context.getString(R.string.note) + ": " + getNote();
        txtNote.setText(note);

        if (getArrImages().size() > 0) {
            imgShow.setVisibility(View.VISIBLE);
            horizontal_layout_load_image.setVisibility(View.VISIBLE);
            progress_bar.setVisibility(View.VISIBLE);
            Glide.with(context).load(getArrImages().get(0)).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    progress_bar.setVisibility(View.GONE);
                    URL_FullImage = getArrImages().get(0);
                    return false;
                }
            }).into(imgShow);

            //
            arrayListImage.clear();
            for (int i = 0; i < getArrImages().size(); i++) {

                Uri uri = Uri.parse(getArrImages().get(i));
                ImageOfMedicalRecord imageOfMedicalRecord = new ImageOfMedicalRecord(uri);
                arrayListImage.add(imageOfMedicalRecord);
            }
            listAdapter = new ListAdapter(context, arrayListImage);

            imgShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context,ViewImageFullScreenActivity.class);
                    intent.putStringArrayListExtra(Consts.EXTRA__LIST_IMAGE, getArrImages());
                    intent.putExtra(Consts.EXTRA_POSITION, 0);
                    context.startActivity(intent);
                }
            });

            horizontal_layout_load_image.setAdapter(listAdapter);

            horizontal_layout_load_image.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    final ImageOfMedicalRecord imageOfMedicalRecord = (ImageOfMedicalRecord) arrayListImage.get(position);
                    progress_bar.setVisibility(View.VISIBLE);
                    Glide.with(context).load(imageOfMedicalRecord.getUri()).listener(new RequestListener<Uri, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progress_bar.setVisibility(View.GONE);
                            URL_FullImage = imageOfMedicalRecord.getUri().toString();

                            imgShow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context,ViewImageFullScreenActivity.class);
                                    intent.putStringArrayListExtra(Consts.EXTRA__LIST_IMAGE, getArrImages());
                                    intent.putExtra(Consts.EXTRA_POSITION, position);
                                    context.startActivity(intent);
                                }
                            });
                            return false;
                        }
                    }).into(imgShow);
                }
            });

        } else {
            progress_bar.setVisibility(View.GONE);
            imgShow.setVisibility(View.GONE);
            horizontal_layout_load_image.setVisibility(View.GONE);
        }

        return rowView;
    }

}
