package com.app.khambenh.bacsiviet.stringee.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.stringee.define.StringeeSound;
import com.app.khambenh.bacsiviet.stringee.define.TransferKeys;
import com.app.khambenh.bacsiviet.stringee.log.LogStringee;
import com.app.khambenh.bacsiviet.stringee.service.StringeeService;
import com.app.khambenh.bacsiviet.stringee.utils.TimerTaskImp;
import com.app.khambenh.bacsiviet.stringee.utils.TimerTask;
import com.app.khambenh.bacsiviet.util.ResultUser;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.stringee.StringeeClient;
import com.stringee.call.StringeeCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OutgoingCallActivity extends AppCompatActivity implements View.OnClickListener, TimerTaskImp {

    private FrameLayout mLocalViewContainer;
    private FrameLayout mRemoteViewContainer;
    private TextView tvTo;
    private TextView tvState;
    private TextView txtTimer;
    private ImageButton btnMute;
    private ImageButton btnSpeaker;
    private ImageButton btnVideo;
    private ImageButton btnSwitch;

    private StringeeCall mStringeeCall;
    private String from;
    private String to;
    private String name;
    private boolean isVideoCall;
    private boolean isMute = false;
    private boolean isSpeaker = false;
    private boolean isVideo = false;

    private StringeeCall.MediaState mMediaState;
    private StringeeCall.SignalingState mSignalingState;
    private TimerTask timerTask = null;

    public static final String TAG = "OutgoingCallActivity";
    public static final int REQUEST_PERMISSION_CALL = 1;
    public static final int REQUEST_PERMISSION_CAMERA = 2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outgoing_call);

        from = getIntent().getStringExtra("from");
        to = getIntent().getStringExtra("to");
        name = getIntent().getStringExtra("name");
        isVideoCall = getIntent().getBooleanExtra("is_video_call", false);

        mLocalViewContainer = (FrameLayout) findViewById(R.id.v_local);
        mRemoteViewContainer = (FrameLayout) findViewById(R.id.v_remote);

        tvTo = (TextView) findViewById(R.id.tv_to);
        tvTo.setText(name);
        tvState = (TextView) findViewById(R.id.tv_state);
        txtTimer = findViewById(R.id.tv_timer);
        timerTask = new TimerTask(txtTimer);
        timerTask.setTimerTaskImp(this);

        btnMute = (ImageButton) findViewById(R.id.btn_mute);
        btnMute.setOnClickListener(this);
        btnSpeaker = (ImageButton) findViewById(R.id.btn_speaker);
        btnSpeaker.setOnClickListener(this);
        btnVideo = (ImageButton) findViewById(R.id.btn_video);
        btnVideo.setOnClickListener(this);
        btnSwitch = (ImageButton) findViewById(R.id.btn_switch);
        btnSwitch.setOnClickListener(this);

        isSpeaker = isVideoCall;
        if (isSpeaker) {
            btnSpeaker.setImageResource(R.drawable.ic_speaker_on);
        } else {
            btnSpeaker.setImageResource(R.drawable.ic_speaker_off);
        }

        isVideo = isVideoCall;
        if (isVideo) {
            btnVideo.setImageResource(R.drawable.ic_video);
        } else {
            btnVideo.setImageResource(R.drawable.ic_video_off);
        }

        ImageButton btnEnd = (ImageButton) findViewById(R.id.btn_end);
        btnEnd.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> lstPermissions = new ArrayList<>();

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.RECORD_AUDIO);
            }

            if (isVideoCall) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    lstPermissions.add(Manifest.permission.CAMERA);
                }
            }

            if (lstPermissions.size() > 0) {
                String[] permissions = new String[lstPermissions.size()];
                for (int i = 0; i < lstPermissions.size(); i++) {
                    permissions[i] = lstPermissions.get(i);
                }
                ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION_CALL);
                return;
            }
        }

        makeCall();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        boolean isGranted = false;
        if (grantResults.length > 0) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    isGranted = false;
                    break;
                } else {
                    isGranted = true;
                }
            }
        }
        switch (requestCode) {
            case REQUEST_PERMISSION_CALL:
                if (!isGranted) {
                    finish();
                } else {
                    makeCall();
                }
                break;
            case REQUEST_PERMISSION_CAMERA:
                if (isGranted) {
                    enableOrDisableVideo();
                }
                break;
        }
    }

    private void makeCall() {
        StringeeClient client = StringeeService.getInstance().stringeeClient;
        mStringeeCall = new StringeeCall(this, client, from, to);
        mStringeeCall.setVideoCall(isVideoCall);

        mStringeeCall.setCallListener(new StringeeCall.StringeeCallListener() {
            @Override
            public void onSignalingStateChange(StringeeCall stringeeCall, final StringeeCall.SignalingState signalingState, String s, int i, String s1) {
                LogStringee.error("Stringee", "======== Custom data: " + stringeeCall.getCustomDataFromYourServer());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSignalingState = signalingState;
                        Intent soundReceive = new Intent("service.Broadcast");
                        soundReceive.putExtra(TransferKeys.KEY, StringeeSound.SOUND_KEY);
                        if (signalingState == StringeeCall.SignalingState.CALLING) {
                            tvState.setText("Outgoing call");
                        } else if (signalingState == StringeeCall.SignalingState.RINGING) {
                            soundReceive.putExtra(StringeeSound.SOUND_KEY, StringeeSound.RING);
                            sendBroadcast(soundReceive);
                            tvState.setText("Ringing");
                        } else if (signalingState == StringeeCall.SignalingState.ANSWERED) {
                            soundReceive.putExtra(StringeeSound.SOUND_KEY, StringeeSound.OFF);
                            sendBroadcast(soundReceive);
                            tvState.setText("Starting");
                            if (mMediaState == StringeeCall.MediaState.CONNECTED) {
                                tvState.setText("Started");
                            }
                        } else if (signalingState == StringeeCall.SignalingState.BUSY) {
                            soundReceive.putExtra(StringeeSound.SOUND_KEY, StringeeSound.BUSY);
                            sendBroadcast(soundReceive);
                            tvState.setText("Busy");
                            mStringeeCall.hangup();
                            finish();
                        } else if (signalingState == StringeeCall.SignalingState.ENDED) {
                            soundReceive.putExtra(StringeeSound.SOUND_KEY, StringeeSound.END);
                            sendBroadcast(soundReceive);
                            tvState.setText("Ended");
                            mStringeeCall.hangup();
                            timerTask.stop();
                            finish();
                        } else {
                            soundReceive.putExtra(StringeeSound.SOUND_KEY, StringeeSound.OFF);
                            sendBroadcast(soundReceive);
                        }
                    }
                });
            }

            @Override
            public void onError(StringeeCall stringeeCall, int i, String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogStringee.toastAnywhere("Fails to make call.");
                    }
                });
            }

            @Override
            public void onHandledOnAnotherDevice(StringeeCall stringeeCall, StringeeCall.SignalingState signalingState, String s) {

            }

            @Override
            public void onMediaStateChange(StringeeCall stringeeCall, final StringeeCall.MediaState mediaState) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMediaState = mediaState;
                        if (mediaState == StringeeCall.MediaState.CONNECTED) {
                            if (mSignalingState == StringeeCall.SignalingState.ANSWERED) {
                                tvState.setText("Started");
                                timerTask.start();
                            }
                        }
                    }
                });
            }

            @Override
            public void onLocalStream(final StringeeCall stringeeCall) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (stringeeCall.isVideoCall()) {
                            mLocalViewContainer.addView(stringeeCall.getLocalView());
                            stringeeCall.renderLocalView(true);
                        }
                    }
                });
            }

            @Override
            public void onRemoteStream(final StringeeCall stringeeCall) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (stringeeCall.isVideoCall()) {
                            mRemoteViewContainer.addView(stringeeCall.getRemoteView());
                            stringeeCall.renderRemoteView(false);
                        }
                    }
                });
            }

            @Override
            public void onCallInfo(StringeeCall stringeeCall, final JSONObject jsonObject) {
            }
        });

        mStringeeCall.makeCall();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_mute:
                isMute = !isMute;
                if (isMute) {
                    btnMute.setImageResource(R.drawable.ic_mute);
                } else {
                    btnMute.setImageResource(R.drawable.ic_mic);
                }
                if (mStringeeCall != null) {
                    mStringeeCall.mute(isMute);
                }
                break;
            case R.id.btn_speaker:
                isSpeaker = !isSpeaker;
                if (isSpeaker) {
                    btnSpeaker.setImageResource(R.drawable.ic_speaker_on);
                } else {
                    btnSpeaker.setImageResource(R.drawable.ic_speaker_off);
                }
                if (mStringeeCall != null) {
                    mStringeeCall.setSpeakerphoneOn(isSpeaker);
                }
                break;
            case R.id.btn_end:
                mStringeeCall.hangup();
                Intent soundReceive = new Intent("service.Broadcast");
                soundReceive.putExtra(TransferKeys.KEY, StringeeSound.SOUND_KEY);
                soundReceive.putExtra(StringeeSound.SOUND_KEY, StringeeSound.END);
                sendBroadcast(soundReceive);
                timerTask.stop();
                tvState.setText("Ended");
                finish();
                break;
            case R.id.btn_video:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        String[] permissions = {Manifest.permission.CAMERA};
                        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION_CAMERA);
                        return;
                    }
                }
                enableOrDisableVideo();
                break;
            case R.id.btn_switch:
                if (mStringeeCall != null) {
                    mStringeeCall.switchCamera(null);
                }
                break;
        }
    }

    private void enableOrDisableVideo() {
        isVideo = !isVideo;
        if (isVideo) {
            btnVideo.setImageResource(R.drawable.ic_video);
        } else {
            btnVideo.setImageResource(R.drawable.ic_video_off);
        }
        if (mStringeeCall != null) {
            if (!mStringeeCall.isVideoCall()) { // Send camera request
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("type", "cameraRequest");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mStringeeCall.sendCallInfo(jsonObject);
            }
            mStringeeCall.enableVideo(isVideo);
        }
    }

    private void putCountTime(int timer, QBUser qbDoctor) {
        try {

            QBUser qbUser = SharedPrefsHelper.getInstance().getQbUser();
            String email = qbUser.getLogin();
            JSONObject postparams = new JSONObject();
            postparams.put("user_email", email);
            postparams.put("doctor_email", "" + qbDoctor.getLogin());
            postparams.put("user_type_id", "" + 2);
            float times = (float)timer / 60;
            postparams.put("times", times);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_TIME_CALL, postparams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response != null) {
                                try {
                                    if (response.has("isSave")) {
                                        boolean isSave = response.getBoolean("isSave");
                                        if (isSave) {
                                            if (response.has("balance")) {
                                                long balance = response.getLong("balance");
                                                SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_BALANCE, balance);
                                                Intent intent = new Intent();
                                                intent.setAction(Consts.ACTION_UPDATE_BALANCE);
                                                sendBroadcast(intent);
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.d("test_sss", "" + response);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            Log.e("ERROR", "time call");
                            //Failure Callback

                        }
                    });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStartTimerTask() {

    }

    @Override
    public void onStopTimerTask(int timer) {
        Log.e(TAG, timer + "");
        if (timer <=0) {
            return;
        }

        CommonUtils.GetUserServer(to, new ResultUser() {
            @Override
            public void onResult(QBUser qbUser) {
                putCountTime(timer, qbUser);
            }
        });
    }

    @Override
    public void onTickTimerTask(int timer) {

    }
}
