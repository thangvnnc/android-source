package com.app.khambenh.bacsiviet.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.khambenh.bacsiviet.CoreApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPrefsHelper {
    private static final String SHARED_PREFS_NAME = "qb";

    private static final String QB_USER_ID = "qb_user_id";
    private static final String QB_USER_LOGIN = "qb_user_login";
    private static final String QB_USER_PASSWORD = "qb_user_password";
    private static final String QB_USER_FULL_NAME = "qb_user_full_name";
    private static final String QB_USER_TAGS = "qb_user_tags";
    private static final String QB_USER_CUSTOMDATA = "qb_user_custom_data";
    private static final String QB_USER_EXTERNAL_ID = "qb_user_external_id";
    private static final String QB_USER_WEBSITE = "qb_user_website";

    private static final String QB_USER_OPPONENT_ID = "QB_USER_OPPONENT_ID";
    private static final String QB_USER_OPPONENT_LOGIN = "QB_USER_OPPONENT_LOGIN";
    private static final String QB_USER_OPPONENT_PASSWORD = "QB_USER_OPPONENT_PASSWORD";
    private static final String QB_USER_OPPONENT_FULL_NAME = "QB_USER_OPPONENT_FULL_NAME";
    private static final String QB_USER_OPPONENT_TAGS = "QB_USER_OPPONENT_TAGS";
    private static final String QB_USER_OPPONENT_CUSTOMDATA = "QB_USER_OPPONENT_CUSTOMDATA";
    private static final String QB_USER_OPPONENT_EXTERNAL_ID = "QB_USER_OPPONENT_EXTERNAL_ID";
    private static final String QB_USER_OPPONENT_WEBSITE = "QB_USER_OPPONENT_WEBSITE";

    private static SharedPrefsHelper instance;

    private SharedPreferences sharedPreferences;

    public static synchronized SharedPrefsHelper getInstance() {
        if (instance == null) {
            instance = new SharedPrefsHelper();
        }

        return instance;
    }

    private SharedPrefsHelper() {
        instance = this;
        sharedPreferences = CoreApp.getInstance().getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void delete(String key) {
        if (sharedPreferences.contains(key)) {
            getEditor().remove(key).commit();
        }
    }

    public void save(String key, Object value) {
        SharedPreferences.Editor editor = getEditor();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-supported preference");
        }

        editor.commit();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) sharedPreferences.getAll().get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defValue) {
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public boolean has(String key) {
        boolean hasKey = sharedPreferences.contains(key);
        return hasKey;
    }


    public void saveQbUser(QBUser qbUser) {
        save(QB_USER_ID, qbUser.getId());
        save(QB_USER_LOGIN, qbUser.getLogin());
        save(QB_USER_PASSWORD, qbUser.getPassword());
        save(QB_USER_FULL_NAME, qbUser.getFullName());
        save(QB_USER_TAGS, qbUser.getTags().get(0));
        save(QB_USER_CUSTOMDATA, qbUser.getCustomData());
        save(QB_USER_EXTERNAL_ID, qbUser.getExternalId());
        save(QB_USER_WEBSITE, qbUser.getWebsite());
        QBChatService.getInstance().setUser(qbUser);
    }

    public QBUser getCurrentUser() {
        QBUser qbUser = new QBUser();
        Integer id = get(QB_USER_ID);
        String login = get(QB_USER_LOGIN);
        String fullname = get(QB_USER_FULL_NAME);
        String customData = get(QB_USER_CUSTOMDATA);
        String externalId = get(QB_USER_OPPONENT_EXTERNAL_ID);
        String website = get(QB_USER_WEBSITE);
        qbUser.setId(id);
        qbUser.setLogin(login);
        qbUser.setFullName(fullname);
        qbUser.setCustomData(customData);
        qbUser.setExternalId(externalId);
        qbUser.setWebsite(website);
        return qbUser;
    }

    public void saveOpponentQbUser(QBUser qbUser) {
        save(QB_USER_OPPONENT_ID, qbUser.getId());
        save(QB_USER_OPPONENT_LOGIN, qbUser.getLogin());
        save(QB_USER_OPPONENT_PASSWORD, qbUser.getPassword());
        save(QB_USER_OPPONENT_FULL_NAME, qbUser.getFullName());
        save(QB_USER_OPPONENT_TAGS, qbUser.getTags() == null ? "" : qbUser.getTags().get(0));
        save(QB_USER_OPPONENT_CUSTOMDATA, qbUser.getCustomData());
        save(QB_USER_OPPONENT_EXTERNAL_ID, qbUser.getExternalId());
        save(QB_USER_OPPONENT_WEBSITE, qbUser.getWebsite());
    }

    public QBUser getOpponentQbUser() {
        QBUser qbUser = new QBUser();
        Integer id = get(QB_USER_OPPONENT_ID);
        String login = get(QB_USER_OPPONENT_LOGIN);
        String pwd = get(QB_USER_OPPONENT_PASSWORD);
        String fullname = get(QB_USER_OPPONENT_FULL_NAME);
        String customData = get(QB_USER_OPPONENT_CUSTOMDATA);
        String externalId = get(QB_USER_OPPONENT_EXTERNAL_ID);
        String website = get(QB_USER_OPPONENT_EXTERNAL_ID);
        qbUser.setId(id);
        qbUser.setLogin(login);
        qbUser.setPassword(pwd);
        qbUser.setFullName(fullname);
        qbUser.setCustomData(customData);
        qbUser.setExternalId(externalId);
        qbUser.setWebsite(website);
        return qbUser;
    }

    public String getOpponentFullname(){
        return get(QB_USER_OPPONENT_FULL_NAME);
    }

    public String getOpponentAvatar(){
        String custom_data = get(QB_USER_OPPONENT_CUSTOMDATA);
        String avatar ="";
        if(custom_data.contains("-_-")) {
            String[] str = custom_data.split("-_-");
            avatar = str[1];
        }
        return avatar;
    }
    public int getOpponentID(){
        return get(QB_USER_OPPONENT_ID);
    }

    public void removeQbUser() {
        delete(QB_USER_ID);
        delete(QB_USER_LOGIN);
        delete(QB_USER_PASSWORD);
        delete(QB_USER_FULL_NAME);
        delete(QB_USER_TAGS);
    }

    public QBUser getQbUser() {
        if (hasQbUser()) {
            Integer id = get(QB_USER_ID);
            String login = get(QB_USER_LOGIN);
            String password = get(QB_USER_PASSWORD);
            String fullName = get(QB_USER_FULL_NAME);
            String tagsInString = get(QB_USER_TAGS);

            List<String> tags = null;

            if (tagsInString != null) {
                tags = new ArrayList<>();
                String [] arr = tagsInString.split(",");
                tags.addAll(Arrays.asList(arr));
            }

            QBUser user = QBChatService.getInstance().getUser();
            user.setTags(tags);
            return user;
        } else {
            return null;
        }
    }

    public boolean hasQbUser() {
        return has(QB_USER_LOGIN);
    }

    public void clearAllData() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear().commit();
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }
}
