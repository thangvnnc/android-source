package com.app.khambenh.bacsiviet.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.app.khambenh.bacsiviet.utils.Item;

import java.util.List;

public class ListAdapter extends ArrayAdapter<Item> {
    private LayoutInflater mInflater;

    public ListAdapter(Context context, List<Item> items) {
        super(context, 0, items);
        mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View View = null;

        try{
            Item item = getItem(position);
            View = item.getView(getContext(), mInflater, convertView, position);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return View;
    }

    @Override
    public Item getItem(int position) {
        return super.getItem(position);
    }

    public static class ViewHolder {
        public View View;
    }
}
