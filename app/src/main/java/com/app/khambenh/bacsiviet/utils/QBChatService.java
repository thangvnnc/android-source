package com.app.khambenh.bacsiviet.utils;

public class QBChatService {
    private static QBChatService instance = null;
    private static QBUser qUser = null;

    public void setInstance(QBChatService qbChatService) {
        instance = qbChatService;
    }

    public static QBChatService getInstance(){
        if (instance == null) {
            instance = new QBChatService();
        }
        return instance;
    }

    public void setUser(QBUser qbUser) {
        qUser = qbUser;
    }

    public QBUser getUser() {
        if(qUser == null) {
            qUser = SharedPrefsHelper.getInstance().getCurrentUser();
        }
        return qUser;
    }
}
