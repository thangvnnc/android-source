package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.db.FireStoreFunction;
import com.app.khambenh.bacsiviet.db.QbUsersDbManager;
import com.app.khambenh.bacsiviet.fragments.ConversationFirebaseFragment;
import com.app.khambenh.bacsiviet.fragments.DoctorFragment;
import com.app.khambenh.bacsiviet.fragments.InfoFragment;
import com.app.khambenh.bacsiviet.fragments.MapFragment;
import com.app.khambenh.bacsiviet.fragments.QuestionFragment;
import com.app.khambenh.bacsiviet.services.bubble.BubbleService;
import com.app.khambenh.bacsiviet.util.ShowNoti;
import com.app.khambenh.bacsiviet.util.UserFirebase;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.LanguageUtils;
import com.app.khambenh.bacsiviet.utils.PermissionsChecker;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.QbUsersHolder;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.UsersUtils;
import com.facebook.login.LoginManager;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nex3z.notificationbadge.NotificationBadge;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

//import com.app.khambenh.bacsiviet.services.CallService;
//import com.app.khambenh.bacsiviet.utils.CallUltils;

//import com.app.khambenh.bacsiviet.fragments.ConversionFragment;
//import com.app.khambenh.bacsiviet.utils.WebRtcSessionManager;


/**
 * QuickBlox team
 */
public class OpponentsActivity extends BaseActivity {
    private static final String TAG = OpponentsActivity.class.getSimpleName();

    private static final long ON_ITEM_CLICK_DELAY = TimeUnit.SECONDS.toMillis(10);
    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;

    private TextView txtVersion = null;

    //private ListView opponentsListView;
    private QBUser currentUser;
    private QbUsersDbManager dbManager;
    private boolean isRunForCall;
//    private WebRtcSessionManager webRtcSessionManager;

    private PermissionsChecker checker;
    SharedPreferences pre;
    private SearchView searchView;

//    private BubblesManager bubblesManager;
    private NotificationBadge notificationBadge;

    //TODO: Tab layout
    private ViewPager main_pager;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    //    private String[] mTitles = {"Trò chuyện", "Bác sĩ", "Người dùng", "Khuyến mại"};
    List<String> mTitles; //add by Tuan
    List<String> arrBubbleHasShow = new ArrayList<>(); //add by Tuan
    private CommonTabLayout mTabLayout_2;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private int[] mIconSelectIds = {
            R.drawable.k_ic_conversation_sel, R.drawable.k_ic_doctor_sel, R.drawable.k_ic_qa_sel,
            R.drawable.k_ic_map_sel, R.drawable.k_ic_menu_sel};
    private int[] mIconUnselectIds = {
            R.drawable.k_ic_conversation_un, R.drawable.k_ic_doctor_un, R.drawable.k_ic_qa_un,
            R.drawable.k_ic_map_un, R.drawable.k_ic_menu_un};
    public static boolean isOpen = false;
    private boolean isShowBubble = false;
    private boolean loginfb = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_LOGIN_FB, Boolean.class);
    View currenBubble;
    String userType;
    int curUserId;

    WindowManager.LayoutParams mWindowParams;
    Activity activity = null;

    public static void start(Context context, boolean isRunForCall) {
        Intent intent = new Intent(context, OpponentsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Consts.EXTRA_IS_STARTED_FOR_CALL, isRunForCall);
        context.startActivity(intent);
    }

    BroadcastReceiver BroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Consts.ACTION_VOICE_CALL_DIALOG)) {
                CallUltils.isFromChatActivity = true;
                Integer doctorId = intent.getIntExtra(Consts.EXTRA_DOCTOR_ID, 0);
                QBUser qbUser = QbUsersHolder.getInstance().getUserById(doctorId);

                CallUltils.checkToCall(qbUser, activity, Consts.TYPE_CALL_AUDIO);
            } else if (intent.getAction().equals(Consts.ACTION_VIDEO_CALL_DIALOG)) {
                CallUltils.isFromChatActivity = true;
                Integer doctorId = intent.getIntExtra(Consts.EXTRA_DOCTOR_ID, 0);
                QBUser qbUser = QbUsersHolder.getInstance().getUserById(doctorId);
                CallUltils.checkToCall(qbUser, OpponentsActivity.this, Consts.TYPE_CALL_VIDEO);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opponents);
        activity = this;
//        BubbleService.show(getApplicationContext());
        pre = getSharedPreferences("app_bacsi", MODE_PRIVATE);
        userType = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_TYPE, String.class);
        curUserId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);
        initFields();
        initDefaultActionBar();
        initUi();

        //

//        //add by Tuan
//        initBubbleMessager();

        //
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        //
        // TODO: Login Chat
//        QBUser qbUserLoginChat = sharedPrefsHelper.getQbUser();
//        qbUserLoginChat.setPassword("x6Bt0VDy5");
//        login(qbUserLoginChat);

        //
//        if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
//            CallActivity.start(OpponentsActivity.this, true);
//        }

//        checker = new PermissionsChecker(getApplicationContext());


        //Update thong tin user
//        if (!pre.getString("user_name", "").equals("") && !pre.getString("pass_word", "").equals("")) {
//            try {
//                processLogin(pre.getString("user_name", ""), pre.getString("pass_word", ""));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }


//
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (!Settings.canDrawOverlays(this)) {
//                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
//                        Uri.parse("package:" + getPackageName()));
//                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
//            }
//        } else {
//            Intent intent = new Intent(this, Service.class);
//            startService(intent);
//        }


//        checkPermission();
        registrationToken();
        registerBroadcastFilter();
        if (getIntent().getExtras() != null) {
            String senderId = getIntent().getExtras().getString(Consts.EXTRA_USER_ID);

            if (senderId != null) {
                Intent intent = new Intent(this, MessageFirebaseActivity.class);
                intent.putExtra(Consts.EXTRA_USER_ID, senderId);
                startActivity(intent);
            }
        }
    }

    private void registrationToken() {

//        FirebaseMessaging.getInstance().subscribeToTopic("" + curUserId);

        UserFirebase userFirebase = new UserFirebase();
        userFirebase.setUser_id(String.valueOf(curUserId));
        userFirebase.setDevice_token(FirebaseInstanceId.getInstance().getToken());

        FireStoreFunction.getData(Consts.COLLECTION_USER)
                .document(userFirebase.getUser_id())
                .set(userFirebase)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", "Error writing document", e);
                    }
                });
    }


    private void registerBroadcastFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Consts.ACTION_VIDEO_CALL_DIALOG);
        filter.addAction(Consts.ACTION_VOICE_CALL_DIALOG);
        registerReceiver(BroadcastReceiver, filter);
    }

//    //TODO: processLogin
//    private void processLogin(String userName, String passWord) throws JSONException {
//        JSONObject postparams = new JSONObject();
//        postparams.put("email", "" + userName);
//        postparams.put("pwd", "" + passWord);
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                URLUtils.URL_LOGIN_MOBILE, postparams,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        if (response != null) {
//                            try {
//                                if (response.getString("isLogin").equals("true")) {
//                                    pre.edit().putString("id_face", "").commit();
//                                    pre.edit().putInt("paid", response.getInt("paid")).commit();
//                                    pre.edit().putString("user_type", response.getString("user_type")).commit();
//
//                                    if (response.has("id")) {
//                                        pre.edit().putString("user_id", "" + response.getString("id")).commit();
//                                    } else {
//                                        pre.edit().putString("user_id", "" + new Random().nextInt(1000000) + new Random().nextInt(10000)).commit();
//                                    }
//                                    if (response.has("image")) {
//                                        pre.edit().putString("custom_data", response.getString("fullname") + "-_-" + response.getString("image")).commit();
//                                    } else {
//                                        pre.edit().putString("custom_data", response.getString("fullname")).commit();
//                                    }
//
//                                }
//                            } catch (JSONException e) {
//                                Log.d("test_error", "" + e.getMessage());
//                            }
//                        }
//                    }
//
//
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                        //Failure Callback
//
//                    }
//                });
//        // Adding the request to the queue along with a unique string tag
//        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
//    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            Log.e("Intent", "notification");
//            isRunForCall = intent.getExtras().getBoolean(Consts.EXTRA_IS_STARTED_FOR_CALL);
//            if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
//                CallActivity.start(OpponentsActivity.this, true);
//            }
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.list_opponents);
    }

    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, Consts.PERMISSIONS);
    }

    private void initFields() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isRunForCall = extras.getBoolean(Consts.EXTRA_IS_STARTED_FOR_CALL);
        }


        currentUser = sharedPrefsHelper.getQbUser();
        dbManager = QbUsersDbManager.getInstance(getApplicationContext());
//        webRtcSessionManager = WebRtcSessionManager.getInstance(getApplicationContext());
    }


    private void initUi() {
        // DialogsActivity
        txtVersion = findViewById(R.id.txtversion);
        String appVersion = getCurrenVersion();
        txtVersion.setText(appVersion);

        main_pager = findViewById(R.id.main_pager);
        main_pager.setOffscreenPageLimit(5);
        mTabLayout_2 = findViewById(R.id.tablayout);
        mTitles = LanguageUtils.getListFromResource(this, R.array.arr_bottom_bar);

        for (int i = 0; i < mTitles.size(); i++) {
            mTabEntities.add(new TabEntity(mTitles.get(i), mIconSelectIds[i], mIconUnselectIds[i]));
        }
        tl_2();
        //
        mFragments.add(ConversationFirebaseFragment.getInstance());
        mFragments.add(DoctorFragment.getInstance());
        //mFragments.add(ClinicFragment.getInstance());
        mFragments.add(QuestionFragment.getInstance());
        // mFragments.add(SaleFragment.getInstance());
        mFragments.add(MapFragment.getInstance());
        mFragments.add(InfoFragment.getInstance());
        //
        main_pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mTabLayout_2.setMsgMargin(0, -5, 5);
        main_pager.setCurrentItem(0);
    }

    private String getCurrenVersion() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        return currentVersion;
    }

    private void tl_2() {
        mTabLayout_2.setTabData(mTabEntities);
        mTabLayout_2.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                main_pager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
                if (position == 0) {

//                    UnreadMsgUtils.show(mTabLayout_2.getMsgView(0), mRandom.nextInt(100) + 1);
                }
            }
        });

        main_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout_2.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        main_pager.setCurrentItem(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (userType.equalsIgnoreCase("user"))
            if (loginfb) {
                getMenuInflater().inflate(R.menu.menu_opponents_of_userfb, menu);
            } else {
                getMenuInflater().inflate(R.menu.menu_opponents_of_user, menu);
            }
        else {
            getMenuInflater().inflate(R.menu.activity_opponents, menu);
        }
//        final MenuItem myActionMenuItem = menu.findItem(R.id.search);
//        searchView = (SearchView) myActionMenuItem.getActionView();
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                myActionMenuItem.collapseActionView();
//                EventBus.getDefault().post(new MessageEvent(query));
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                EventBus.getDefault().post(new MessageEvent(s));
//                return false;
//            }
//        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.settings:
                showSettings();
                return true;
            case R.id.change_language:
                LanguageUtils.showDialogSelectLanguage(this);
                return true;
            case R.id.change_password:
                Intent changePass = new Intent(OpponentsActivity.this, ChangePassActivity.class);
                startActivity(changePass);
                return true;
//            case R.id.map_dr:
//                turnGPSOn();
//                return true;
            case R.id.log_out:
                logOut();
                return true;
            /*add by Binh 26-08-2-2018*/
            case R.id.account:
                startAccountPage();
                return true;
            /*add by Binh 26-08-2-2018*/
            case R.id.medical_record:
                Intent intent = new Intent(OpponentsActivity.this, MedicalRecordActivity.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*add by Binh*/
    private void startAccountPage() {
//        Intent accountPageIntent = new Intent(this, CustomerActivity.class);
//        startActivity(accountPageIntent);
    }

    private void showSettings() {
        SettingsActivity.start(this);
    }


    private void logOut() {
        LoginManager.getInstance().logOut();
//        unsubscribeFromPushes();
        startLogoutCommand();
        removeAllUserData();
//        QBUsers.signOut();
        ConversationFirebaseFragment.clearData();
        SharedPrefs.getInstance().clear();
        startLoginActivity();
        finish();
    }

    private void startLogoutCommand() {
//        CallService.logout(this);
    }

//    private void unsubscribeFromPushes() {
//        SubscribeService.unSubscribeFromPushes(this);
//    }

    private void removeAllUserData() {
        UsersUtils.removeUserData(getApplicationContext());
//        requestExecutor.deleteCurrentUser(currentUser.getId(), new QBEntityCallback<Void>() {
//            @Override
//            public void onSuccess(Void aVoid, Bundle bundle) {
//                Log.d(TAG, "Current user was deleted from QB");
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Log.e(TAG, "Current user wasn't deleted from QB " + e);
//            }
//        });
    }

    private void startLoginActivity() {
        LoginActivity.start(this);
        finish();
    }

//    private void login(final QBUser user) {
//        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
//            @Override
//            public void onSuccess(Void result, Bundle bundle) {
//
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Log.d("test_err", "" + e.getMessage());
//            }
//        });
//    }

    @Override
    public void onBackPressed() {
        if (searchView != null) {
            if (searchView.isIconified()) {
                // Hide
//                showDialogExit();
            } else {
                //Show
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.setIconified(true);
            }
        } else {
//            showDialogExit();
        }
    }

    private void showDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_close_app));
        builder.setMessage(getString(R.string.exit_app));
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                System.exit(0);
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

    @Subscribe
    public void onMessageEvent(ShowNoti event) {/* Do something */
        if (event != null && event.isShow()) {

        }
    }

//    private void initBubbleMessager() {
//        bubblesManager = new BubblesManager.Builder(this)
//                .setTrashLayout(R.layout.layout_bubble_remove)
//                .setInitializationCallback(new OnInitializedCallback() {
//                    @Override
//                    public void onInitialized() {
//                        //do something
//                    }
//                }).build();
//        bubblesManager.initialize();
//    }

//    private void showBubble(int countUnReadMessage) {
//        isShowBubble = true;
//        final BubbleLayout bubbleView = (BubbleLayout) LayoutInflater.from(this)
//                .inflate(R.layout.layout_bubble_messager, null);
//        CircleImageView avatar = (CircleImageView) bubbleView.findViewById(R.id.avatar);
////        Glide.with(this).load(qbChatDialog.getPhoto()).into(avatar);
//        currenBubble = bubbleView;
//        notificationBadge = (NotificationBadge) bubbleView.findViewById(R.id.badge_count);
//        notificationBadge.setNumber(countUnReadMessage);
//        bubbleView.setOnBubbleRemoveListener(new BubbleLayout.OnBubbleRemoveListener() {
//            @Override
//            public void onBubbleRemoved(BubbleLayout bubble) {
//
//            }
//        });
//        bubbleView.setOnBubbleClickListener(new BubbleLayout.OnBubbleClickListener() {
//            @Override
//            public void onBubbleClick(BubbleLayout bubble) {
////                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.app.khambenh.bacsiviet");
////                if (launchIntent != null) {
//                bubblesManager.removeBubble(bubbleView);
//                Intent intent = new Intent(OpponentsActivity.this, SplashActivity.class);
//                startActivity(intent);
//
//            }
//        });
//        bubbleView.setShouldStickToWall(true);
//        bubblesManager.addBubble(bubbleView, 60, 20);
//
//    }

    @Override
    protected void onStart() {
        super.onStart();
        CommonUtils.checkAllPermission(this);
        isOpen = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isOpen = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (bubblesManager != null) {
//            bubblesManager.recycle();
//        }
        unregisterReceiver(BroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpen = true;
        CommonUtils.checkAllPermission(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Consts.REQUEST_CODE_PERMISSION_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
            }
            break;
            case Consts.REQUEST_CODE_PERMISSION_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
            }
            break;
            case Consts.REQUEST_CODE_PERMISSION_COARSE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
            }
            break;
            case Consts.REQUEST_CODE_PERMISSION_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
            }
            break;
            case Consts.REQUEST_CODE_PERMISSION_PHONE: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
            }
            break;
            case Consts.REQUEST_CODE_PERMISSION_INTERNET: {
                if (grantResults.length > 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
            }
            break;
        }
    }
}