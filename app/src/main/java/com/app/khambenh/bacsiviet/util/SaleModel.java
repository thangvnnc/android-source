package com.app.khambenh.bacsiviet.util;

/**
 * Created by os-nguyenxuanduc on 4/3/2018.
 */

public class SaleModel {
    private int deal_id;
    private String image;
    private String deal_description;
    private String deal_content;
    private String link_detail;
    private String deal_title;

    public String getDeal_title() {
        return deal_title;
    }

    public void setDeal_title(String deal_title) {
        this.deal_title = deal_title;
    }

    public int getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(int deal_id) {
        this.deal_id = deal_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDeal_description() {
        return deal_description;
    }

    public void setDeal_description(String deal_description) {
        this.deal_description = deal_description;
    }

    public String getDeal_content() {
        return deal_content;
    }

    public void setDeal_content(String deal_content) {
        this.deal_content = deal_content;
    }

    public String getLink_detail() {
        return link_detail;
    }

    public void setLink_detail(String link_detail) {
        this.link_detail = link_detail;
    }
}
