package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Item;
import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import de.hdodenhof.circleimageview.CircleImageView;

public class Doctor implements Serializable, Item {
    @SerializedName("doctor_id")
    private int doctor_id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("doctor_name")
    private String doctor_name;

    @SerializedName("doctor_url")
    private String doctor_url;

    @SerializedName("profile_image")
    private String profile_image;

    @SerializedName("doctor_description")
    private String doctor_description;

    @SerializedName("experience")
    private String experience;

    @SerializedName("training")
    private String training;

    @SerializedName("doctor_address")
    private String doctor_address;

    @SerializedName("featured")
    private int featured;

    @SerializedName("province_id")
    private String province_id;

    @SerializedName("order_doctor")
    private String order_doctor;

    @SerializedName("doctor_timework")
    private String doctor_timework;

    @SerializedName("doctor_clinic")
    private String doctor_clinic;

    @SerializedName("status")
    private int status;

    @SerializedName("vote")
    private float vote;

    @SerializedName("specialitys")
    private String specialitys;

    private String speciality_name;

    public float getVote() {
        return vote;
    }

    public void setVote(float vote) {
        this.vote = vote;
    }

    public String getSpecialitys() {
        return specialitys;
    }

    public void setSpecialitys(String speciality_id) {
        this.specialitys = specialitys;
    }

    public String getSpeciality_name() {
        return speciality_name;
    }

    public void setSpeciality_name(String speciality_name) {
        this.speciality_name = speciality_name;
    }

    private int isOnline; // 0- not login app, 1- login app

    public int getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(int isOnline) {
        this.isOnline = isOnline;
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDoctor_url() {
        return doctor_url;
    }

    public void setDoctor_url(String doctor_url) {
        this.doctor_url = doctor_url;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getDoctor_description() {
        return doctor_description;
    }

    public void setDoctor_description(String doctor_description) {
        this.doctor_description = doctor_description;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getTraining() {
        return training;
    }

    public void setTraining(String training) {
        this.training = training;
    }

    public String getDoctor_address() {
        return doctor_address;
    }

    public void setDoctor_address(String doctor_address) {
        this.doctor_address = doctor_address;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getOrder_doctor() {
        return order_doctor;
    }

    public void setOrder_doctor(String order_doctor) {
        this.order_doctor = order_doctor;
    }

    public String getDoctor_timework() {
        return doctor_timework;
    }

    public void setDoctor_timework(String doctor_timework) {
        this.doctor_timework = doctor_timework;
    }

    public String getDoctor_clinic() {
        return doctor_clinic;
    }

    public void setDoctor_clinic(String doctor_clinic) {
        this.doctor_clinic = doctor_clinic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    @Override
    public View getView(final Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_speciality, null);
        } else {
            rowView = convertView;
        }
        CircleImageView imgAvatar = (CircleImageView)rowView.findViewById(R.id.imgAvatar);
        TextView txt_name = (TextView)rowView.findViewById(R.id.txt_name);
        ImageView img_status = (ImageView)rowView.findViewById(R.id.img_status);

        Glide.with(context).load(getProfile_image()).error(R.color.blue).into(imgAvatar);
        txt_name.setText(getDoctor_name());

        if(getFeatured()==1){
            img_status.setVisibility(View.VISIBLE);
        }else {
            img_status.setVisibility(View.INVISIBLE);
        }
        return rowView;
    }
}
