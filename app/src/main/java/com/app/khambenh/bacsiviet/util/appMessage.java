package com.app.khambenh.bacsiviet.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.ViewImageFullScreenActivity;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.adapters.ListAdapterViewType;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.ItemViewType;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.StaticCommon;
import com.bumptech.glide.Glide;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class appMessage implements ItemViewType {
    private String message;
    private String senderId;
    private String receiverId;
    private ArrayList<String> listAttachment;
    private String date;
    private String time;
    private int totalAttachment;
    private Date createTime;
    private String myAvatar;
    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMyAvatar() {
        return myAvatar;
    }

    public void setMyAvatar(String myAvatar) {
        this.myAvatar = myAvatar;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getTotalAttachment() {
        return totalAttachment;
    }

    public void setTotalAttachment(int totalAttachment) {
        this.totalAttachment = totalAttachment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public ArrayList<String> getListAttachment() {
        return listAttachment;
    }

    public void setListAttachment(ArrayList<String> listAttachment) {
        this.listAttachment = listAttachment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int getViewType() {
        return ListAdapterViewType.RowType.LIST_ITEM.ordinal();
    }

    @Override
    public View getView(final Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;
        int currUserId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);

        rowView = (View) layoutInflater.inflate(R.layout.item_list_message, null);

        final LinearLayout layout_chat_message_container = rowView.findViewById(R.id.layout_chat_message_container);
        RelativeLayout layout_message_content_container = rowView.findViewById(R.id.layout_message_content_container);
        TextView text_message = rowView.findViewById(R.id.text_message);
        final TextView text_message_info = rowView.findViewById(R.id.text_message_info);
        TwoWayView lvAttachment = rowView.findViewById(R.id.lvAttachment);
        final String message = getMessage();
 
        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setClipboard(context, message);
                Toast.makeText(context, context.getString(R.string.title_copy), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        text_message.setText(message);
        CircleImageView img_avatar = (CircleImageView) rowView.findViewById(R.id.img_avatar);
        text_message_info.setText(getTime());
        if (Integer.parseInt(getSenderId()) == currUserId) {
            layout_chat_message_container.setGravity(Gravity.RIGHT);
            layout_message_content_container.setGravity(Gravity.RIGHT);
            layout_message_content_container.setBackgroundResource(R.drawable.k_ic_message_bubble_right);
            img_avatar.setVisibility(View.INVISIBLE);

            if (position== StaticCommon.LastMessage) {
                text_message_info.setVisibility(View.VISIBLE);
                if (StaticCommon.statusMessage == 0) {
                    text_message_info.setText(R.string.notifi_status_chat_sending);
                } else if (StaticCommon.statusMessage == 1) {
                    text_message_info.setText(R.string.notifi_status_chat_received);
                } else if (StaticCommon.statusMessage == 2) {
                    text_message_info.setText(R.string.notifi_status_chat_seen);
                }
            }
        } else if (Integer.parseInt(getSenderId()) != currUserId) {
            layout_chat_message_container.setGravity(Gravity.LEFT);
            layout_message_content_container.setGravity(Gravity.LEFT);
            layout_message_content_container.setBackgroundResource(R.drawable.k_ic_message_bubble_left);
            if (getMyAvatar() != null) {
                Glide.with(context).load(getMyAvatar()).error(R.drawable.ic_other_user).into(img_avatar);
                img_avatar.setVisibility(View.VISIBLE);
            }
        }

        if(getTotalAttachment()>0) {
            ArrayList<Item> itemArrayList = new ArrayList<>();
            for (int i = 0; i < getTotalAttachment(); i++) {
                Uri uri = Uri.parse(getListAttachment().get(i));
                Attachment attachment = new Attachment(uri);
                itemArrayList.add(attachment);
            }

            ListAdapter listAdapter = new ListAdapter(context, itemArrayList);
            lvAttachment.setAdapter(listAdapter);
            lvAttachment.setVisibility(View.VISIBLE);
            lvAttachment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, ViewImageFullScreenActivity.class);
                    intent.putStringArrayListExtra(Consts.EXTRA__LIST_IMAGE, getListAttachment());
                    intent.putExtra(Consts.EXTRA_POSITION, 0);
                    context.startActivity(intent);
                }
            });
        }else {
            lvAttachment.setVisibility(View.GONE);
        }
        return rowView;
    }

    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }
}
