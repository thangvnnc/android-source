package com.app.khambenh.bacsiviet.utils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by tereha on 12.05.16.
 */
public class CollectionsUtils {


    public static ArrayList<Integer> getIdsSelectedOpponents(Collection<QBUser> selectedUsers) {
        ArrayList<Integer> opponentsIds = new ArrayList<>();
        if (!selectedUsers.isEmpty()) {
            for (QBUser qbUser : selectedUsers) {
                opponentsIds.add(qbUser.getId());
            }
        }

        return opponentsIds;
    }
}
