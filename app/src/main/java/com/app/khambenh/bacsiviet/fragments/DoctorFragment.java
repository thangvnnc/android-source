package com.app.khambenh.bacsiviet.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.DoctorAdapter;
import com.app.khambenh.bacsiviet.adapters.EndlessRecyclerViewScrollListener;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.db.QbUsersDbManager;
import com.app.khambenh.bacsiviet.util.Doctor;
import com.app.khambenh.bacsiviet.util.Speciality;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;

public class DoctorFragment extends Fragment {
    private ArrayList<Doctor> currentOpponentsList;
    private ArrayList<Item> specialityItemArrayList;
    private ArrayList<Item> doctorsOfSpecItemArrayList;
    ProgressDialog progressDialog;
    private LinearLayout btn_all;
    private QbUsersDbManager dbManager;
    private boolean flag_loading = false;
    private DoctorAdapter doctorAdapter;
    private ListAdapter specialityAdapter;
    TwoWayView horizontal_layout;
    private RecyclerView opponentsListView;
    private int page = 1;
    private String keyword = null;
    private ArrayList<QBUser> arrSearch;
    private static DoctorFragment doctorFragment;
    private boolean isLoad = false;
    LinearLayoutManager mLayoutManager;
    EndlessRecyclerViewScrollListener endless;
    Speciality currenSpeciality;

    private boolean isSelect = true;

    //paging
    String next_page_url, prev_page_url;

    public static DoctorFragment getInstance() {
        if (doctorFragment == null) {
            doctorFragment = new DoctorFragment();
        }
        return doctorFragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isLoad && isVisibleToUser) {
            isLoad = true;
//            swipeRefreshLayout.setRefreshing(true);
            currentOpponentsList.clear();
            currenSpeciality = null;
            startLoadUsers(null, URLUtils.URL_GET_DOCTORS_BY_SPECIALITY);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.k_frm_specialist, container, false);
        arrSearch = new ArrayList<>();
        opponentsListView = rootView.findViewById(R.id.k_frm_specialist_rv_doctor_id);
        horizontal_layout = rootView.findViewById(R.id.k_frm_specialist_rv_specialist_id);
//        swipeRefreshLayout = rootView.findViewById(R.id.k_frm_specialist_refesh_id);
        btn_all = rootView.findViewById(R.id.k_frm_specialist_card_more_id);
//        swipeRefreshLayout.setColorSchemeResources(new int[]{R.color.black, R.color.red, R.color.orange});
//        swipeRefreshLayout.setOnRefreshListener(this);
        dbManager = QbUsersDbManager.getInstance(getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity());
        progressDialog = new ProgressDialog(getContext());
        //
//        swipeRefreshLayout.setRefreshing(true);
        specialityItemArrayList = new ArrayList<>();
        doctorsOfSpecItemArrayList = new ArrayList<>();
        currentOpponentsList = new ArrayList<>();
        specialityAdapter = new ListAdapter(getContext(), specialityItemArrayList);
        horizontal_layout.setAdapter(specialityAdapter);

        initListAdapter();

        initSpecialityAdapter();
        startLoadSpeciality();
        currentOpponentsList.clear();
        currenSpeciality = null;
        startLoadUsers(null, URLUtils.URL_GET_DOCTORS_BY_SPECIALITY);

                btn_all.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                endless.resetState();
                if (!isSelect) {
                    isSelect = true;
                    // btn_all.setBackgroundResource(R.drawable.shape_button_all);
                }
                currentOpponentsList.clear();
                currenSpeciality = null;
                startLoadUsers(null, URLUtils.URL_GET_DOCTORS_BY_SPECIALITY);
            }
        });

//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
        //
//        opponentsListView.setOnScrollListener(new OnScrollListener() {
//            public void onScrollStateChanged(AbsListView absListView, int i) {
//            }
//
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0 && !flag_loading) {
//                    flag_loading = true;
//                    page = page + 1;
//                    Log.d("test_more", "sss");
//
//                }
//            }
//        });

//        opponentsListView.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if () {
//                    if (!next_page_url.equalsIgnoreCase("null")) {
//                        if (!flag_loading) {
//                            flag_loading = true;
//                            startLoadUsers(currenSpeciality, next_page_url);
//                        }
//                    }
//                }
//            }
//        });

        return rootView;
    }

    private void initListAdapter(){
        opponentsListView.setLayoutManager(mLayoutManager);
        doctorAdapter = new DoctorAdapter(getActivity(), currentOpponentsList);
        opponentsListView.setAdapter(doctorAdapter);
        endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (!next_page_url.equalsIgnoreCase("null")) {
                    if (!flag_loading) {
                        flag_loading = true;
                        startLoadUsers(currenSpeciality, next_page_url);
                    }
                }
            }
        };
        opponentsListView.setOnScrollListener(endless);
    }

    private void initSpecialityAdapter() {
        horizontal_layout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                isSelect = false;
//                btn_all.setBackgroundResource(R.drawable.shape_button_all_unselect);
                Speciality speciality = (Speciality) specialityItemArrayList.get(position);
                currenSpeciality = speciality;
                currentOpponentsList.clear();
                doctorAdapter.notifyDataSetChanged();
                endless.resetState();
                startLoadUsers(speciality, URLUtils.URL_GET_DOCTORS_BY_SPECIALITY);
            }
        });
    }

    private void startLoadSpeciality() {
        specialityItemArrayList.clear();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST,
                URLUtils.URL_GET_SPECIALITIES, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            Speciality speciality = new Gson().fromJson(response.getJSONObject(i).toString(), Speciality.class);
                            specialityItemArrayList.add(speciality);
                        }

                        specialityAdapter = new ListAdapter(getContext(), specialityItemArrayList);
                        horizontal_layout.setAdapter(specialityAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.showToast(getActivity(), error.getMessage());
            }
        });
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
    }

    private void startLoadUsers(Speciality speciality, String URL) {
        JSONObject postparams = new JSONObject();
        try {
            if (speciality != null) {
                postparams.put("speciality_id", speciality.getSpeciality_id());
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URL, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
//                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            int total = response.getInt("total");
                            int per_page = response.getInt("per_page");
                            int current_page = response.getInt("current_page");
                            int last_page = response.getInt("last_page");
                            next_page_url = response.getString("next_page_url");
                            prev_page_url = response.getString("prev_page_url");
                            JSONArray jsonArrayData = response.getJSONArray("data");
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                int curUserID = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);
                                final Doctor doctor = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), Doctor.class);
                                if (!String.valueOf(curUserID).equalsIgnoreCase(doctor.getUser_id())) {
                                    currentOpponentsList.add(doctor);
                                }
                            }
                            doctorAdapter.notifyDataSetChanged();
                            flag_loading = false;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(getActivity(), error.getMessage());
                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private void startLoadUsers(final int page) {
//        requestExecutor.loadUsersByTag("doctor", new QBEntityCallback<ArrayList<QBUser>>() {
//            public void onSuccess(ArrayList<QBUser> result, Bundle params) {


//                swipeRefreshLayout.setRefreshing(false);
//                if (page == 1) {
//                    dbManager.saveAllUsers(result, true);
//                    currentOpponentsList = dbManager.getAllUsers();
//                    currentOpponentsList.remove(sharedPrefsHelper.getQbUser());
//
//                    opponentsListView.setLayoutManager(mLayoutManager);
//                    opponentsAdapter = new OpponentsAdapter(getActivity(), currentOpponentsList);
//                    //
//                    Collections.sort(currentOpponentsList, new Comparator<QBUser>() {
//
//                        @Override
//                        public int compare(QBUser u1, QBUser u2) {
//                            return u2.getId().compareTo(u1.getId());
//                        }
//                    });
//
//                    //
//                    opponentsListView.setAdapter(opponentsAdapter);
//                } else {
//                    dbManager.saveAllUsers(result, false);
//                    currentOpponentsList.addAll(result);
//                    currentOpponentsList.remove(sharedPrefsHelper.getQbUser());
////                    Collections.sort(currentOpponentsList, new Comparator<QBUser>() {
////
////                        @Override
////                        public int compare(QBUser u1, QBUser u2) {
////                            return u2.getId().compareTo(u1.getId());
////                        }
////                    });
//                    opponentsAdapter.notifyDataSetChanged();
//                }
//                initUsersList();
//                flag_loading = false;
//            }
//
//            public void onError(QBResponseException responseException) {
//                if (responseException.getHttpStatusCode() == HttpStatus.SC_NOT_FOUND) {
//                    flag_loading = true;
//                }
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        }, page);
//    }

//    private void initUsersList() {
//        if (currentOpponentsList != null) {
//            ArrayList<QBUser> actualCurrentOpponentsList = dbManager.getAllUsers();
//            actualCurrentOpponentsList.remove(sharedPrefsHelper.getQbUser());
//            if (!isCurrentOpponentsListActual(actualCurrentOpponentsList)) {
//            }
//        }
//    }

//    private boolean isCurrentOpponentsListActual(ArrayList<QBUser> actualCurrentOpponentsList) {
//        return (actualCurrentOpponentsList.retainAll(currentOpponentsList) || currentOpponentsList.retainAll(actualCurrentOpponentsList)) ? false : true;
//    }


    public void onRefresh() {
        endless.resetState();
        page = 1;
        flag_loading = false;
        currentOpponentsList.clear();
        startLoadUsers(currenSpeciality, URLUtils.URL_GET_DOCTORS_BY_SPECIALITY);
    }

//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(MessageEvent event) {/* Do something */
//        if (event != null) {
//            keyword = event.getKeyword();
//            if (getUserVisibleHint() && keyword != null) {
//                String searchkey = CommonUtils.removeAccent(keyword);//add by Tuan
//                handSearch(searchkey.toLowerCase());
//            }
//
//        }
//    }

//    private void handSearch(String keyword) {
//        arrSearch = new ArrayList<>();
//        if (currentOpponentsList != null && currentOpponentsList.size() > 0) {
//            for (int i = 0; i < currentOpponentsList.size(); i++) {
//                String custom_data = CommonUtils.removeAccent(currentOpponentsList.get(i).getCustomData()).toLowerCase(); //update by Tuan - 30/10/2018
//                if (custom_data != null && custom_data.contains("-")) {
//                    String split[] = custom_data.split("-");
//                    if (split[0].contains(keyword)) {
//                        if (!arrSearch.contains(currentOpponentsList.get(i))) {
//                            arrSearch.add(currentOpponentsList.get(i));
//                        }
//
//                    }
//                } else {
//                    if (custom_data.contains(keyword)) {
//                        if (!arrSearch.contains(currentOpponentsList.get(i))) {
//                            arrSearch.add(currentOpponentsList.get(i));
//                        }
//                    }
//                }
//            }
//        }
//        if (opponentsAdapter != null) {
//            opponentsAdapter.swapData(arrSearch);
//        }
//    }

}
