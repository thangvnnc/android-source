package com.app.khambenh.bacsiviet.db;

import com.app.khambenh.bacsiviet.utils.Consts;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class FireStorageFuntion {


    static FirebaseStorage storage = FirebaseStorage.getInstance(Consts.MyCustomBucket);
    static StorageReference storageRef = storage.getReference();

    public static StorageReference getStorageReference(String parent,String path){
        StorageReference StorageReference = storageRef.child(parent + path);
        return  StorageReference;
    }

    public static FirebaseStorage getStorage() {
        return storage;
    }

    public void setStorage(FirebaseStorage storage) {
        this.storage = storage;
    }
}
