package com.app.khambenh.bacsiviet.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.fragments.QuestionFragment;
import com.app.khambenh.bacsiviet.util.HttpUtils;
import com.app.khambenh.bacsiviet.util.MResults;
import com.app.khambenh.bacsiviet.util.MSpeciality;
import com.app.khambenh.bacsiviet.util.PathUtil;
import com.app.khambenh.bacsiviet.utils.QBChatService;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joooonho.SelectableRoundedImageView;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddQuestionActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText _edtShortQuestion = null;
    private EditText _edtDetaitsQuestion = null;
    // private EditText _edtName = null;
    private Spinner _spinner = null;
    private Button _btnChooseImage = null;
    private Button _btnCommit = null;
    private Bitmap _bitmap = null;
    private String _filePath = null;
    private ImageView _imageView = null;
    private ArrayAdapter<String> _spinnerAdapter = null;
    private List<MSpeciality> _specialitys = null;
    private ArrayList<String> _listSpinner = null;
    private RequestQueue _rQueue;

    private Handler mHandler;
    private ProgressDialog progressDialog;
    private static final int PICK_IMAGE = 1;
    private static final String STRING_EMPTY = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.k_activity_add_question);
        init();
        getData();
    }

    /**
     * Ánh xạ và khởi tạo
     */
    private void init()
    {
        _edtShortQuestion = findViewById(R.id.k_activity_add_question_edt_short_title_id);
        _edtDetaitsQuestion = findViewById(R.id.k_activity_add_question_edt_content_id);
        // _edtName = findViewById(R.id.activity_add_question_edt_content_name_id);
        _spinner = findViewById(R.id.k_activity_add_question_spinner_specialist_id);
        _btnChooseImage = findViewById(R.id.k_activity_add_question_btn_choose_img_id);
        _btnCommit = findViewById(R.id.k_activity_add_question_btn_finish_id);
        _imageView = findViewById(R.id.k_activity_add_question_img_content_id);

        _btnCommit.setOnClickListener(this);
        _btnChooseImage.setOnClickListener(this);

        _listSpinner = new ArrayList<>();
    }

    @Override
    public void onClick(View view)
    {
        int id = view.getId();
        switch (id)
        {
            case R.id.k_activity_add_question_btn_choose_img_id:
                chooseImage();
                break;

            case R.id.k_activity_add_question_btn_finish_id:
                showProgressDialog(R.string.dlg_add_question);
                if (isHadInput() == false)
                {
                    hideProgressDialog();
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Vui lòng nhập đủ thông tin!").setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            dialog.dismiss();
                        }
                    });
                    builder.create().show();
                    return;
                }

                insertQuestion();
                break;
        }
    }

    void showProgressDialog(@StringRes int messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            // Disable the back button
            DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return keyCode == KeyEvent.KEYCODE_BACK;
                }
            };
            progressDialog.setOnKeyListener(keyListener);
        }

        progressDialog.setMessage(getString(messageId));

        progressDialog.show();

    }

    void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed()
    {
        setResult(QuestionFragment.RESULT_QA);
        super.onBackPressed();
    }

    private boolean isHadInput()
    {
        boolean ret = true;
        if (_edtShortQuestion.getText().toString().trim().equals(STRING_EMPTY))
        {
            ret = false;
        }
        else if (_edtDetaitsQuestion.getText().toString().trim().equals(STRING_EMPTY))
        {
            ret = false;
        }
//        else if (_edtName.getText().toString().trim().equals(STRING_EMPTY))
//        {
//            ret = false;
//        }
        else if (_filePath == null)
        {
            ret = false;
        }

        return ret;
    }

    private void chooseImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void getData()
    {
        HttpUtils httpUtils = new HttpUtils(this, new HttpUtils.VolleyCallback()
        {
            @Override
            public void onSuccess(String result)
            {

                if (result == null || result.equals(""))
                {
                    return;
                }
                Gson gson = new Gson();
                _specialitys = gson.fromJson(result, new TypeToken<List<MSpeciality>>()
                {
                }.getType());

                if (_specialitys == null || _specialitys.size() == 0)
                {
                    return;
                }

                // Set spinner
                for (MSpeciality mSpeciality : _specialitys)
                {
                    _listSpinner.add(mSpeciality.getSpecialityName());
                }
                _spinnerAdapter = new ArrayAdapter<>(AddQuestionActivity.this, android.R.layout.simple_spinner_item, _listSpinner);
                _spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                _spinner.setAdapter(_spinnerAdapter);
            }
        });
        httpUtils.call(URLUtils.URL_GET_SPECIALITIES);
    }

    private void insertQuestion()
    {
        String title = _edtShortQuestion.getText().toString().trim();
        String content = _edtDetaitsQuestion.getText().toString().trim();
        // String name = _edtName.getText().toString().trim();
        QBUser qbUser = QBChatService.getInstance().getUser();
        Integer userId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);

        int pos = _spinner.getSelectedItemPosition();
        if (pos < 0)
        {
            hideProgressDialog();
            return;
        }

        Integer specialityId = _specialitys.get(pos).getSpecialityId();

        final Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(userId));
        params.put("fullname", qbUser.getFullName());
        params.put("question_title", title);
        params.put("question_content", content);
        params.put("speciality_id", String.valueOf(specialityId));

        HttpUtils httpUtils = new HttpUtils(this, new HttpUtils.VolleyCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                hideProgressDialog();
                if (result == null || result.equals(""))
                {
                    return;
                }
                Gson gson = new Gson();
                MResults results = gson.fromJson(result, new TypeToken<MResults>(){}.getType());

                if (results == null || results.getCode() != 0)
                {
                    Toast.makeText(AddQuestionActivity.this, results.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                postImage(results.getContent());
            }
        });
        httpUtils.call(URLUtils.URL_INSERT_QUESTION, params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE)
        {
            if (data != null)
            {
                Uri selectedImage = data.getData();
                if (selectedImage != null)
                {
                    try
                    {
                        _filePath = PathUtil.getPath(AddQuestionActivity.this, selectedImage);
                    }
                    catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }
                    Glide.with(this).load(selectedImage).thumbnail(0.5f).error(R.drawable.ic_image_error).centerCrop().into(_imageView);
                    _imageView.setVisibility(View.VISIBLE);

                    try
                    {
                        InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        _bitmap = BitmapFactory.decodeStream(imageStream);
                    }
                    catch (FileNotFoundException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void postImage(String id)
    {
        mHandler = new Handler(Looper.getMainLooper());
        MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("img",
                        id,
                        RequestBody.create(MEDIA_TYPE_PNG, new File(_filePath)))
                .build();

        final com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder()
                .url(URLUtils.URL_UPLOAD_IMAGE)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final com.squareup.okhttp.Request request, final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AddQuestionActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                final String json = response.body().string();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (json.trim().equals(""))
                        {
                            Toast.makeText(AddQuestionActivity.this, "Hình quá lớn vui lòng chọn kích thước nhỏ hơn", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        MResults results = null;
                        try
                        {
                            Gson gson = new Gson();
                            results = gson.fromJson(json, new TypeToken<MResults>(){}.getType());
                        }
                        catch (Exception ex)
                        {
                            Toast.makeText(AddQuestionActivity.this, "Thất bại", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (results == null || results.getCode() != 0)
                        {
                            Toast.makeText(AddQuestionActivity.this, "Thất bại", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        setResult(QuestionFragment.RESULT_QA);
                        finish();
                    }
                });
            }
        });
    }
}
