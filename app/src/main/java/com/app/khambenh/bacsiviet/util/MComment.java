package com.app.khambenh.bacsiviet.util;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MComment
{
    @SerializedName("answer_id")
    @Expose
    private Long answerId;
    @SerializedName("question_id")
    @Expose
    private String questionId;
    @SerializedName("answer_user_id")
    @Expose
    private String answerUserId;
    @SerializedName("answer_content")
    @Expose
    private String answerContent;
    @SerializedName("created_at")
    @Expose
    private String createAt;

    public String getCreateAt()
    {
        return createAt;
    }

    public void setCreateAt(String createAt)
    {
        this.createAt = createAt;
    }

    public String getFullname()
    {
        return fullname;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    @SerializedName("fullname")
    @Expose
    private String fullname;

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAnswerUserId() {
        return answerUserId;
    }

    public void setAnswerUserId(String answerUserId) {
        this.answerUserId = answerUserId;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }
}

