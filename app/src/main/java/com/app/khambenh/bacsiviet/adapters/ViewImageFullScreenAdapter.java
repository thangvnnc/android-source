package com.app.khambenh.bacsiviet.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.app.khambenh.bacsiviet.Event.TouchImageView;
import com.app.khambenh.bacsiviet.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewImageFullScreenAdapter  extends PagerAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<String> itemArrayList;

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public ViewImageFullScreenAdapter(Activity activity,
                                      ArrayList<String> itemArrayList) {
        this.activity = activity;
        this.itemArrayList = itemArrayList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_view_image_full_screen, container,
                false);
        TouchImageView imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);

        Glide.with(activity).load(itemArrayList.get(position)).into(imgDisplay);

        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
