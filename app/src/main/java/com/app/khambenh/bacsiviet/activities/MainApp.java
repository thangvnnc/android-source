package com.app.khambenh.bacsiviet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.app.khambenh.bacsiviet.R;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by os-nguyenxuanduc on 4/2/2018.
 */

public class MainApp extends BaseActivity {
    private ViewPager main_pager;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    //    private String[] mTitles = {"Trò chuyện", "Bác sĩ", "Người dùng", "Khuyến mại"};
    List<String> mTitles; //add by Tuan
    private CommonTabLayout mTabLayout_2;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private int[] mIconSelectIds = {
            R.mipmap.ic_notification, R.mipmap.ic_notification,
            R.mipmap.ic_notification, R.mipmap.ic_notification};
    private int[] mIconUnselectIds = {
            R.mipmap.ic_notification, R.mipmap.ic_notification,
            R.mipmap.ic_notification, R.mipmap.ic_notification};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        initUI();
        //

    }

    private void initUI() {
        main_pager = findViewById(R.id.main_pager);
        mTabLayout_2 = findViewById(R.id.tl_2);
        for (int i = 0; i < mTitles.size(); i++) {
            mTabEntities.add(new TabEntity(mTitles.get(i), mIconSelectIds[i], mIconUnselectIds[i]));
        }
        tl_2();
        for (String title : mTitles) {
            mFragments.add(SimpleCardFragment.getInstance(title));
        }
        main_pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mTabLayout_2.setMsgMargin(0, -5, 5);
        main_pager.setCurrentItem(0);
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void tl_2() {
        mTabLayout_2.setTabData(mTabEntities);
        mTabLayout_2.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                main_pager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
                if (position == 0) {

//                    UnreadMsgUtils.show(mTabLayout_2.getMsgView(0), mRandom.nextInt(100) + 1);
                }
            }
        });

        main_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout_2.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        main_pager.setCurrentItem(1);
    }

    // Custom Adapter
    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
