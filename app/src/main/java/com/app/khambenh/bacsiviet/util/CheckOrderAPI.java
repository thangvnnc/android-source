package com.app.khambenh.bacsiviet.util;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by binh on 8/19/2018.
 * call api check order status
 */

public class CheckOrderAPI extends AsyncTask<CheckOrder, Void, CheckOrder.CheckOrderResult> {
    private APICall<CheckOrder.CheckOrderResult> apiCall;

    public CheckOrderAPI(APICall<CheckOrder.CheckOrderResult> apiCall) {
        this.apiCall = apiCall;
    }

    @Override
    protected CheckOrder.CheckOrderResult doInBackground(CheckOrder... checkOrders) {
        CheckOrder.CheckOrderResult checkOrderResult = null;
        HttpURLConnection connection = null;
        try {
            CheckOrder checkOrder = checkOrders[0];
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("func", checkOrder.getFunc())
                    .appendQueryParameter("version", checkOrder.getVersion())
                    .appendQueryParameter("merchant_id", checkOrder.getMerchantId())
                    .appendQueryParameter("token_code", checkOrder.getTokenCode())
                    .appendQueryParameter("checksum", checkOrder.getChecksum());

            String query = builder.build().getEncodedQuery();
            URL myUrl = new URL(CommonUtils.MAIN_URL + "?" + query);

            connection = (HttpURLConnection)
                    myUrl.openConnection();

            connection.setConnectTimeout(CommonUtils.CONNECTION_TIMEOUT);
            connection.setRequestMethod("POST");
            connection.setReadTimeout(CommonUtils.READ_TIMEOUT);
            connection.setDoInput(true);
            connection.setDoOutput(true);


            connection.connect();
            Log.d(SendOrderAPI.class.getSimpleName(), connection.getURL().toString());
            int resCode = connection.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                checkOrderResult = new Gson().fromJson(reader,  CheckOrder.CheckOrderResult.class);

                streamReader.close();
                reader.close();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (connection != null)
                connection.disconnect();
        }

        return checkOrderResult;
    }

    @Override
    protected void onPostExecute(CheckOrder.CheckOrderResult checkOrderResult) {
        if (apiCall != null)
            apiCall.onFinish(checkOrderResult);
    }
}
