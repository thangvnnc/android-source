package com.app.khambenh.bacsiviet.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.District;
import com.app.khambenh.bacsiviet.util.Province;
import com.app.khambenh.bacsiviet.util.UserDetail;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.LanguageUtils;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PICK_IMAGE_CODE = 111;
    private static final int PERMISSION_READ_STORAGE = 4;
    private EditText etName, etPhone, etBirthDate, etAddress;
    private CircleImageView imageView;
    TextView tvDistrict, tvProvince;
    private Button btnChooseImage, btnUpdate;
    private RadioButton radioMan, radioWoman, radioOther;

    private AlertDialog provinceDialog, districtDialog;
    private ArrayList<Province> provinces;
    private ArrayList<District> districts;
    private Province selectedProvince;
    private District selectedDistrict;
    private AlertDialog alertDialog;
    private File savedCaptureFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageUtils.loadLocale(this);
        setContentView(R.layout.activity_update);
        etName = findViewById(R.id.etName);
        etPhone = findViewById(R.id.etPhone);
        etBirthDate = findViewById(R.id.etBirthDate);
//        etAddress = findViewById(R.id.etAddress);
//        tvDistrict = findViewById(R.id.tvDistrict);
//        tvProvince = findViewById(R.id.tvProvince);
        imageView = findViewById(R.id.imageUpdate);
        btnChooseImage = findViewById(R.id.btnChooseImage);
        btnUpdate = findViewById(R.id.btnUpdate);

        radioMan = findViewById(R.id.radioMan);
        radioWoman = findViewById(R.id.radioWoman);
        radioOther = findViewById(R.id.radioOther);

        String avatar = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_AVATAR, String.class);
        if (!avatar.equals("")) {
            Glide.with(this).load(avatar).into(imageView);
        } else {
            Glide.with(this).load(R.drawable.ic_other_user).into(imageView);
        }

//        etName.setText(SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_FULLNAME, String.class));


        btnUpdate.setOnClickListener(this);
        btnChooseImage.setOnClickListener(this);
        etBirthDate.setOnClickListener(this);
//        tvDistrict.setOnClickListener(this);
//        tvProvince.setOnClickListener(this);

//        provinces = new ArrayList<>();
//        districts = new ArrayList<>();
        loadData();
    }

    private void loadData() {
        displayUserDetail();
//        getProvinces();
    }

//    private void getUserDetail() {
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
//                CommonUtils.BASE_URL + "user/" + pre.getString("user_id", "-1") +"/detail", null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            userDetail = new Gson().fromJson(response.toString(), UserDetail.class);
//                            if (userDetail != null)
//                                displayUserDetail(userDetail);
//                        } catch (Exception e) {
//                            //
//                        }
//                    }
//
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });
//        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "getRequest");
//    }

    private void displayUserDetail() {
        UserDetail userDetail = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_DETAIL, UserDetail.class);

        etName.setText(SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_FULLNAME, String.class));
        etBirthDate.setText(CommonUtils.parseDatetime(CommonUtils.DatePattern.SQL_DATE, CommonUtils.DatePattern.DISPLAY_DATE, userDetail.getBirthDate()));
        etPhone.setText(userDetail.getPhone() != null ? "0" + userDetail.getPhone() : "");
//        etAddress.setText(userDetail.getAddress() != null ? userDetail.getAddress() : "");
//        tvProvince.setText(userDetail.getProvinceName() != null ? userDetail.getProvinceName() : "");
//        tvDistrict.setText(userDetail.getDistrictName() != null ? userDetail.getDistrictName() : "");
        if (userDetail.getGender() == Consts.TYPE_MAN) {
            radioMan.setChecked(true);
        } else if (userDetail.getGender() == Consts.TYPE_WOMEN) {
            radioWoman.setChecked(true);
        } else {
            radioOther.setChecked(true);
        }
    }

    public void callUpdateAPI(UserDetail userDetail) {

        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(userDetail));
            alertDialog = new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.notifi_get_county_district))
                    .create();
            alertDialog.show();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_UPDATE_USER, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            alertDialog.dismiss();
                            CommonUtils.showToast(UpdateActivity.this, getString(R.string.notifi_update_successful));
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertDialog.dismiss();
                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    if (response.statusCode == 200)
                        return Response.success(new JSONObject(),
                                HttpHeaderParser.parseCacheHeaders(response));
                    else
                        return Response.error(new VolleyError(response));
                }
            };
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getProvinces() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                CommonUtils.BASE_URL + "province", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            ArrayList<Province> results =
                                    new Gson().fromJson(response.toString(), new TypeToken<ArrayList<Province>>() {
                                    }.getType());
                            if (results != null) {
                                provinces.clear();
                                provinces.addAll(results);

                            }
                        } catch (Exception e) {
                            //
                            Log.d("ERROR:", e.getMessage());
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        CoreApp.getInstance().addToRequestQueue(jsonArrayRequest, "getRequest");
    }

    public void getDistrict(int provinceId) {
        alertDialog = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.notifi_get_county_district))
                .create();
        alertDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                CommonUtils.BASE_URL + "district?provinceId=" + provinceId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        alertDialog.dismiss();
                        try {
                            ArrayList<District> results =
                                    new Gson().fromJson(response.toString(), new TypeToken<ArrayList<District>>() {
                                    }.getType());
                            if (results != null) {
                                districts.clear();
                                districts.addAll(results);

                            }
                        } catch (Exception e) {
                            //
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.dismiss();
            }
        });

        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "getRequest");
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnUpdate:
                update();
                break;

            case R.id.btnChooseImage:
//                chooseImage();
                inputPicture();
                break;
//            case R.id.tvDistrict:
//                chooseDistrict();
//                break;
//            case R.id.tvProvince:
//                chooseProvince();
//                break;
            case R.id.etBirthDate:
                chooseBirthDate();
                break;
        }
    }

    private void chooseBirthDate() {
        CommonUtils.showDatePickerDialog(this, etBirthDate);
    }

    private void chooseProvince() {
        if (provinces.isEmpty()) {
            CommonUtils.showToast(this, getString(R.string.notifi_not_found_province_cyties));
            return;
        }
        provinceDialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_select_province_city))
                .setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, provinces),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    ListView lv = ((AlertDialog) dialog).getListView();
                                    Province province = (Province) lv.getSelectedItem();
                                    if (province != null) {
                                        tvProvince.setText(province.getName());
                                        selectedProvince = province;
                                        getDistrict(province.getId());
                                    }
                                } catch (ClassCastException e) {
                                    //
                                }
                            }
                        })
                .create();
        provinceDialog.show();

    }

    private void chooseDistrict() {
        if (selectedProvince == null) {
            CommonUtils.showToast(this, getString(R.string.error_selecte_province_city_first));
            return;
        }
        if (districts.isEmpty()) {
            CommonUtils.showToast(this, getString(R.string.error_not_found_list_county_district));
            return;
        }
        districtDialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_select_county_district))
                .setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, provinces),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    ListView lv = ((AlertDialog) dialog).getListView();
                                    if (selectedDistrict != null) {
                                        selectedDistrict = (District) lv.getSelectedItem();
                                        tvDistrict.setText(selectedDistrict.getName());
                                    }

                                } catch (ClassCastException e) {
                                    //
                                }
                            }
                        })
                .create();
        districtDialog.show();
    }

    private void update() {
        UserDetail userDetail = new UserDetail();
        if (!validateFrom() || userDetail == null)
            return;
        userDetail.setFullname(etName.getText().toString());

        RadioGroup radioGroup = findViewById(R.id.radioGender);
        int radioButtonId = radioGroup.getCheckedRadioButtonId();
        if (radioButtonId == R.id.radioMan)
            userDetail.setGender(1);
        else if (radioButtonId == R.id.radioWoman)
            userDetail.setGender(2);
        else {
            userDetail.setGender(3);
        }

        userDetail.setBirthDate(CommonUtils.parseDatetime(
                CommonUtils.DatePattern.DISPLAY_DATE,
                CommonUtils.DatePattern.SQL_DATE,
                etBirthDate.getText().toString()));

        userDetail.setPhone(etPhone.getText().toString());
//        userDetail.setAddress(etAddress.getText().toString());

        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_USER_DETAIL, userDetail);
//        if (selectedProvince != null)
//            userDetail.setProvinceId(selectedProvince.getId());
//        if (selectedDistrict != null)
//            userDetail.setDistrictId(selectedDistrict.getId());

        callUpdateAPI(userDetail);
    }

    private boolean validateFrom() {
        if (TextUtils.isEmpty(etName.getText())) {
            etName.requestFocus();
            etName.setError(getString(R.string.error_empty_fullname));
            return false;
        } else if (TextUtils.isEmpty(etPhone.getText())) {
            etPhone.requestFocus();
            etPhone.setError(getString(R.string.error_empty_phone));
            return false;
        }
        return true;
    }

    private void chooseImage() {
        if (ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,
                    new String[]{READ_EXTERNAL_STORAGE}, PERMISSION_READ_STORAGE);
        else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 96);
            intent.putExtra("outputY", 96);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.title_select_image)), PICK_IMAGE_CODE);
        }
    }

    private void inputPicture() {
        final CharSequence[] items = {getString(R.string.capture), getString(R.string.choose_from_gallery), getString(R.string.cancel)};
        final AlertDialog.Builder builder = new AlertDialog.Builder(UpdateActivity.this);
        builder.setTitle(getString(R.string.select));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                //take camera
                if (items[item].equals(getString(R.string.capture))) {
                    savedCaptureFile = new File(CommonUtils.getFolderApp(Consts.APP_NAME), Consts.FILE_TMP_NAME);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(savedCaptureFile));
                    startActivityForResult(takePictureIntent, Consts.REQUEST_CAMERA);
                } else if (items[item].equals(getString(R.string.choose_from_gallery))) {//choose from lib
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, Consts.SELECT_PICTURE);
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == PICK_IMAGE_CODE) {
//            if (resultCode == Activity.RESULT_OK)
//                handleBitmap(data.getData());
//
//        }
        if (requestCode == Consts.REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
//                Bitmap img = null;
//                try {
//                    img = CommonUtils.decodeFile(savedCaptureFile, widthOfDevice);
//                } catch (Throwable throwable) {
//                    throwable.printStackTrace();
//                }
                Glide.with(this).load(savedCaptureFile).into(imageView);
            }
        } else if (requestCode == Consts.SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                savedCaptureFile = new File(getPath(uri));
//                Bitmap img = null;
//                try {
//                    img = CommonUtils.decodeFile(savedCaptureFile, widthOfDevice);
//                } catch (Throwable throwable) {
//                    throwable.printStackTrace();
//                }
                Glide.with(this).load(savedCaptureFile).into(imageView);
            }
        }
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void handleBitmap(Uri uri) {
        Glide.with(getApplicationContext())
                .load(uri)
                .asBitmap()
                .into(imageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (provinceDialog != null && provinceDialog.isShowing())
            provinceDialog.dismiss();

        if (districtDialog != null && districtDialog.isShowing())
            districtDialog.dismiss();
    }
}
