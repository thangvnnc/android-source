package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Item;

public class Language implements Item {
    private int id;
    private String language;
    private String code;
    private int icon;

    public Language(int id, String language, String code, int icon) {
        this.id = id;
        this.language = language;
        this.code = code;
        this.icon = icon;
    }

    public Language() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public View getView(Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_language, null);
        } else {
            rowView = convertView;
        }
        ImageView imgLanguage = (ImageView) rowView.findViewById(R.id.imgLanguage);
        TextView txtLanguage = (TextView) rowView.findViewById(R.id.txtLanguage);
        imgLanguage.setImageResource(getIcon());
        txtLanguage.setText(getLanguage());
        return rowView;
    }
}
