package com.app.khambenh.bacsiviet.services.gcm;

import android.os.Bundle;
import android.util.Log;

import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.constant.GcmConsts;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by tereha on 13.05.16.
 */
public class GcmPushListenerService extends GcmListenerService {
    private static final String TAG = GcmPushListenerService.class.getSimpleName();

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.v("CCCCCCCCCCCCCCC","");

        String message = data.getString(GcmConsts.EXTRA_GCM_MESSAGE);
        Log.e(TAG, "From: " + from);
        Log.e(TAG, "Message: " + message);

        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        if (sharedPrefsHelper.hasQbUser()) {
            Log.e(TAG, "CoreApp have logined user");
            QBUser qbUser = sharedPrefsHelper.getQbUser();
            startLoginService(qbUser);
        }
    }

    private void startLoginService(QBUser qbUser){
//        CallService.start(this, qbUser);
    }
}