package com.app.khambenh.bacsiviet.services.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.MessageFirebaseActivity;
import com.app.khambenh.bacsiviet.services.bubble.BubbleService;
import com.app.khambenh.bacsiviet.stringee.service.StringeeService;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
//        BubbleService.show(getApplicationContext());

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData().get("senderId"));

        //Calling method to generate notification

        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(CoreApp.getAppContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String fromId = remoteMessage.getData().get("senderId");
        if (fromId != null) {
            SharedPrefs.getInstance().put(Consts.EXTRA_MESSAGE_FROM_USER_ID, fromId);
        }

        if (StringeeService.isRunning(this)) {
            Integer idUser = SharedPrefsHelper.getInstance().getQbUser().getId();
            if (idUser != null)
                StringeeService.getInstance().connect(idUser + "");
        }
//        Log.e(TAG, "" + remoteMessage.getData());
//        String s = String.format(String.valueOf(R.string.text_push_notification_message));
//        if (s.equalsIgnoreCase(remoteMessage.getData().get("message")) || remoteMessage.getData().containsKey("VOIPCall")) {
//            Log.e("Step1:", "" + StaticCommon.IsStartCallActivity);
//            if (!StaticCommon.IsStartCallActivity) {
//                Log.e("Step2:", "" + StaticCommon.IsStartCallActivity);
//                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(getPackageName());
//                if (launchIntent != null) {
//                    startActivity(launchIntent);//null pointer check in case package name was not found
//                    Intent intent = new Intent(this, CallActivity.class);
//                    intent.putExtra(Consts.EXTRA_IS_INCOMING_CALL, true);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            }
//        } else {
//             sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
//        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MessageFirebaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notifi_message)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }
}
