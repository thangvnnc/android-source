package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Item;
import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import de.hdodenhof.circleimageview.CircleImageView;

public class Clinic implements Serializable, Item {

    @SerializedName("clinic_id")
    private int clinic_id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("clinic_name")
    private String clinic_name;

    @SerializedName("profile_image")
    private String profile_image;

    @SerializedName("clinic_url")
    private String clinic_url;

    @SerializedName("featured")
    private int  featured;

    @SerializedName("clinic_desc")
    private String clinic_desc;

    @SerializedName("clinic_address")
    private String clinic_address;

    @SerializedName("clinic_time")
    private String clinic_time;

    @SerializedName("clinic_phone")
    private String clinic_phone;

    @SerializedName("facilities")
    private String facilities;

    @SerializedName("province_id")
    private int province_id;

    @SerializedName("district_id")
    private String district_id;

    @SerializedName("clinic_timeopen")
    private String clinic_timeopen;

    @SerializedName("vote")
    private float vote;

    @SerializedName("speciality_id")
    private String speciality_id;

    String province_name;

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public float getVote() {
        return vote;
    }

    public void setVote(float vote) {
        this.vote = vote;
    }

    public String getSpeciality_id() {
        return speciality_id;
    }

    public void setSpeciality_id(String speciality_id) {
        this.speciality_id = speciality_id;
    }

    public int getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(int clinic_id) {
        this.clinic_id = clinic_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getClinic_url() {
        return clinic_url;
    }

    public void setClinic_url(String clinic_url) {
        this.clinic_url = clinic_url;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    public String getClinic_desc() {
        return clinic_desc;
    }

    public void setClinic_desc(String clinic_desc) {
        this.clinic_desc = clinic_desc;
    }

    public String getClinic_address() {
        return clinic_address;
    }

    public void setClinic_address(String clinic_address) {
        this.clinic_address = clinic_address;
    }

    public String getClinic_time() {
        return clinic_time;
    }

    public void setClinic_time(String clinic_time) {
        this.clinic_time = clinic_time;
    }

    public String getClinic_phone() {
        return clinic_phone;
    }

    public void setClinic_phone(String clinic_phone) {
        this.clinic_phone = clinic_phone;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getClinic_timeopen() {
        return clinic_timeopen;
    }

    public void setClinic_timeopen(String clinic_timeopen) {
        this.clinic_timeopen = clinic_timeopen;
    }

    private int isOnline; // 0- not login app, 1- login app

    public int getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(int isOnline) {
        this.isOnline = isOnline;
    }

    @Override
    public View getView(Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_speciality, null);
        } else {
            rowView = convertView;
        }
        CircleImageView imgAvatar = (CircleImageView)rowView.findViewById(R.id.imgAvatar);
        TextView txt_name = (TextView)rowView.findViewById(R.id.txt_name);
        ImageView img_status = (ImageView)rowView.findViewById(R.id.img_status);

        Glide.with(context).load(getProfile_image()).error(R.color.blue).into(imgAvatar);
        txt_name.setText(getClinic_name());

        if(getFeatured()==1){
            img_status.setVisibility(View.VISIBLE);
        }else {
            img_status.setVisibility(View.INVISIBLE);
        }
        return rowView;
    }
}
