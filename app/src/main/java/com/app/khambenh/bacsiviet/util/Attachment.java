package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class Attachment implements Item {

    private Uri uri;
    private boolean hasUpload;

    public Attachment(Uri uri) {
        this.uri = uri;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    @Override
    public View getView(final Context context, LayoutInflater layoutInflater, View convertView, final int position) {

        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_attachment_message_firebase, null);
        } else {
            rowView = convertView;
        }

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
        final ProgressBar progress_attachment_preview = (ProgressBar)rowView.findViewById(R.id.progress_attachment_preview);
        progress_attachment_preview.setVisibility(View.VISIBLE);
        Glide.with(context).load(getUri()).listener(new RequestListener<Uri, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress_attachment_preview.setVisibility(View.INVISIBLE);
                return false;
            }
        }).into(imageView);


        return rowView;
    }
}
