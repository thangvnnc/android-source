package com.app.khambenh.bacsiviet.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.KeyEvent;
import android.view.View;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;

import org.greenrobot.eventbus.EventBus;

/**
 * QuickBlox team
 */
public abstract class BaseActivity extends CoreBaseActivity {

    SharedPrefsHelper sharedPrefsHelper;
    private ProgressDialog progressDialog;
    public final static int APP_ACCOUNT_KIT_REQUEST_CODE = 99;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Consts.ACTION_UPDATE_BALANCE)){
                initDefaultActionBar();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        registerBroadcastFilter();
    }
    private void registerBroadcastFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Consts.ACTION_UPDATE_BALANCE);
        registerReceiver(broadcastReceiver, filter);
    }

    public void initDefaultActionBar() {
        String currentUserFullName = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_FULLNAME,String.class);
//        if (sharedPrefsHelper.getQbUser() != null) {
//            currentUserFullName = sharedPrefsHelper.getQbUser().getFullName();
//        }
        setActionBarTitle(getString(R.string.app_name));
//        setActionBarTitle(SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_FACEBOOK_ID,String.class));
//        setActionbarSubTitle(String.format(getString(R.string.subtitle_text_logged_in_as), currentUserFullName));
        String currenType = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_TYPE,String.class);
        long balance= SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_BALANCE,Long.class);
        if(currenType.equals("user")){
            setActionbarSubTitle(currentUserFullName + " @@@@ " + getString(R.string.balance) + ": "+ balance);
        }
       else {
            setActionbarSubTitle(currentUserFullName);
        }
       actionBar.hide();
    }


    public void setActionbarSubTitle(String subTitle) {
        if (actionBar != null)
            actionBar.setSubtitle(subTitle);
    }

    public void removeActionbarSubTitle() {
        if (actionBar != null)
            actionBar.setSubtitle(null);
    }

    void showProgressDialog(@StringRes int messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            // Disable the back button
            DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return keyCode == KeyEvent.KEYCODE_BACK;
                }
            };
            progressDialog.setOnKeyListener(keyListener);
        }

        progressDialog.setMessage(getString(messageId));

        progressDialog.show();

    }

    void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

//    protected void showErrorSnackbar(@StringRes int resId, Exception e,
//                                     View.OnClickListener clickListener) {
//        if (getSnackbarAnchorView() != null) {
//            ErrorUtils.showSnackbar(getSnackbarAnchorView(), resId, e,
//                    R.string.dlg_retry, clickListener);
//        }
//    }
//
//    protected void showSnackbar(@StringRes int resId,
//                                View.OnClickListener clickListener) {
//        if (getSnackbarAnchorView() != null) {
//            ErrorUtils.showSnackbar(getSnackbarAnchorView(), resId,
//                    R.string.recharge, clickListener);
//        }
//    }

    protected abstract View getSnackbarAnchorView();

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}




