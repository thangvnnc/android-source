package com.app.khambenh.bacsiviet.util;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by binh on 8/19/2018.
 * call api get checkout url
 */

public class SendOrderAPI extends AsyncTask<SendOrder, Void, SendOrder.SendOrderResult> {
    private APICall<SendOrder.SendOrderResult> apiCall;

    public SendOrderAPI(APICall<SendOrder.SendOrderResult> apiCall) {
        this.apiCall = apiCall;
    }

    @Override
    protected SendOrder.SendOrderResult doInBackground(SendOrder... sendOrders) {
        SendOrder.SendOrderResult sendOrderResult = null;
        HttpURLConnection connection = null;
        try {
            SendOrder order = sendOrders[0];
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("func", order.getFunc())
                    .appendQueryParameter("version", order.getVersion())
                    .appendQueryParameter("merchant_id", order.getMerchantId())
                    .appendQueryParameter("merchant_account", order.getMerchantAccount())
                    .appendQueryParameter("order_code", order.getOrderCode())
                    .appendQueryParameter("total_amount", String.valueOf(order.getTotalAmount()))
                    .appendQueryParameter("currency", order.getCurrency())
                    .appendQueryParameter("language", order.getLanguage())
                    .appendQueryParameter("return_url", order.getReturnUrl())
                    .appendQueryParameter("cancel_url", order.getCancelUrl())
                    .appendQueryParameter("notify_url", order.getNotifyUrl())
                    .appendQueryParameter("buyer_fullname", order.getBuyerFullName())
                    .appendQueryParameter("buyer_email", order.getBuyerEmail())
                    .appendQueryParameter("buyer_mobile", order.getBuyerMobile())
                    .appendQueryParameter("buyer_address", order.getBuyerAddress())
                    .appendQueryParameter("checksum", order.getChecksum());

            String query = builder.build().getEncodedQuery();
            URL myUrl = new URL(CommonUtils.MAIN_URL + "?" + query);

            connection = (HttpURLConnection)
                    myUrl.openConnection();

            connection.setConnectTimeout(CommonUtils.CONNECTION_TIMEOUT);
            connection.setRequestMethod("POST");
            connection.setReadTimeout(CommonUtils.READ_TIMEOUT);
            connection.setDoInput(true);
            connection.setDoOutput(true);


            connection.connect();
            Log.d(SendOrderAPI.class.getSimpleName(), connection.getURL().toString());
            int resCode = connection.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                sendOrderResult = new Gson().fromJson(reader, SendOrder.SendOrderResult.class);

                streamReader.close();
                reader.close();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (connection != null)
                connection.disconnect();
        }

        return sendOrderResult;
    }

    @Override
    protected void onPostExecute(SendOrder.SendOrderResult sendOrderResult) {
        if (apiCall != null)
            apiCall.onFinish(sendOrderResult);
    }
}
