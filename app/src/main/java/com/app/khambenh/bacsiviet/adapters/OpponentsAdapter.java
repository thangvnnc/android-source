package com.app.khambenh.bacsiviet.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * QuickBlox team
 */
public class OpponentsAdapter extends RecyclerView.Adapter<OpponentsAdapter.UserViewHolder> {
    private Activity mContext;
    private List<QBUser> arr_users;

    public OpponentsAdapter(Activity context, List<QBUser> users) {
        this.mContext = context;
        this.arr_users = users;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_opponents_list, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        String link_image = "";
        String link_profile = "";
        final QBUser user = arr_users.get(position);
        if (user != null && user.getLogin() != null) {
            if (user != null && user.getCustomData() != null) {
                if (user.getCustomData().contains("-")) {
                    String split[] = user.getCustomData().split("-");
                    holder.opponentName.setText(split[0]);
                    holder.txt_chuyennganh.setText(split[1]);
                    try {
                        if (split[1] != null && split[1].contains("!linkprofile!")) {
                            link_profile = split[1].split("!linkprofile!")[1];
                        }
                        if (user.getCustomData().contains("-_-")) {
                            link_image = user.getCustomData().split("-_-")[1];
                        }
                    } catch (Exception e) {
                        Log.d("test_link", "" + e.getMessage());
                    }
                } else {
                    holder.opponentName.setText(user.getCustomData());
                    link_image = user.getCustomData();
                }
            } else {
                holder.opponentName.setText(user.getCustomData());
            }

            if (!link_image.equals("")) {
                Glide.with(mContext).load("http://" + link_image).into(holder.opponentIcon);
            } else {
                holder.opponentIcon.setImageResource(R.drawable.ic_other_user);
            }
        }
        final String finalLink_profile = link_profile;
        holder.layout_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showAction(user, finalLink_profile);
                CallUltils.checkToCall(user,mContext,Consts.TYPE_CHAT);
            }
        });

    }

    private void showAction(final QBUser qbUser, final String linkweb) {
        Log.d("test_id", "" + qbUser.getId());
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setContentView(R.layout.layout_diaglog);
        TextView txt_docname = dialog.findViewById(R.id.txt_docname);
        TextView txt_mieuta = dialog.findViewById(R.id.txt_mieuta);
        if (qbUser != null && qbUser.getCustomData() != null) {
            if (qbUser.getCustomData().contains("-")) {
                String split[] = qbUser.getCustomData().split("-");
                txt_docname.setText(split[0]);
                txt_mieuta.setText(split[1]);
            } else {
                txt_docname.setText(qbUser.getFullName());
            }
        }
        LinearLayout lineVideo = dialog.findViewById(R.id.lnVideoCall);
        LinearLayout lineAudio = dialog.findViewById(R.id.lnAudioCall);
        LinearLayout lineChat = dialog.findViewById(R.id.lnChat);
        LinearLayout lineProfile = dialog.findViewById(R.id.lnProfile);
        if ("user".equalsIgnoreCase("" + qbUser.getTags().get(0).toString())) {
            lineProfile.setVisibility(View.GONE);
        } else {
            lineProfile.setVisibility(View.VISIBLE);
            if (qbUser.getTags().get(0).toString().equals("clinic")) {
                TextView txtProfile = dialog.findViewById(R.id.txtProfile);
                txtProfile.setText(mContext.getString(R.string.text_clinic_profile));
            }
        }
        lineVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                CallUltils.isFromChatActivity = false;
                CallUltils.checkToCall(qbUser, mContext, Consts.TYPE_CALL_VIDEO);
            }
        });
        lineAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                CallUltils.isFromChatActivity = false;
                CallUltils.checkToCall(qbUser, mContext, Consts.TYPE_CALL_AUDIO);

            }
        });
        lineChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CallUltils.checkToCall(qbUser, mContext, Consts.TYPE_CHAT);
                CallUltils.chat(qbUser, mContext);
                dialog.dismiss();
            }
        });
        lineProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qbUser.getWebsite() != null && !"".equals(qbUser.getWebsite())) {
                    loadProfileByLink(qbUser.getWebsite());
                } else {
//                    requestExecutor.getUserById(qbUser.getId()).performAsync(new QBEntityCallback<QBUser>() {
//                        @Override
//                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                            QBUser qbUser = null;
                            if (qbUser != null) {
                                if (qbUser.getWebsite() != null && !"".equals(qbUser.getWebsite())) {
                                    loadProfileByLink(qbUser.getWebsite());
                                } else {
                                    try {
                                        processLinkProfiles(qbUser.getExternalId());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
//                        }
//
//                        @Override
//                        public void onError(QBResponseException e) {
//
//                        }
//                    });
                }
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public int getItemCount() {
        return arr_users.size();
    }

    private void loadProfileByLink(String website) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(website));
            mContext.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Todo: Get link info
    private void processLinkProfiles(String id) throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage(mContext.getString(R.string.notifi_processing));
        progressDialog.show();
        JSONObject postparams = new JSONObject();
        postparams.put("id", "" + id);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URLUtils.URL_INFO_DOCTOR, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            try {
                                if (response.has("link") && !response.getString("link").equals("")) {

                                    try {
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse(response.getString("link")));
                                        mContext.startActivity(i);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse(URLUtils.URL_LIST_DOCTOR));
                                        mContext.startActivity(i);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (JSONException e) {
                                Log.d("test_error", "" + e.getMessage());
                            }
                        }
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        //Failure Callback

                    }
                });

        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        CircleImageView opponentIcon;
        ImageView img_status;
        TextView opponentName, txt_chuyennganh;
        LinearLayout layout_holder;

        public UserViewHolder(View view) {
            super(view);
            opponentIcon = view.findViewById(R.id.image_opponent_icon);
            opponentName = view.findViewById(R.id.opponentsName);
            img_status = view.findViewById(R.id.img_status);
            txt_chuyennganh = view.findViewById(R.id.txt_chuyennganh);
            layout_holder = view.findViewById(R.id.layout_holder);

        }
    }

    public void swapData(ArrayList<QBUser> data) {
        this.arr_users = data;
        this.notifyDataSetChanged();
    }

}
