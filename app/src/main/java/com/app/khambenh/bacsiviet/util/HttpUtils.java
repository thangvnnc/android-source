package com.app.khambenh.bacsiviet.util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class HttpUtils
{
    public interface VolleyCallback
    {
        void onSuccess(String result);
    }

    private Context context;
    private VolleyCallback callback;

    public HttpUtils(Context context, VolleyCallback callback)
    {
        this.context = context;
        this.callback = callback;
    }

    public void call(String url, final Map<String, String> params)
    {
        try
        {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        response = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                        callback.onSuccess(response);
                    }
                    catch (UnsupportedEncodingException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    callback.onSuccess(null);
                }
            }){
                @Override
                public Map<String, String> getParams()
                {
                    if (params != null)
                    {
                        return params;
                    }
                    Map<String, String>  para = new HashMap<>();
                    return para;
                }
            };
            requestQueue.add(stringRequest);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void call(String url)
    {
        call(url, null);
    }
}
