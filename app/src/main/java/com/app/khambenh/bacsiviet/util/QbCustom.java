package com.app.khambenh.bacsiviet.util;

import com.app.khambenh.bacsiviet.utils.QBUser;
import com.google.gson.annotations.SerializedName;

/**
 * Created by os-nguyenxuanduc on 3/24/2018.
 */

public class QbCustom extends QBUser {
    @SerializedName("website")
    protected String website;

    public String getWebsite() {
        return website;
    }
}
