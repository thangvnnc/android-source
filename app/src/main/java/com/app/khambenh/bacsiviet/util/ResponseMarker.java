package com.app.khambenh.bacsiviet.util;

import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseMarker {
    public class User{
        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("user_type_id")
        @Expose
        private int user_type_id;

        @SerializedName("distance")
        @Expose
        private Double distance;

        @SerializedName("fullname")
        @Expose
        private String fullname;

        @SerializedName("lat")
        @Expose
        private Double lat;

        @SerializedName("lng")
        @Expose
        private Double lng;
    }

    public class Doctors{
        @SerializedName("doctors")
        @Expose
        private JsonArray doctors;
    }

    public class Clinics{
        @SerializedName("clinics")
        @Expose
        private String clinics;
    }
}

