package com.app.khambenh.bacsiviet.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.SaleModel;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by os-nguyenxuanduc on 4/3/2018.
 */

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.MyViewHolder> {

    private List<SaleModel> arr_sale;
    private Context cContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_title, txt_des, txt_content;
        ImageView img;
        LinearLayout layout_sale;

        public MyViewHolder(View view) {
            super(view);
            txt_title = view.findViewById(R.id.deal_title);
            txt_des = view.findViewById(R.id.deal_des);
            txt_content = view.findViewById(R.id.deal_content);
            img = view.findViewById(R.id.image_deal);
            layout_sale = view.findViewById(R.id.layout_sale);
            layout_sale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(arr_sale.get(getAdapterPosition()).getLink_detail()));
                        cContext.startActivity(i);
                    } catch (Exception e) {
                        Log.e("Error: ", "" + e.getMessage());
                    }
                }
            });
        }
    }


    public SalesAdapter(Context context, List<SaleModel> moviesList) {
        this.arr_sale = moviesList;
        this.cContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_sale, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SaleModel saleModel = arr_sale.get(position);
        holder.txt_title.setText(saleModel.getDeal_title());
        holder.txt_content.setText(saleModel.getDeal_content());
        holder.txt_des.setText(saleModel.getDeal_description());
        Glide.with(cContext).load(saleModel.getImage()).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return arr_sale.size();
    }
}