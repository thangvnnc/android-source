package com.app.khambenh.bacsiviet.util;

import java.util.Date;

public class LogApp {
    private String userID;
    private String message;
    private String TagActivity;
    private Date createDate;

    public LogApp(String userID, String message, String tagActivity, Date date) {
        this.userID = userID;
        this.message = message;
        this.TagActivity = tagActivity;
        this.createDate = date;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTagActivity() {
        return TagActivity;
    }

    public void setTagActivity(String tagActivity) {
        TagActivity = tagActivity;
    }
}
