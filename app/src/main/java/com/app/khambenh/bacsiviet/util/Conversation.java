package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class Conversation implements Item, Comparable<Conversation> {

    private String userId_1;
    private String userId_2;
    private String lastMessage;
    private Date createTime;
    private String name_1;
    private String name_2;
    private String userAvatar_1;
    private String userAvatar_2;
    private int unread;
    private String lastUser;
    private boolean isMisscall;
    private String misscallById;
    private int statusMessage;

    public int getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(int statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getUserAvatar_1() {
        return userAvatar_1;
    }

    public void setUserAvatar_1(String userAvatar_1) {
        this.userAvatar_1 = userAvatar_1;
    }

    public String getUserAvatar_2() {
        return userAvatar_2;
    }

    public void setUserAvatar_2(String userAvatar_2) {
        this.userAvatar_2 = userAvatar_2;
    }

    public boolean isMisscall() {
        return isMisscall;
    }

    public void setMisscall(boolean misscall) {
        isMisscall = misscall;
    }

    public String getMisscallById() {
        return misscallById;
    }

    public void setMisscallById(String misscallById) {
        this.misscallById = misscallById;
    }

    public int getUnread() {
        return unread;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    public String getLastUser() {
        return lastUser;
    }

    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }

    public String getName_1() {
        return name_1;
    }

    public void setName_1(String name_1) {
        this.name_1 = name_1;
    }

    public String getName_2() {
        return name_2;
    }

    public void setName_2(String name_2) {
        this.name_2 = name_2;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserId_1() {
        return userId_1;
    }

    public void setUserId_1(String userId_1) {
        this.userId_1 = userId_1;
    }

    public String getUserId_2() {
        return userId_2;
    }

    public void setUserId_2(String userId_2) {
        this.userId_2 = userId_2;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    @Override
    public View getView(Context context, LayoutInflater layoutInflater, View convertView, int position) {

        int currUserId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);

        View rowView;
        if (convertView == null) {
            // rowView = (View) layoutInflater.inflate(R.layout.list_item_dialog, null);
            rowView = (View) layoutInflater.inflate(R.layout.k_item_conversation, null);
        } else {
            rowView = convertView;
        }
        TextView txt_created = rowView.findViewById(R.id.k_item_conversation_txt_time_id);
        TextView text_dialog_name = rowView.findViewById(R.id.k_item_conversation_txt_name_id);
        TextView text_dialog_last_message = rowView.findViewById(R.id.k_item_conversation_txt_content_id);
        TextView text_dialog_unread_count = rowView.findViewById(R.id.k_item_conversation_txt_unread_id);
        CircleImageView img_avatar = rowView.findViewById(R.id.k_item_conversation_img_avatar_id);

        if (Integer.parseInt(getUserId_1()) == currUserId) {
            Glide.with(context).load(getUserAvatar_2()).error(R.drawable.ic_other_user).into(img_avatar);
            text_dialog_name.setText(getName_2());
        } else {
            Glide.with(context).load(getUserAvatar_1()).error(R.drawable.ic_other_user).into(img_avatar);
            text_dialog_name.setText(getName_1());
        }

        if (isMisscall()) {
            text_dialog_last_message.setText(context.getString(R.string.notifi_you_have_missing_call));
        } else {
            text_dialog_last_message.setText(getLastMessage());
        }

        txt_created.setText(formatDate(getCreateTime()));

        if ((getLastUser() != null && !getLastUser().equalsIgnoreCase("" + currUserId))) {
            if (getUnread() > 0) {
                text_dialog_unread_count.setText("" + getUnread());
                text_dialog_unread_count.setVisibility(View.VISIBLE);
            } else {
                text_dialog_unread_count.setVisibility(View.INVISIBLE);
            }
        }


        return rowView;
    }

    private String formatDate(Date date) {
        SimpleDateFormat format2 = new SimpleDateFormat("hh:mm a");
        String result = format2.format(date);
        return result;
    }

    @Override
    public int compareTo(@NonNull Conversation o) {
        return o.getCreateTime().compareTo(getCreateTime());
    }
}
