package com.app.khambenh.bacsiviet.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.MessageFirebaseActivity;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.db.FireStoreFunction;
import com.app.khambenh.bacsiviet.util.Conversation;
import com.app.khambenh.bacsiviet.util.ResultUser;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;

import static com.app.khambenh.bacsiviet.utils.CommonUtils.getDocConversation;

public class ConversationFirebaseFragment extends Fragment {
    private static ConversationFirebaseFragment ConversationFirebaseFragment;

    ListView lvConversation;
    static ListAdapter listAdapter;
    ArrayList<Item> itemArrayList = new ArrayList<>();
    ArrayList<Conversation> conversationArrayList1 = new ArrayList<>();
    ArrayList<Conversation> conversationArrayList2 = new ArrayList<>();
    ArrayList<Conversation> conversationArrayList = new ArrayList<>();
    int currUserId;

    ProgressDialog progressDialog;

    public static ConversationFirebaseFragment getInstance() {
        if (ConversationFirebaseFragment == null) {
            ConversationFirebaseFragment = new ConversationFirebaseFragment();
        }
        return ConversationFirebaseFragment;
    }

    public ConversationFirebaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // View view = inflater.inflate(R.layout.fragment_conversation_firebase, container, false);
        View view = inflater.inflate(R.layout.k_frm_conversation, container, false);
        progressDialog = new ProgressDialog(getContext());
        lvConversation = view.findViewById(R.id.k_frm_conversation_lv_conversation_id);
        currUserId = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);
        loadConversation();

        return view;
    }

    private void initAdapter() {
        listAdapter = new ListAdapter(getContext(), itemArrayList);
        lvConversation.setAdapter(listAdapter);
        lvConversation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Conversation conversation = (Conversation) itemArrayList.get(position);

                conversation.setUnread(0);
                conversation.setMisscall(false);
                conversation.setMisscallById("");
                updateUnreadConversation(conversation);

                progressDialog.show();
                String idUser = getOpponentId(conversation);
                CommonUtils.GetUserServer(idUser, new ResultUser() {
                    @Override
                    public void onResult(QBUser qbUser) {
                        progressDialog.dismiss();
                        SharedPrefsHelper.getInstance().saveOpponentQbUser(qbUser);
                        MessageFirebaseActivity.startMessageFirebase(getContext(), false, qbUser);
                    }
                });
            }
        });
    }

    private void updateUnreadConversation(Conversation conversation) {
        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .document(getDocConversation(currUserId, Integer.parseInt(getOpponentId(conversation))))
                .set(conversation)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", "Error writing document", e);
                    }
                });
    }

    private void updateStatusConversation() {
//        notifyMessage();
        for (Conversation conversation : conversationArrayList) {
            if (conversation.getStatusMessage() == 0 && currUserId != Integer.parseInt(conversation.getLastUser())) {
                conversation.setStatusMessage(1);
                FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                        .document(getDocConversation(currUserId, Integer.parseInt(getOpponentId(conversation))))
                        .set(conversation)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e("ERROR", "Error writing document", e);
                            }
                        });
            }
        }
    }


    private void loadConversation() {
        initAdapter();
        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .whereEqualTo("userId_1", "" + currUserId)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e("ERROR", "Listen failed.", e);
                            return;
                        }
                        conversationArrayList1.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            Conversation conversation = doc.toObject(Conversation.class);
                            conversationArrayList1.add(conversation);
//                            if (conversationArrayList1.size() > 0) {
//                                boolean isExist = false;
//                                for (int i = 0; i < conversationArrayList1.size(); i++) {
//                                    Conversation conversation1 = conversationArrayList1.get(i);
//                                    if (conversation1.getUserId_1().equals(conversation.getUserId_1()) && conversation1.getUserId_2().equals(conversation.getUserId_2())) {
//                                        isExist = true;
//                                    }
//                                    if (!isExist) {
//                                        conversationArrayList1.add(conversation);
//                                    }
//                                }
//                            } else {
//                                conversationArrayList1.add(conversation);
//                            }
                        }
                        if (conversationArrayList1.size() != 0) {
                            conversationArrayList.clear();
                            conversationArrayList.addAll(conversationArrayList1);
                            conversationArrayList.addAll(conversationArrayList2);
                            removeDuplication(conversationArrayList);
                            Collections.sort(conversationArrayList);
                            itemArrayList.clear();
                            itemArrayList.addAll(conversationArrayList);
                        }

                        listAdapter.notifyDataSetChanged();
                        updateStatusConversation();
                    }
                });

        FireStoreFunction.getDb().collection(Consts.COLLECTION_CONVERSATION)
                .whereEqualTo("userId_2", "" + currUserId)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e("ERROR", "Listen failed.", e);
                            return;
                        }
                        conversationArrayList2.clear();
                        for (QueryDocumentSnapshot doc : value) {
                            Conversation conversation = doc.toObject(Conversation.class);
                            conversationArrayList2.add(conversation);
//                            if (conversationArrayList2.size() > 0) {
//                                boolean isExist = false;
//                                for (int i = 0; i < conversationArrayList2.size(); i++) {
//                                    Conversation conversation1 = conversationArrayList2.get(i);
//                                    if (conversation1.getUserId_1().equalsIgnoreCase(conversation.getUserId_1()) && conversation1.getUserId_2().equalsIgnoreCase(conversation.getUserId_2())) {
//                                        isExist = true;
//                                    }
//                                    if (!isExist) {
//                                        conversationArrayList2.add(conversation);
//                                    }
//                                }
//                            } else {
//                                conversationArrayList2.add(conversation);
//                            }

                        }
                        if (conversationArrayList2.size() != 0) {
                            conversationArrayList.clear();
                            conversationArrayList.addAll(conversationArrayList1);
                            conversationArrayList.addAll(conversationArrayList2);
                            removeDuplication(conversationArrayList);
                            Collections.sort(conversationArrayList);
                            itemArrayList.clear();
                            itemArrayList.addAll(conversationArrayList);
                        }
                        listAdapter.notifyDataSetChanged();
                        updateStatusConversation();

                    }
                });
    }

    private void removeDuplication(ArrayList<Conversation> conversations)
    {
        if(conversations == null)
        {
            return;
        }

        if(conversations.size() <= 0)
        {
            return;
        }

        ArrayList<Conversation> cloneList = new ArrayList<>();
        cloneList.addAll(conversations);
        conversations.clear();
        conversations.add(cloneList.get(0));

        for (int idx = 1; idx < cloneList.size(); idx++)
        {
            Conversation conversationClone = cloneList.get(idx);
            String u1Clone = conversationClone.getUserId_1();
            String u2Clone = conversationClone.getUserId_2();

            boolean isExist = false;
            for(int jdx = 0; jdx < conversations.size(); jdx++)
            {
                Conversation conversation = conversations.get(jdx);
                String u1 = conversation.getUserId_1();
                String u2 = conversation.getUserId_2();

                if (u1Clone.equals(u1) && u2Clone.equals(u2))
                {
                    isExist = true;
                    break;
                }
            }

            if (!isExist)
            {
                conversations.add(conversationClone);
            }
        }
    }

    private String getOpponentId(Conversation conversation) {
        if (Integer.parseInt(conversation.getUserId_1()) == currUserId) {
            return conversation.getUserId_2();
        } else {
            return conversation.getUserId_1();
        }
    }

    public static void clearData() {
        listAdapter.clear();
    }
}
