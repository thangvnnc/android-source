package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.AddMedicalRecordActivity;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.bumptech.glide.Glide;

public class ImageOfMedicalRecord implements Item {
    Uri uri;

    public ImageOfMedicalRecord(Uri uri){
        this.uri = uri;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    @Override
    public View getView(final Context context, LayoutInflater layoutInflater, View convertView, final int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_image_list_twoway, null);
        } else {
            rowView = convertView;
        }
        ImageView imageView = (ImageView)rowView.findViewById(R.id.imageView);
        ImageButton btnDelete = (ImageButton)rowView.findViewById(R.id.btnDelete);

        Glide.with(context).load(getUri()).into(imageView);
        if(context instanceof AddMedicalRecordActivity){
            btnDelete.setVisibility(View.VISIBLE);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Consts.ACTION_DELETE_IMAGE_OF_MEDICAL_RECORD);
                    intent.putExtra(Consts.EXTRA_POSITION,position);
                    context.sendBroadcast(intent);
                }
            });
        }else {
            btnDelete.setVisibility(View.GONE);
        }

        return rowView;
    }

}
