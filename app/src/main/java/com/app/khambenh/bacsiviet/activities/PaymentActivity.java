package com.app.khambenh.bacsiviet.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.APICall;
import com.app.khambenh.bacsiviet.util.SendOrder;
import com.app.khambenh.bacsiviet.util.SendOrder.SendOrderResult;
import com.app.khambenh.bacsiviet.util.SendOrderAPI;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.LanguageUtils;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;

import java.util.Random;

import static com.app.khambenh.bacsiviet.utils.CommonUtils.CANCEL_URL;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.MERCHANT_ACCOUNT;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.MERCHANT_ID;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.MERCHANT_PASSWORD;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.NOTIFY_URL;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.RETURN_URL;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.md5;

public class PaymentActivity extends BaseActivity {

    private QBUser currentUser;
    private EditText etName, etEmail, etPhone;
    private RadioGroup radioGroup;
    private AlertDialog alertDialog;
    private Spinner spinner;
    private LinearLayout lnlRecharge, lnlBankInfo;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageUtils.loadLocale(this);
        setContentView(R.layout.activity_payment);
        context = this;

        etName = findViewById(R.id.etNameCharge);
        etEmail = findViewById(R.id.etEmailCharge);
        etPhone = findViewById(R.id.etPhoneCharge);
        Button btnPay = findViewById(R.id.btnCharge);
        radioGroup = findViewById(R.id.radioGroupCharge);
        spinner = findViewById(R.id.spnChoosePayment);
        lnlRecharge = findViewById(R.id.lnlRecharge);
        lnlBankInfo = findViewById(R.id.lnlBankInfo);

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPaymentPage();
            }
        });

        currentUser = sharedPrefsHelper.getQbUser();
        etName.setText(currentUser.getFullName());
        etEmail.setText(currentUser.getEmail());
        etPhone.setText(currentUser.getPhone());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 1) {
                    lnlRecharge.setVisibility(View.VISIBLE);
                    lnlBankInfo.setVisibility(View.GONE);
                    return;
                }

                if(i == 2) {
                    String linhWeb = getResources().getString(R.string.link_web_recharge);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linhWeb));
                    startActivity(browserIntent);
                    return;
                }

                if (i == 3) {
                    lnlRecharge.setVisibility(View.GONE);
                    lnlBankInfo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void startPaymentPage() {
        if (!validateForm())
            return;
        sendOrderObject();
    }

    private void sendOrderObject() {
        String fullName = etName.getText().toString();

        int radioButtonId = radioGroup.getCheckedRadioButtonId();
        int amount = 0;
        if (radioButtonId == R.id.radioCharge50)
            amount = 50000;
        else if (radioButtonId == R.id.radioCharge100)
            amount = 100000;
        else if (radioButtonId == R.id.radioCharge200)
            amount = 200000;
        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_QUANTITY_WHEN_RECHARGE,amount);

        String email = etEmail.getText().toString();
        String phoneNumber = etPhone.getText().toString();
        SendOrder sendOrder = new SendOrder();
        sendOrder.setFunc("sendOrder");
        sendOrder.setVersion("1.0");
        sendOrder.setMerchantId(MERCHANT_ID);
        sendOrder.setMerchantAccount(MERCHANT_ACCOUNT);
        sendOrder.setOrderCode(currentUser.getLogin() != null ? currentUser.getLogin() : "" +
                String.valueOf(new Random().nextInt(1000000) + 100));
        sendOrder.setTotalAmount(amount);
        sendOrder.setCurrency("vnd");
        sendOrder.setLanguage(LanguageUtils.getCurrentLanguage().getCode());
        sendOrder.setReturnUrl(RETURN_URL);
        sendOrder.setCancelUrl(CANCEL_URL);
        sendOrder.setNotifyUrl(NOTIFY_URL);
        sendOrder.setBuyerFullName(fullName);
        sendOrder.setBuyerEmail(email);
        sendOrder.setBuyerMobile(phoneNumber);
        sendOrder.setBuyerAddress("");

        String checksum = getChecksum(sendOrder);
        sendOrder.setChecksum(checksum);


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.notifi_processing));

        progressDialog.show();


        new SendOrderAPI(new APICall<SendOrderResult>() {
            @Override
            public void onFinish(SendOrderResult result) {
                try {
                    progressDialog.dismiss();
                    if ("00".equalsIgnoreCase(result.getResponseCode())) {
                        Intent intentCheckout = new Intent(getApplicationContext(), CheckOutActivity.class);
                        intentCheckout.putExtra(CheckOutActivity.TOKEN_CODE, result.getToken());
                        intentCheckout.putExtra(CheckOutActivity.CHECKOUT_URL, result.getCheckoutUrl());
                        startActivity(intentCheckout);
                        finish();
                    } else {
                        showErrorDialog(CommonUtils.getCodeError(getApplicationContext(), result.getResponseCode()));
                    }
                } catch (Exception e) {
                    showErrorDialog(getString(R.string.error_connection_broken));

                }
            }
        }).execute(sendOrder);


    }

    private void showErrorDialog(String message) {
        alertDialog = new AlertDialog.Builder(PaymentActivity.this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

        alertDialog.show();
    }

    private String getChecksum(SendOrder sendOrder) {
        String stringSendOrder = sendOrder.getFunc() + "|" +
                sendOrder.getVersion() + "|" +
                sendOrder.getMerchantId() + "|" +
                sendOrder.getMerchantAccount() + "|" +
                sendOrder.getOrderCode() + "|" +
                sendOrder.getTotalAmount() + "|" +
                sendOrder.getCurrency() + "|" +
                sendOrder.getLanguage() + "|" +
                sendOrder.getReturnUrl() + "|" +
                sendOrder.getCancelUrl() + "|" +
                sendOrder.getNotifyUrl() + "|" +
                sendOrder.getBuyerFullName() + "|" +
                sendOrder.getBuyerEmail() + "|" +
                sendOrder.getBuyerMobile() + "|" +
                sendOrder.getBuyerAddress() + "|" +
                MERCHANT_PASSWORD;

        return md5(stringSendOrder);
    }

    private boolean validateForm() {
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, getString(R.string.notifi_select_a_payment_plan), Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(etName.getText())) {
            etName.setError(getString(R.string.error_empty_fullname));
            etName.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_empty_email));
            etEmail.requestFocus();
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches()) {
            etEmail.setError(getString(R.string.error_email_invalid));
            etEmail.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }
}
