package com.app.khambenh.bacsiviet.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.app.khambenh.bacsiviet.activities.SplashActivity;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;

public class VersionChecker extends AsyncTask<String, String, String> {
    private String latestVersion;
    private String currentVersion;
    private Context context;

    public VersionChecker(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        String url = "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=vi";
        try {
            latestVersion = Jsoup.connect(url)
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                    .first()
                    .ownText();
            Log.e("latestversion", "---" + latestVersion);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return latestVersion;
    }

    public void showForceUpdateDialog() {

        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName())));
    }
}
