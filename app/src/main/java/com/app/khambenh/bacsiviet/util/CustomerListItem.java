package com.app.khambenh.bacsiviet.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.Item;

import java.io.Serializable;

public class CustomerListItem implements Item, Serializable {
    private int id;
    private int icon;
    private String title;

    public CustomerListItem() {
    }

    public CustomerListItem(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public CustomerListItem(int id, int icon, String title) {
        this.id = id;
        this.icon = icon;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public View getView(Context context, LayoutInflater layoutInflater, View convertView, int position) {
        View rowView;
        if (convertView == null) {
            rowView = (View) layoutInflater.inflate(R.layout.item_customer_page, null);
        } else {
            rowView = convertView;
        }
        TextView title = (TextView) rowView.findViewById(R.id.title);
        ImageView icon = (ImageView) rowView.findViewById(R.id.icon);

        title.setText(getTitle());
        icon.setImageResource(getIcon());

        return rowView;
    }
}
