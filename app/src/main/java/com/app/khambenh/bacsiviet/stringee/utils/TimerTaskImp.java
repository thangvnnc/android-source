package com.app.khambenh.bacsiviet.stringee.utils;

public interface TimerTaskImp {
    void onStartTimerTask();
    void onStopTimerTask(int timer);
    void onTickTimerTask(int timer);
}
