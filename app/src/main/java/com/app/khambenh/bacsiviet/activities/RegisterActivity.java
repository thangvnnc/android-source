package com.app.khambenh.bacsiviet.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.MessageEvent;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends BaseActivity  {
    private Button btnRegister;
    private Activity self;
    private TextView[] groupAccountType;
    private int acccountType = 0;
    private TextView[] groupGender;
    private int gender = 3;
    private String arr_value_type[] = {"user", "professional", "place"};
    private String type = "user";
    private String regex = "[a-z0-9]+";
    private int type_gender = 3;
    private EditText txtFullname, txtPhone, txtUsername, txtPass, txtPresenter;
    private String phoneNumber = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        self = this;
        phoneNumber = getIntent().getStringExtra("phoneNumber");
        setContentView(R.layout.k_activity_singup_1);
        initView();
        initEvent();
    }

    private void initView() {
        txtFullname = findViewById(R.id.k_activity_signup_edt_fullname_id);
        txtPhone = findViewById(R.id.k_activity_signup_edt_phone_num_id);
        txtPhone.setEnabled(false);
        txtPhone.setFocusable(false);
        txtPhone.setText(phoneNumber);
        txtUsername = findViewById(R.id.k_activity_signup_edt_user_name_id);
        txtPass = findViewById(R.id.k_activity_signup_edt_password_id);
        txtPresenter = findViewById(R.id.k_activity_signup_edt_check_code_id);

        groupAccountType = new TextView[] {
                findViewById(R.id.k_activity_signup_txt_member_id),
                findViewById(R.id.k_activity_signup_txt_doctor_id),
                findViewById(R.id.k_activity_signup_txt_health_facilities_id)
        };
        setGroup(groupAccountType, new OnGroupSelected() {
            @Override
            public void Selected(int tag) {
                acccountType = tag;
            }
        });
        setColorSelectedItem(groupAccountType, acccountType);

        groupGender = new TextView[] {
                findViewById(R.id.k_activity_signup_txt_male_id),
                findViewById(R.id.k_activity_signup_txt_female_id),
                findViewById(R.id.k_activity_signup_txt_Other_id)
        };
        setGroup(groupGender, new OnGroupSelected() {
            @Override
            public void Selected(int tag) {
                gender = tag;
            }
        });
        setColorSelectedItem(groupGender, gender);

        btnRegister = findViewById(R.id.btnRegister);
    }

    interface OnGroupSelected {
        void Selected(int tag);
    }

    private void setGroup(final TextView []txtViews, final OnGroupSelected onGroupSelected) {
        int size = txtViews.length;
        for(int i = 0; i < size; i++) {
            TextView txtView = txtViews[i];
            txtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int tag = Integer.parseInt(String.valueOf(view.getTag()));
                    onGroupSelected.Selected(tag);
                    setColorSelectedItem(txtViews, tag);
                }
            });
        }
    }

    private void setColorSelectedItem(TextView []txtViews, int tagSelected) {
        int size = txtViews.length;
        for(int i = 0; i < size; i++) {
            TextView txtView = txtViews[i];
            int tag = Integer.parseInt(String.valueOf(txtView.getTag()));
            if (tagSelected == tag) {
                txtView.setBackgroundResource(R.drawable.k_bg_chipview_selected);
            }
            else {
                txtView.setBackgroundResource(R.drawable.k_bg_chipview_normal);
            }
        }
    }

    private void initEvent() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateForm()){
//                    verifyPhone();
                    String phoneNumber = getIntent().getStringExtra("phoneNumber");
                    checkPhoneNumberExistRegister(phoneNumber);
                }
            }
        });
    }

//    public void verifyPhone() {
//        AccountKit.logOut();
//        final Intent intent = new Intent(this, AccountKitActivity.class);
//        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
//                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN);
//
//        configurationBuilder.setReadPhoneStateEnabled(true);
//        PhoneNumber phoneNumber = new PhoneNumber("+84", txtPhone.getText().toString().trim());
//        configurationBuilder.setInitialPhoneNumber(phoneNumber);
//        UIManager uiManager = new SkinManager(
//                SkinManager.Skin.CLASSIC,
//                getResources().getColor(R.color.blue));
//        configurationBuilder.setUIManager(uiManager);
//        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
//        startActivityForResult(intent, APP_ACCOUNT_KIT_REQUEST_CODE);
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == APP_ACCOUNT_KIT_REQUEST_CODE) { // confirm that this response matches your request
//            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
//            if (loginResult.getError() != null) {
//                Toast.makeText(self, "System error", Toast.LENGTH_SHORT).show();
//            } else if (loginResult.wasCancelled()) {
//                Toast.makeText(self, "Đã từ chối đăng nhập facebook", Toast.LENGTH_SHORT).show();
//            } else {
//                getAccountKitInfo();
//            }
//        }
//    }

    private void getAccountKitInfo() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phone = phoneNumber.getPhoneNumber();
                    JSONObject postparams = new JSONObject();
                    try {
                        postparams.put("phone", phone + "");
                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                                URLUtils.URL_CHECK_PHONE, postparams, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if (response != null) {
                                    try {
                                        boolean isExist = response.getBoolean("isExist");
                                        if(isExist){
                                            String msg = response.getString("msg");
                                            CommonUtils.showToast(self, msg);
                                        } else {
                                            processRegister();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(final AccountKitError error) {
                // Handle Error
                Log.e("Nhan", error.toString());
            }
        });
    }

    private void checkPhoneNumberExistRegister(String phone) {
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("phone", phone + "");
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_CHECK_PHONE, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            boolean isExist = response.getBoolean("isExist");
                            if(isExist){
                                String msg = response.getString("msg");
                                CommonUtils.showToast(self, msg);
                            } else {
                                processRegister();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processRegister() throws JSONException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.registering));
        progressDialog.show();
        JSONObject postparams = new JSONObject();

        postparams.put("username", "" + txtUsername.getText());
        postparams.put("password", "" + txtPass.getText());
        postparams.put("fullname", "" + txtFullname.getText());
        postparams.put("gender", type_gender);
        postparams.put("phone", "" + txtPhone.getText());
        postparams.put("type", type);
        postparams.put("presenter", "" + txtPresenter.getText());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URLUtils.URL_REGISTER, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        if (response != null) {
                            try {
                                if (response.getString("isLogin").equals("true")) {
                                    EventBus.getDefault().post(new MessageEvent(txtUsername.getText().toString(), txtPass.getText().toString()));
                                    finish();
                                } else {
                                    CommonUtils.SaveLLog(RegisterActivity.this, "", response.toString(), Consts.TAG_LOGIN_ACTIVITY);
//                                    Toast.makeText(self, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(self, "System error", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

    }

    private boolean validateForm() {
        if ("".equals(txtFullname.getText().toString().trim())) {
            Toast.makeText(self, getString(R.string.error_empty_fullname), Toast.LENGTH_LONG).show();
            txtFullname.requestFocus();
            return false;
        }
        if ("".equals(txtUsername.getText().toString().trim())) {
            Toast.makeText(self, getString(R.string.error_empty_username), Toast.LENGTH_LONG).show();
            txtUsername.requestFocus();
            return false;
        }
        if ("".equals(txtPass.getText().toString().trim())) {
            Toast.makeText(self, getString(R.string.error_empty_password), Toast.LENGTH_LONG).show();
            txtPass.requestFocus();
            return false;
        }
        if (txtPhone.getText().toString().trim().length() < 8) {
            Toast.makeText(self, getString(R.string.error_phone_invalid), Toast.LENGTH_LONG).show();
            txtPass.requestFocus();
            return false;
        }

        String username = txtUsername.getText().toString();
        if (!username.matches(regex)) {
            CommonUtils.showToast(this, getString(R.string.wrong_username));
            return false;
        }
        return true;
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
}
