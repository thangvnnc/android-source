package com.app.khambenh.bacsiviet.util;

/**
 * Created by binh on 8/19/2018.
 */

public abstract class APICall<T> {
    protected abstract void onFinish(T result);
}
