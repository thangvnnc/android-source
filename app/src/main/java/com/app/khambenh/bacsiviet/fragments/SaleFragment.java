package com.app.khambenh.bacsiviet.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.SalesAdapter;
import com.app.khambenh.bacsiviet.util.SaleModel;
import com.app.khambenh.bacsiviet.utils.URLUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by os-nguyenxuanduc on 4/2/2018.
 */

public class SaleFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    View rootView;
    private static SaleFragment saleFragment;
    private RecyclerView rc_sale;
    ArrayList<SaleModel> arr_sale;
    private boolean isLoad = false;
    private SwipeRefreshLayout swipe_sale;

    public static SaleFragment getInstance() {
        if (saleFragment == null) {
            saleFragment = new SaleFragment();
        }
        return saleFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_fragment_sale, container, false);
        rc_sale = rootView.findViewById(R.id.rc_sale);
        swipe_sale = rootView.findViewById(R.id.swipe_sale);
        swipe_sale.setOnRefreshListener(this);
        arr_sale = new ArrayList<>();
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isLoad && isVisibleToUser) {
            isLoad = true;
            processData();
        }
    }
//

    //TODO: processLogin
    private void processData() {
        swipe_sale.setRefreshing(true);
        JSONArray postparams = new JSONArray();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST,
                URLUtils.URL_LIST_PROMOTION, postparams,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("test_json", "" + response.toString());
                        if (response != null) {
                            for (int i = 0; i < response.length(); i++) {

                                try {
                                    JSONObject sale_item = (JSONObject) response
                                            .get(i);
                                    SaleModel saleModel = new SaleModel();
                                    if (sale_item.has("deal_id")) {
                                        saleModel.setDeal_id(sale_item.getInt("deal_id"));
                                    }
                                    if (sale_item.has("deal_title")) {
                                        saleModel.setDeal_title(sale_item.getString("deal_title"));
                                    }
                                    if (sale_item.has("image")) {
                                        saleModel.setImage(sale_item.getString("image"));
                                    }
                                    if (sale_item.has("deal_description")) {
                                        saleModel.setDeal_description(sale_item.getString("deal_description"));
                                    }
                                    if (sale_item.has("deal_content")) {
                                        saleModel.setDeal_content(sale_item.getString("deal_content"));
                                    }
                                    if (sale_item.has("link_detail")) {
                                        saleModel.setLink_detail(sale_item.getString("link_detail"));
                                    }
                                    arr_sale.add(saleModel);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            swipe_sale.setRefreshing(false);
                            // Set Adapter
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            rc_sale.setLayoutManager(mLayoutManager);
                            rc_sale.setItemAnimator(new DefaultItemAnimator());
                            SalesAdapter salesAdapter = new SalesAdapter(getActivity(), arr_sale);
                            rc_sale.setAdapter(salesAdapter);
                        }
                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipe_sale.setRefreshing(false);
                        Log.d("test_json", "" + error.toString());
                    }

                });

// Adding the request to the queue along with a unique string tag
        CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");

    }


    @Override
    public void onRefresh() {
        swipe_sale.setRefreshing(true);
        processData();
    }
}
