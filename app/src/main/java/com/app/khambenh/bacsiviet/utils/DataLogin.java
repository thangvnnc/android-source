package com.app.khambenh.bacsiviet.utils;

/**
 * Created by haibi on 07-Jan-18.
 */

public class DataLogin {
    private String email, pass;

    public DataLogin(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }


    public String getPass() {
        return pass;
    }


}
