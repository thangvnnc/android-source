package com.app.khambenh.bacsiviet.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private static MapFragment mapFragment;
    View view;
    private GoogleMap mMap;
    private Location myLocation = null;
    SupportMapFragment spMapFragment;
    MapView mMapView;
    private String isLoginUser;
    private boolean isLoadMyLocation = true;
    boolean isFirst = false;
    private LocationManager mLocationManager = null;
    LatLng myLatLng;
    public static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 20;
    public static final int LOCATION_UPDATE_MIN_TIME = 1000;
    Button btn_update, btn_reload;
    //    EditText edit_update_location;
    LinearLayout lnLayout_update_location;
    int user_id;
    HashMap<Marker, QBUser> qbUserMarkerHashMap = new HashMap<>();
    Map<String, Object> myMarkerHashMap = new HashMap<>();
    ProgressDialog progressDialog;

    public static MapFragment getInstance() {
        if (mapFragment == null) {
            mapFragment = new MapFragment();
        }
        return mapFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);
//        spMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
//        spMapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (myLocation == null) {
            handler.removeCallbacks(runnable);
//        progressDialog.dismiss();
            myLocation = location;
            myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            getNearLocation(Consts.Distance_Default);
        }
    }

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());

        }
    };

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        progressDialog = new ProgressDialog(getContext());
        user_id = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_ID, Integer.class);

        btn_update = view.findViewById(R.id.btn_update);
        btn_reload = view.findViewById(R.id.btn_reload);
        isLoginUser = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_TYPE, String.class);
        if (isLoginUser.equals("user")) {
            btn_update.setVisibility(View.INVISIBLE);
        } else {
            btn_update.setVisibility(View.VISIBLE);
        }

        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myLatLng = mMap.getCameraPosition().target;
                getNearLocation(Consts.Distance_Default);
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMarker();
            }
        });
        //
        try {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.setMyLocationEnabled(true);

//            progressDialog.setMessage(getString(R.string.notifi_geting_your_location));
//            progressDialog.setCanceledOnTouchOutside(false);
//            progressDialog.show();

            runnable.run();
        } catch (SecurityException e) {
            CommonUtils.showToast(getContext(), e.getMessage());
        }
    }

    private void selectMarker() {
        LatLng latLng = null;
        if (myLatLng != null) {
            latLng = myLatLng;
        }
        if (latLng != null) {
            addMarker(latLng, "", "", 0, 0, "");
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            setMyLocation(latLng);
        } else {
            CommonUtils.showToast(getActivity(), getActivity().getString(R.string.error_cant_get_current_location));
        }
    }

    private void setMyLocation(LatLng latLng) {
        try {
            JSONObject postparams = new JSONObject();
            postparams.put("user_id", user_id);
            postparams.put("lat", latLng.latitude);
            postparams.put("lng", latLng.longitude);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_SET_LOCATION, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
//                        edit_update_location.setText("");
                        CommonUtils.showToast(getActivity(), getString(R.string.notifi_update_success));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(getActivity(), error.getMessage());
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            CommonUtils.showToast(getActivity(), e.getMessage());
        }
    }

    private void getMyLocation() {
        try {
            JSONObject postparams = new JSONObject();
            postparams.put("user_id", user_id);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_GET_LOCATION, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            boolean isSuccess = response.getBoolean("success");
                            if (isSuccess) {
                                JSONObject jsonObject = response.getJSONObject("location");
                                double lat = jsonObject.getDouble("lat");
                                double lng = jsonObject.getDouble("lng");
                                LatLng latLng = new LatLng(lat, lng);
                                if (latLng != null) {
                                    addMarker(latLng, "", "", 0, 0, "");
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                                }
                            } else {
                                if (myLocation != null) {
                                    myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 15));
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(getActivity(), error.getMessage());
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            CommonUtils.showToast(getActivity(), e.getMessage());
        }
    }

    private void getNearLocation(double distance) {
        mMap.clear();
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("distance", distance);
            postparams.put("lat", myLatLng.latitude);
            postparams.put("lng", myLatLng.longitude);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URLUtils.URL_SEARCH_LOCATION, postparams, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        try {
                            myMarkerHashMap = jsonToMap(response);
                            showMarkerDoctors();
                            showMarkerClinics();
                            showMarkerDrugStores();

                            if (!isFirst) {
                                isFirst = true;
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(myLatLng).zoom(11).build();
                                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 11));
                            }

                            MarkerOptions option = new MarkerOptions();
                            option.position(myLatLng);
                            option.title(getString(R.string.my_location));
                            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            Marker currentMarker = mMap.addMarker(option);
                            currentMarker.showInfoWindow();

//                            JSONArray jsonArray = response.getJSONArray("user");
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                int userID = jsonObject.getInt("user_id");
//                                String id = String.valueOf(userID);
//                                String fullname = jsonObject.getString("fullname");
//                                int type_user = jsonObject.getInt("user_type_id");
//                                double distance = jsonObject.getDouble("distance");
//                                double lat = jsonObject.getDouble("lat");
//                                double lng = jsonObject.getDouble("lng");
//                                LatLng latLng = new LatLng(lat, lng);
//                                if (latLng != null) {
//                                    if (userID == user_id) {
//                                        isLoadMyLocation = false;
//                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//                                        addMarker(latLng, "", fullname, 0, distance);
//                                    } else {
//                                        addMarker(latLng, id, fullname, type_user, distance);
//                                    }
//                                }
//                            }
//                            if (isLoadMyLocation && !isLoginUser.equals("user")) {
//                                getMyLocation();
//                            }
//                            if (isLoginUser.equals("user")) {
//                                if (myLocation != null) {
//                                    myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
//                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLatLng).zoom(15).build();
//                                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 15));
//                                }
//                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.showToast(getActivity(), error.getMessage());
                    //Failure Callback

                }
            });
            CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
        } catch (Exception e) {
            CommonUtils.showToast(getActivity(), e.getMessage());
        }
    }

    // Khi người dùng trả lời yêu cầu cấp quyền (cho phép hoặc từ chối).
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        switch (requestCode) {
            case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION: {


                // Chú ý: Nếu yêu cầu bị bỏ qua, mảng kết quả là rỗng.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getActivity(), "Permission granted!", Toast.LENGTH_LONG).show();

                    // Hiển thị vị trí hiện thời trên bản đồ.
//                    showMyLocation();
                }
                // Hủy bỏ hoặc từ chối.
                else {
                    Toast.makeText(getActivity(), "Permission denied!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }


    private void addMarker(final LatLng latLng, final String user_id, final String fullname, final int type, final double distance, final String speciality) {
        if (type == Consts.TYPE_DEFAULT) {
            MarkerOptions option = new MarkerOptions();
            option.position(latLng);
            option.title(getString(R.string.my_location));
            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            final Marker currentMarker = mMap.addMarker(option);
            currentMarker.showInfoWindow();
        } else {
//            Performer<QBUser> qbUser = QBUsers.getUserByExternalId(user_id);
//            qbUser.performAsync(new QBEntityCallback<QBUser>() {
//                @Override
//                public void onSuccess(final QBUser qbUser, Bundle bundle) {
                    QBUser qbUser = null;
                    if (qbUser != null) {
                        String split[] = qbUser.getCustomData().split("-_-");
                        // Thêm Marker cho Map:
                        MarkerOptions option = new MarkerOptions();
                        option.position(latLng);
                        option.title("Medix - " + getDistance(distance));
                        option.snippet(split[0]);
                        if (type == Consts.TYPE_DOCTOR) {
                            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_doctor_map));
                        } else if (type == Consts.TYPE_CLINIC) {
                            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_clinic_map));
                        } else if (type == Consts.TYPE_DRUG) {
                            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pharmacy));
                        }

                        final Marker currentMarker = mMap.addMarker(option);
                        qbUserMarkerHashMap.put(currentMarker, qbUser);
                        currentMarker.showInfoWindow();
                        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                QBUser qb = qbUserMarkerHashMap.get(marker);
                                if (qb != null) {
                                    CallUltils.chat(qb, getActivity());
                                }

                            }
                        });
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                if (marker.equals(currentMarker)) {
//                                        progressDialog.show();
                                    CallUltils.chat(qbUser, getActivity());
                                }
                                return false;
                            }
                        });
                    } else {
                        showMarkerNotSaveQB(latLng, user_id, fullname, type, distance, speciality);
                    }
//                }
//
//                @Override
//                public void onError(QBResponseException e) {
//                    showMarkerNotSaveQB(latLng, user_id, fullname, type, distance, speciality);
//                }
//            });
        }
    }

    private void showMarkerNotSaveQB(LatLng latLng, String user_id, String fullname, int type, double distance, String speciality) {
        MarkerOptions option = new MarkerOptions();
        option.position(latLng);
        option.title(getDistance(distance));
        if (type == Consts.TYPE_DOCTOR) {
            option.snippet(fullname + "-" + speciality);
            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_doctor_map));
        } else if (type == Consts.TYPE_CLINIC) {
            option.snippet(fullname + "-" + speciality);
            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_clinic_map));
        } else if (type == Consts.TYPE_DRUG) {
            option.snippet(fullname + "-" + speciality);
            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pharmacy));
        }

        final Marker currentMarker = mMap.addMarker(option);
        currentMarker.showInfoWindow();
    }

    private void showMarkerDoctors() {
        ArrayList arratDoctors = (ArrayList) myMarkerHashMap.get("doctors");
        for (int i = 0; i < arratDoctors.size(); i++) {
            String speciality = "";
            HashMap<String, HashMap> hashMapDoctors = (HashMap<String, HashMap>) arratDoctors.get(i);

            HashMap<String, ArrayList> hashMapDoctor = (HashMap<String, ArrayList>) hashMapDoctors.get("doctor");
            ArrayList arraySpecialitys = (ArrayList) hashMapDoctor.get("specialitys");
            for (int j = 0; j < arraySpecialitys.size(); j++) {
                HashMap<String, ArrayList> hmSpecialitys = (HashMap<String, ArrayList>) arraySpecialitys.get(j);
                ArrayList arraySpeciality_detail = (ArrayList) hmSpecialitys.get("speciality_detail");
                for (int k = 0; k < arraySpeciality_detail.size(); k++) {
                    HashMap<String, HashMap> hmSpecialityDetail = (HashMap<String, HashMap>) arraySpeciality_detail.get(k);
                    Object ojb = hmSpecialityDetail.get("speciality_name");
                    String speciality_name = ojb.toString();
                    speciality = speciality + ", " + speciality_name;
                }
            }

            Object objectUserId = hashMapDoctors.get("user_id");
            String userid = objectUserId.toString();
            int id = Integer.parseInt(userid);

            Object objectType = hashMapDoctors.get("user_type_id");
            int type = Integer.parseInt(objectType.toString());

            Object objectFullname = hashMapDoctors.get("fullname");
            String fullname = objectFullname.toString();

            Object objDistance = hashMapDoctors.get("distance");
            Object objLat = hashMapDoctors.get("lat");
            Object objLng = hashMapDoctors.get("lng");

            double distance = Double.valueOf(objDistance.toString());
            double lat = Double.valueOf(objLat.toString());
            double lng = Double.valueOf(objLng.toString());
            LatLng latLng = new LatLng(lat, lng);

            if (id == user_id) {
                isLoadMyLocation = false;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                addMarker(latLng, "", fullname, 0, distance, "");
            } else {
                addMarker(latLng, userid, fullname, type, distance, speciality);
            }
        }
    }

    private void showMarkerClinics() {
        ArrayList arrayClinics = (ArrayList) myMarkerHashMap.get("clinics");
        for (int i = 0; i < arrayClinics.size(); i++) {
            String speciality = "";
            HashMap<String, ArrayList> hashMapClinics = (HashMap<String, ArrayList>) arrayClinics.get(i);

            ArrayList arrayclinic = (ArrayList) hashMapClinics.get("clinic");
            for (int j = 0; j < arrayclinic.size(); j++) {
                HashMap<String, ArrayList> hmClinic = (HashMap<String, ArrayList>) arrayclinic.get(j);
                ArrayList arraySpeciality_detail = (ArrayList) hmClinic.get("speciality_detail");
                for (int k = 0; k < arraySpeciality_detail.size(); k++) {
                    HashMap<String, HashMap> hmSpecialityDetail = (HashMap<String, HashMap>) arraySpeciality_detail.get(k);
                    Object ojb = hmSpecialityDetail.get("speciality_name");
                    String speciality_name = ojb.toString();
                    if (speciality.equalsIgnoreCase("")) {
                        speciality = speciality_name;
                    } else {
                        speciality = speciality + ", " + speciality_name;
                    }
                }
            }
            Object objectUserId = hashMapClinics.get("user_id");
            String userid = objectUserId.toString();
            int id = Integer.parseInt(userid);

            Object objectType = hashMapClinics.get("user_type_id");
            int type = Integer.parseInt(objectType.toString());

            Object objectFullname = hashMapClinics.get("fullname");
            String fullname = objectFullname.toString();

            Object objDistance = hashMapClinics.get("distance");
            Object objLat = hashMapClinics.get("lat");
            Object objLng = hashMapClinics.get("lng");

            double distance = Double.valueOf(objDistance.toString());
            double lat = Double.valueOf(objLat.toString());
            double lng = Double.valueOf(objLng.toString());
            LatLng latLng = new LatLng(lat, lng);

            if (id == user_id) {
                isLoadMyLocation = false;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                addMarker(latLng, "", fullname, 0, distance, "");
            } else {
                addMarker(latLng, userid, fullname, type, distance, speciality);
            }
        }
    }

    private void showMarkerDrugStores() {
        ArrayList arrayDrug = (ArrayList) myMarkerHashMap.get("drugstores");
        for (int i = 0; i < arrayDrug.size(); i++) {
            HashMap<String, HashMap> hashMapDrug = (HashMap<String, HashMap>) arrayDrug.get(i);
            String speciality = "";
            Object drugstore = hashMapDrug.get("drugstore");
            if (!drugstore.toString().equalsIgnoreCase("null")) {
                HashMap<String, HashMap> hashMapDrugStore = hashMapDrug.get("drugstore");
                Object objectSpeciality = hashMapDrugStore.get("drugstore_desc");
                speciality = objectSpeciality.toString();
            }
            Object objectUserId = hashMapDrug.get("user_id");
            String userid = objectUserId.toString();
            int id = Integer.parseInt(userid);

            Object objectType = hashMapDrug.get("user_type_id");
            int type = Integer.parseInt(objectType.toString());

            Object objectFullname = hashMapDrug.get("fullname");
            String fullname = objectFullname.toString();

            Object objDistance = hashMapDrug.get("distance");
            Object objLat = hashMapDrug.get("lat");
            Object objLng = hashMapDrug.get("lng");

            double distance = Double.valueOf(objDistance.toString());
            double lat = Double.valueOf(objLat.toString());
            double lng = Double.valueOf(objLng.toString());
            LatLng latLng = new LatLng(lat, lng);

            if (id == user_id) {
                isLoadMyLocation = false;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                addMarker(latLng, "", fullname, 0, distance, "");
            } else {
                addMarker(latLng, userid, fullname, type, distance, speciality);
            }
        }
    }

    private String getDistance(double distance) {
        String str = "";
        double s = distance * 1000;
        if (s > 1000) {
            str = String.valueOf((int) distance) + "km";
        } else {
            str = String.valueOf((int) s) + "m";
        }
        return str;
    }

    private void getCurrentLocation() {
        try {
            Location location = null;
            if (getActivity() != null) {
                mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!(isGPSEnabled || isNetworkEnabled))
                    CommonUtils.showToast(getContext(), getString(R.string.error_cant_get_location));
                else {
                    if (isNetworkEnabled) {
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                                LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this);
                        location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }

                    if (isGPSEnabled) {
                        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                                LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this);
                        location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }
            if (location != null) {
//                progressDialog.dismiss();
                myLocation = location;
            }
        } catch (SecurityException e) {
            CommonUtils.showToast(getContext(), e.getMessage());
        }
    }

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (myLocation == null) {
                getCurrentLocation();
                handler.postDelayed(this, 3000);
            } else {
                myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                getNearLocation(Consts.Distance_Default);
                handler.removeCallbacks(this);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(false);
        mLocationManager.removeUpdates(this);
        mLocationManager = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        getCurrentLocation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}
