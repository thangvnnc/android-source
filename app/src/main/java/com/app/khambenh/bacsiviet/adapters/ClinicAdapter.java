package com.app.khambenh.bacsiviet.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.Clinic;
import com.app.khambenh.bacsiviet.utils.CallUltils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * QuickBlox team
 */
public class ClinicAdapter extends RecyclerView.Adapter<ClinicAdapter.UserViewHolder> {
    private Activity mContext;
    private List<Clinic> arr_users;
    ProgressDialog progressDialog;

    public ClinicAdapter(Activity context, List<Clinic> users) {
        this.mContext = context;
        this.arr_users = users;
        this.progressDialog = new ProgressDialog(mContext);
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_opponents_list, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        final Clinic clinic = arr_users.get(position);

        holder.opponentName.setText(clinic.getClinic_name());
        holder.txt_chuyennganh.setText(clinic.getProvince_name());
        Glide.with(mContext).load(clinic.getProfile_image()).error(R.drawable.ic_clinic).into(holder.opponentIcon);
        if (clinic.getFeatured() == 1) {
            holder.img_status.setVisibility(View.VISIBLE);
        } else {
            holder.img_status.setVisibility(View.INVISIBLE);
        }
        holder.ratingBar.setRating(clinic.getVote());
        holder.ratingBar.setIsIndicator(true);

        holder.layout_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showAction(user, finalLink_profile);
                progressDialog.setMessage(mContext.getString(R.string.notifi_connection_with_chat) + " " + clinic.getClinic_name());
                progressDialog.show();
//                Performer<QBUser> qbUser = QBUsers.getUserByExternalId(clinic.getUser_id());
//                qbUser.performAsync(new QBEntityCallback<QBUser>() {
//                    @Override
//                    public void onSuccess(QBUser qbUser, Bundle bundle) {
                        QBUser qbUser = null;
                        progressDialog.dismiss();
                        CallUltils.checkToCall(qbUser, mContext, Consts.TYPE_CHAT);
                        SharedPrefsHelper.getInstance().saveOpponentQbUser(qbUser);
//                    }
//
//                    @Override
//                    public void onError(QBResponseException e) {
//                        progressDialog.dismiss();
//                        CommonUtils.showToast(mContext, clinic.getClinic_name() + " " + mContext.getString(R.string.error_found_user_on_qb));
//                        Log.e("ERROR_QB", e.getMessage().toUpperCase());
//                    }
//
//                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return arr_users.size();
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        CircleImageView opponentIcon;
        ImageView img_status;
        TextView opponentName, txt_chuyennganh;
        LinearLayout layout_holder;
        RatingBar ratingBar;

        public UserViewHolder(View view) {
            super(view);
            opponentIcon = view.findViewById(R.id.image_opponent_icon);
            opponentName = view.findViewById(R.id.opponentsName);
            img_status = view.findViewById(R.id.img_status);
            txt_chuyennganh = view.findViewById(R.id.txt_chuyennganh);
            layout_holder = view.findViewById(R.id.layout_holder);
            ratingBar = view.findViewById(R.id.ratingBar);
        }
    }

}
