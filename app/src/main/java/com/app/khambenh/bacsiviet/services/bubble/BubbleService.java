package com.app.khambenh.bacsiviet.services.bubble;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;

import com.app.khambenh.bacsiviet.R;

/**
 * Created by bijoysingh on 2/19/17.
 */

public class BubbleService extends FloatingBubbleService {

  public static void show(Context context) {
    context.startService(new Intent(context, BubbleService.class));
  }

  @Override
  protected FloatingBubbleConfig getConfig() {
    Context context = getApplicationContext();

    return new FloatingBubbleConfig.Builder()
        .bubbleIcon(ContextCompat.getDrawable(context, R.drawable.ic_bubble))
        .removeBubbleIcon(ContextCompat.getDrawable(context, R.drawable.close_default_icon))
        .bubbleIconDp(54)
        .removeBubbleIconDp(54)
        .paddingDp(4)
        .borderRadiusDp(0)
        .physicsEnabled(false)
        .expandableColor(Color.WHITE)
        .triangleColor(0xFF215A64)
        .gravity(Gravity.LEFT)
        .build();
  }
}
