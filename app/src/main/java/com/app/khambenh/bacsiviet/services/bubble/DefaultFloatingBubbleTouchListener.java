package com.app.khambenh.bacsiviet.services.bubble;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.activities.SplashActivity;

/**
 * Created by bijoysingh on 2/19/17.
 */

public class DefaultFloatingBubbleTouchListener implements FloatingBubbleTouchListener {
  @Override
  public void onDown(float x, float y) {
    Log.v("AAA", "onDown");
  }

  @Override
  public void onTap(boolean expanded) {
    Log.v("AAA", "onTap");
    Context context = CoreApp.getAppContext();
    Intent dialogIntent = new Intent(context, SplashActivity.class);
    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(dialogIntent);
  }

  @Override
  public void onRemove() {
    Log.v("AAA", "onRemove");

  }

  @Override
  public void onMove(float x, float y) {
    Log.v("AAA", "onMove");

  }

  @Override
  public void onUp(float x, float y) {
    Log.v("AAA", "onUp");

  }
}
