package com.app.khambenh.bacsiviet.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public interface ItemViewType {
    public int getViewType();
    View getView(Context context, LayoutInflater layoutInflater, View convertView, int position);
}
