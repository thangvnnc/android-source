package com.app.khambenh.bacsiviet.util;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendOrder implements Serializable{
    @SerializedName("func")
    private String func;
    @SerializedName("version")
    private String version;
    @SerializedName("merchant_id")
    private String merchantId;
    @SerializedName("merchant_account")
    private String merchantAccount;
    @SerializedName("order_code")
    private String orderCode;
    @SerializedName("total_amount")
    private int totalAmount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("language")
    private String language;
    @SerializedName("return_url")
    private String returnUrl;
    @SerializedName("cancel_url")
    private String cancelUrl;
    @SerializedName("notify_url")
    private String notifyUrl;
    @SerializedName("buyer_fullname")
    private String buyerFullName;
    @SerializedName("buyer_email")
    private String buyerEmail;
    @SerializedName("buyer_mobile")
    private String buyerMobile;
    @SerializedName("buyer_address")
    private String buyerAddress;
    @SerializedName("checksum")
    private String checksum;

    public SendOrder() {

    }

    public void setFunc(String func) {
        this.func = func;
    }

    public String getFunc() {
        return func;
    }

    public void setVersion(String pVersion) {
        this.version = pVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantAccount(String pMerchantAccount) {
        this.merchantAccount = pMerchantAccount;
    }

    public String getMerchantAccount() {
        return merchantAccount;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    public String getCancelUrl() {
        return cancelUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setBuyerFullName(String buyerFullName) {
        this.buyerFullName = buyerFullName;
    }

    public String getBuyerFullName() {
        return buyerFullName;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerMobile(String buyerMobile) {
        this.buyerMobile = buyerMobile;
    }

    public String getBuyerMobile() {
        return buyerMobile;
    }

    public void setBuyerAddress(String buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksum() {
        return checksum;
    }

    public static class SendOrderResult implements Serializable {
        @SerializedName("response_code")
        private String responseCode;
        @SerializedName("token_code")
        private String token;
        @SerializedName("checkout_url")
        private String checkoutUrl;
        public SendOrderResult() {
        }

        public String getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(String responseCode) {
            this.responseCode = responseCode;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getCheckoutUrl() {
            return checkoutUrl;
        }

        public void setCheckoutUrl(String checkoutUrl) {
            this.checkoutUrl = checkoutUrl;
        }
    }
}
