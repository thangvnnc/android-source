package com.app.khambenh.bacsiviet.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.SplashActivity;
import com.app.khambenh.bacsiviet.adapters.ListAdapter;
import com.app.khambenh.bacsiviet.util.Language;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.app.khambenh.bacsiviet.utils.ResourceUtils.getString;

//Add by Tuan
public class LanguageUtils {

    private static Language sCurrentLanguage = null;


    public static Language getCurrentLanguage() {
        if (sCurrentLanguage == null) {
            sCurrentLanguage = initCurrentLanguage();
        }
        return sCurrentLanguage;
    }

    /**
     * check language exist in SharedPrefs, if not exist then default language is English
     */
    private static Language initCurrentLanguage() {
        Language currentLanguage =
                SharedPrefs.getInstance().get(SharedPrefs.LANGUAGE, Language.class);
        if (currentLanguage != null) {
            return currentLanguage;
        }
        currentLanguage = new Language(Consts.DEFAULT_LANGUAGE_ID,
                CoreApp.getInstance().getString(R.string.language_vietnamese),
                CoreApp.getInstance().getString(R.string.language_vietnamese_code), R.drawable.ic_vi);
        SharedPrefs.getInstance().put(SharedPrefs.LANGUAGE, currentLanguage);
        return currentLanguage;
    }

    /**
     * load current locale and change language
     */
    public static void loadLocale(Context context) {
        changeLanguage(context, initCurrentLanguage());
    }

    /**
     * change app language
     */
    public static void changeLanguage(Context context, Language language) {
        SharedPrefs.getInstance().put(SharedPrefs.LANGUAGE, language);
        sCurrentLanguage = language;
        Locale locale = new Locale(language.getCode());
        setLocale(context, language.getCode());
//
//        Resources resources = CoreApp.getInstance().getResources();
//        Configuration configuration = resources.getConfiguration();
//        configuration.setLocale(locale);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            configuration.setLocale(locale);
//        } else {
//            configuration.locale = locale;
//        }
//        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    /**
     * return language list from string.xml
     */
    public static ArrayList<Item> getListLanguage() {
        ArrayList<Item> arrLanguageUtils = new ArrayList<>();
        List<Integer> languageIcons = new ArrayList<>();
        languageIcons.add(R.drawable.ic_en_us);
        languageIcons.add(R.drawable.ic_vi);
        languageIcons.add(R.drawable.ic_fr);

        List<String> languageNames =
                Arrays.asList(CoreApp.getInstance().getResources().getStringArray(R.array.language_names));
        List<String> languageCodes =
                Arrays.asList(CoreApp.getInstance().getResources().getStringArray(R.array.language_codes));
        if (languageNames.size() != languageCodes.size()) {
            // error, make sure these arrays are same size
            return arrLanguageUtils;
        }
        for (int i = 0, size = languageNames.size(); i < size; i++) {
            arrLanguageUtils.add(new Language(i, languageNames.get(i), languageCodes.get(i), languageIcons.get(i)));
        }
        return arrLanguageUtils;
    }

    public static void showDialogSelectLanguage(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        dialog.setTitle(getString(R.string.title_select_language));
        dialog.setContentView(R.layout.dialog_language);
        dialog.setCancelable(false);
        final ListView lvLanguage = (ListView) dialog.findViewById(R.id.lvLanguage);
        final ListAdapter listAdapter = new ListAdapter(activity, getListLanguage());
        lvLanguage.setAdapter(listAdapter);
        lvLanguage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Language language = (Language) listAdapter.getItem(position);
                String curCode = getCurrentLanguage().getCode();
                if (!language.getCode().equals(curCode)) {
                    onChangeLanguageSuccessfully(language, activity);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private static void onChangeLanguageSuccessfully(final Language language, Activity activity) {
        LanguageUtils.setCurrentLanguage(language);
        LanguageUtils.changeLanguage(activity, language);
        restartApp(activity);
    }

    private static void setCurrentLanguage(Language language) {
        sCurrentLanguage = language;
    }

    private static void restartApp(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restart_app);
        dialog.setCancelable(false);
        Button btnRestartApp = (Button) dialog.findViewById(R.id.btnRestartApp);
        btnRestartApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mStartActivity = new Intent(activity, SplashActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(activity, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager) activity.getSystemService(activity.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis(), mPendingIntent);

                dialog.dismiss();
                System.exit(0);
            }
        });
        dialog.show();
    }


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({ENGLISH, FRENCH, VIETNAM})
    public @interface LocaleDef {
        String[] SUPPORTED_LOCALES = {ENGLISH, FRENCH, VIETNAM};
    }

    public static final String ENGLISH = "en";
    public static final String FRENCH = "fr";
    public static final String VIETNAM = "vi";


    public static boolean setLocale(Context context, @LocaleDef String language) {
        return updateResources(context, language);
    }

    private static boolean updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        context.createConfigurationContext(configuration);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return true;
    }

    public static List<String> getListFromResource(Context context, int id) {
        Language language = LanguageUtils.getCurrentLanguage();
        Configuration configuration = getConfiguration(context, language.getCode());
        return Arrays.asList(context.createConfigurationContext(configuration).getResources().getStringArray(R.array.arr_bottom_bar));
    }

    @NonNull
    private static Configuration getConfiguration(Context context, String lang) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(new Locale(lang));
        return configuration;
    }
}
