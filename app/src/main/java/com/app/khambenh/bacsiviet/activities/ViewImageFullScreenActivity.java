package com.app.khambenh.bacsiviet.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.app.khambenh.bacsiviet.Event.TouchImageView;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.adapters.ViewImageFullScreenAdapter;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.Item;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

public class ViewImageFullScreenActivity extends AppCompatActivity {
    private ViewImageFullScreenAdapter adapter;
    private ViewPager viewPager;

    ProgressBar progress_bar;
    TouchImageView imgDisplay;
    ImageButton imgbtn_next, imgbtn_prev;
    Button btn_close;

    ArrayList<String> arrayListImage = new ArrayList<>();
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_full_green);

        Intent intent = getIntent();
        position = intent.getIntExtra(Consts.EXTRA_POSITION, 0);
        arrayListImage = intent.getStringArrayListExtra(Consts.EXTRA__LIST_IMAGE);

        if (arrayListImage == null) {
            finish();
        }
        progress_bar = (ProgressBar)findViewById(R.id.progress_bar);
        progress_bar.setVisibility(View.VISIBLE);
        imgDisplay = (TouchImageView) findViewById(R.id.imgDisplay);

        btn_close = (Button) findViewById(R.id.btnClose);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        showDisplay();
    }

    private void showDisplay() {
        Glide.with(this).load(arrayListImage.get(position)).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progress_bar.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress_bar.setVisibility(View.GONE);
                return false;
            }
        }).into(imgDisplay);
    }
}
