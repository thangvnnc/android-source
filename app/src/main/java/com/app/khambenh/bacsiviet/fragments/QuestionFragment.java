package com.app.khambenh.bacsiviet.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.activities.AddQuestionActivity;
import com.app.khambenh.bacsiviet.activities.QuestionDetailsActivity;
import com.app.khambenh.bacsiviet.adapters.EndlessRecyclerViewScrollListener;
import com.app.khambenh.bacsiviet.adapters.QuestionAdapter;
import com.app.khambenh.bacsiviet.util.HttpUtils;
import com.app.khambenh.bacsiviet.util.MQuestionItem;
import com.app.khambenh.bacsiviet.util.MSpeciality;
import com.app.khambenh.bacsiviet.util.QuestionItem;
import com.app.khambenh.bacsiviet.utils.QBUser;
import com.app.khambenh.bacsiviet.utils.SharedPrefsHelper;
import com.app.khambenh.bacsiviet.utils.URLUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    private static QuestionFragment _questionFrm = null;
    private SwipeRefreshLayout _refeshLayout = null;
    private RecyclerView _rvListQuestion = null;
    private List<MQuestionItem.QItem> _questionItems = null;
    private QuestionAdapter _questionAdapter = null;
    private Spinner _spinner = null;
    private String _nextPageUrl = URLUtils.URL_LIST_QUESTION;
    private boolean _isLoading = false;
    private Button _btnAdd = null;
    public static final int RESULT_QA = 3000;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        reloadListQA();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            reloadListQA();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden == false) {
            reloadListQA();
        }
    }

    public static QuestionFragment getInstance()
    {
        if (_questionFrm == null)
        {
            _questionFrm = new QuestionFragment();
        }
        return _questionFrm;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        // View rootView = inflater.inflate(R.layout.fragment_question, container, false);
        View rootView = inflater.inflate(R.layout.k_frm_question, container, false);
        init(rootView);
//        getData();
        getQuestionList();
        return rootView;
    }

    /**
     * Ánh xạ và khởi tạo
     *
     * @param view : view
     */
    private void init(View view)
    {
        _refeshLayout       = view.findViewById(R.id.k_frm_question_refesh_id);
        _refeshLayout.setOnRefreshListener(this);
        _rvListQuestion     = view.findViewById(R.id.k_frm_question_rv_question_id);
        _spinner            = view.findViewById(R.id.k_frm_question_spinner_specialist_id);
         _btnAdd             = view.findViewById(R.id.fragment_question_btn_new_question_id);
//        _btnAdd             = new Button(getContext());
        _questionItems      = new ArrayList<>();
        _questionAdapter    = new QuestionAdapter(getActivity(), _questionItems);

        // Thiết định recycler view
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        _rvListQuestion.setLayoutManager(layoutManager);
        _rvListQuestion.setAdapter(_questionAdapter);

        EndlessRecyclerViewScrollListener endless = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (_nextPageUrl != null) {
                    if (!_isLoading) {
                        _isLoading = true;
                        getQuestionList();
                    }
                }
            }
        };
        _rvListQuestion.setOnScrollListener(endless);

        // Chọn trên spinner
        _spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                _questionItems.clear();
                _questionAdapter.notifyDataSetChanged();
                getQuestionList();
                layoutManager.scrollToPositionWithOffset(0,0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });

        _btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivityForResult(new Intent(getActivity(), AddQuestionActivity.class), RESULT_QA);
            }
        });
    }

    @Override
    public void onRefresh()
    {
        _questionItems.clear();
        _questionAdapter.notifyDataSetChanged();
        _nextPageUrl = URLUtils.URL_LIST_QUESTION;
        getQuestionList();
    }

    private void reloadListQA() {
        _questionItems.clear();
        _questionAdapter.notifyDataSetChanged();
        _nextPageUrl = URLUtils.URL_LIST_QUESTION;
        getQuestionList();
    }

    private void getQuestionList()
    {
        if ( _nextPageUrl == null || _nextPageUrl.equals(""))
        {
            return;
        }

        int userId = SharedPrefsHelper.getInstance().getQbUser().getId();

        Map<String, String> params = new HashMap<>();
        params.put("user_id", userId + "");
        _refeshLayout.setRefreshing(true);

        HttpUtils httpUtils = new HttpUtils(getContext(), new HttpUtils.VolleyCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                _refeshLayout.setRefreshing(false);
                _isLoading = false;

                if (result == null || result.equals(""))
                {
                    return;
                }
                Gson gson = new Gson();
                MQuestionItem questionItem = gson.fromJson(result, new TypeToken<MQuestionItem>(){}.getType());
                if (questionItem.getTotal() <= 0)
                {
                    return;
                }

                _nextPageUrl = questionItem.getNextPageUrl();

                _questionItems.addAll(questionItem.getData());
                _questionAdapter.notifyDataSetChanged();
            }
        });
        httpUtils.call(_nextPageUrl, params);
    }
}
