package com.app.khambenh.bacsiviet.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.khambenh.bacsiviet.CoreApp;
import com.app.khambenh.bacsiviet.R;
import com.app.khambenh.bacsiviet.util.APICall;
import com.app.khambenh.bacsiviet.util.CheckOrder;
import com.app.khambenh.bacsiviet.util.CheckOrderAPI;
import com.app.khambenh.bacsiviet.utils.CommonUtils;
import com.app.khambenh.bacsiviet.utils.Consts;
import com.app.khambenh.bacsiviet.utils.SharedPrefs;
import com.app.khambenh.bacsiviet.utils.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.app.khambenh.bacsiviet.utils.CommonUtils.MERCHANT_ID;
import static com.app.khambenh.bacsiviet.utils.CommonUtils.MERCHANT_PASSWORD;

public class CheckOrderActivity extends BaseActivity {

    public static final String TOKEN_CODE = "token_code";
    private TextView tvStatus;
    private String mTokenCode = "";
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_order);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mTokenCode = extras.getString(TOKEN_CODE, "");
        }

        tvStatus = findViewById(R.id.tvCheckOrderStatus);
        checkOrderObject();
        Button btnFinish = findViewById(R.id.btnFinishCheckOrder);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject postparams = new JSONObject();
                String usename = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_USER_NAME, String.class);
                int quantity = SharedPrefs.getInstance().get(SharedPrefs.SHAREPREFS_QUANTITY_WHEN_RECHARGE, Integer.class);
                try {
                    postparams.put("email", "" + usename);
                    postparams.put("quantity", "" + quantity);
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                            URLUtils.URL_RECHARGE, postparams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response != null) {
                                try {
                                    boolean isSuccess = response.getBoolean("isSuccess");
                                    String msg = response.getString("msg");
                                    if(isSuccess){
                                        long balace= response.getLong("balance");
                                        SharedPrefs.getInstance().put(SharedPrefs.SHAREPREFS_BALANCE,balace);
                                        Intent intent = new Intent();
                                        intent.setAction(Consts.ACTION_UPDATE_BALANCE);
                                        sendBroadcast(intent);
                                        Toast.makeText(CheckOrderActivity.this,msg,Toast.LENGTH_LONG).show();
                                    }else {
                                        Toast.makeText(CheckOrderActivity.this,msg,Toast.LENGTH_LONG).show();
                                    }
                                    finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(CheckOrderActivity.this, "" + error.getMessage(), Toast.LENGTH_LONG).show();
                            //Failure Callback
                            CommonUtils.SaveLLog(CheckOrderActivity.this, "", error.getMessage(), Consts.TAG_LOGIN_ACTIVITY);
                        }
                    });
                    CoreApp.getInstance().addToRequestQueue(jsonObjReq, "postRequest");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void checkOrderObject() {
        CheckOrder checkOrder = new CheckOrder();
        checkOrder.setFunc("checkOrder");
        checkOrder.setVersion("1.0");
        checkOrder.setMerchantId(MERCHANT_ID);
        checkOrder.setTokenCode(mTokenCode);
        String checksum = getChecksum(checkOrder);
        checkOrder.setChecksum(checksum);

        new CheckOrderAPI(new APICall<CheckOrder.CheckOrderResult>() {
            @Override
            protected void onFinish(CheckOrder.CheckOrderResult result) {
                try {

                    if ("00".equalsIgnoreCase(result.getResponseCode())) {
                        String view = "buyer_email:  " + result.getBuyerEmail() + "\n\n" +
                                "order_code:  " + result.getOrderCode() + "\n\n" +
                                "total_amount:  " + result.getTotalAmount() + "\n\n" +
                                "currency:  " + result.getCurrency() + "\n\n" +
                                "buyer_full_name:  " + result.getBuyerFullName() + "\n\n" +
                                "buyer_email:  " + result.getBuyerEmail() + "\n\n" +
                                "buyer_mobile:  " + result.getBuyerMobile() + "\n\n" +
                                "buyer_address:  " + result.getTotalAmount() + "\n\n" +
                                "transaction_status:  " + result.getTransactionStatus() + "\n\n";
                        tvStatus.setText(view);
                    } else {
                        showErrorDialog(CommonUtils.getCodeError(getApplicationContext(), result.getResponseCode()));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    showErrorDialog(getString(R.string.notifi_recharge_faild));
                }
            }
        }).execute(checkOrder);

    }

    private void showErrorDialog(String message) {
        alertDialog = new AlertDialog.Builder(CheckOrderActivity.this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }

    private String getChecksum(CheckOrder checkOrder) {
        String stringSendOrder = checkOrder.getFunc() + "|" +
                checkOrder.getVersion() + "|" +
                checkOrder.getMerchantId() + "|" +
                checkOrder.getTokenCode() + "|" +
                MERCHANT_PASSWORD;

        return CommonUtils.md5(stringSendOrder);
    }


    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
//        startActivity(intent);
//        finish();
    }
}
